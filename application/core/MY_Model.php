<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    // Ten table
    var $table = '';
    // Key chinh cua table
    var $key = '';
    // Order mac dinh (VD: $order = array('id', 'desc))
    var $order = '';
    // Cac field select mac dinh khi get_list (VD: $select = 'id, name')
    var $select = '';

    /**
     * Noi cac bang voi nhau
     */
    function get_Join($joinTable, $joinKey, $refKey) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($joinTable, "$joinTable.$joinKey = $this->table.$refKey");
        $query = $this->db->get();
        return $query->result();
    }

    function get_Join_where($joinTable, $joinKey, $refKey, $where = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($joinTable, "$joinTable.$joinKey = $this->table.$refKey");
        $this->db->where($where);
        //$this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    //Join nhiều bảng lại với nhau
    //$numTable: Số bảng cần join với bảng hiện tại
    //$listJoinTable : Danh sách các bảng cần join
    //$listJoinKey: Khóa liên kết giữa các bảng (đặt trong trường hợp khóa chính và khóa ngoại có cùng một tên), khác thì báo bố viết cái khác vào thay chứ sao giờ

    function get_Join_nTable($numTable, $listJoinTable = array(), $listJoinKey = array()) {
        $this->db->select("*");
        $this->db->from($this->table);

        for ($i = 1; $i < $numTable; $i++) {
            $this->db->join($listJoinTable[$i], "$listJoinTable[$i].$listJoinKey[$i] = $this->table.$listJoinKey[$i]");
        }

        $query = $this->db->get();
        return $query->result();
    }

    

    /**
     * Them row moi
     * $data : du lieu ma ta can them
     */
    function create($data = array()) {
        if ($this->db->insert($this->table, $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Cap nhat row tu id
     * $id : khoa chinh cua bang can sua
     * $data : mang du lieu can sua
     */
    function update($id, $data) {
        if (!$id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key] = $id;
        return $this->update_rule($where, $data);
    }

    /**
     * Cap nhat row tu dieu kien
     * $where : dieu kien
     * $data : mang du lieu can cap nhat
     */
    function update_rule($where, $data) {
        if (!$where) {
            return FALSE;
        }

        $this->db->where($where);
        if ($this->db->update($this->table, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Xoa row tu id
     * $id : gia tri cua khoa chinh
     */
    function delete($id) {
        if (!$id) {
            return FALSE;
        }
        //neu la so
        if (is_numeric($id)) {
            $where = array($this->key => $id);
        } else {
            //$id = 1,2,3...
            $where = $this->key . " IN (" . $id . ") ";
        }
        $this->del_rule($where);

        return TRUE;
    }

    /**
     * Xoa row tu dieu kien
     * $where : mang dieu kien de xoa
     */
    function del_rule($where) {
        if (!$where) {
            return FALSE;
        }

        $this->db->where($where);
        $this->db->delete($this->table);

        return TRUE;
    }

    /**
     * Thực hiện xóa một row nào đó theo khóa chính bất kỳ của bảng bất kỳ
     * Đã test và thành công
     */
    function delete_key($id) {
        $this->db->where($this->key, $id);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() > 0);
    }

    /**
     * Thực hiện câu lệnh query
     * $sql : cau lenh sql
     */
    function query($sql) {
        $rows = $this->db->query($sql);
        return $rows->result;
    }

    /**
     * Lay thong tin cua row tu id
     * $id : id can lay thong tin
     * $field : cot du lieu ma can lay
     */
    function get_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key] = $id;

        return $this->get_info_rule($where, $field);
    }

    /**
     * Lay thong tin cua row tu dieu kien
     * $where: Mảng điều kiện
     * $field: Cột muốn lấy dữ liệu
     */
    function get_info_rule($where = array(), $field = '') {
        if ($field) {
            $this->db->select($field);
        }
        $this->db->where($where);
        $query = $this->db->get($this->table);
        if ($query->num_rows()) {
            return $query->row();
        }

        return FALSE;
    }

    /**
     * Lay tong so
     */
    function get_total($input = array()) {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    /**
     * Lay tong so
     * $field: cot muon tinh tong
     */
    function get_sum($field, $where = array()) {
        $this->db->select_sum($field); //tinh rong
        $this->db->where($where); //dieu kien
        $this->db->from($this->table);

        $row = $this->db->get()->row();
        foreach ($row as $f => $v) {
            $sum = $v;
        }
        return $sum;
    }

    /**
     * Lay 1 row
     */
    function get_row($input = array()) {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->row();
    }

    /**
     * Lay danh sach
     * $input : mang cac du lieu dau vao
     */
    function get_list($input = array()) {
        //xu ly ca du lieu dau vao
        $this->get_list_set_input($input);

        //thuc hien truy van du lieu
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result();
    }

    /**
     * Gan cac thuoc tinh trong input khi lay danh sach
     * $input : mang du lieu dau vao
     */
    protected function get_list_set_input($input = array()) {

        // Thêm điều kiện cho câu truy vấn truyền qua biến $input['where'] 
        //(vi du: $input['where'] = array('email' => 'hocphp@gmail.com'))
        if ((isset($input['where'])) && $input['where']) {
            $this->db->where($input['where']);
        }

        //tim kiem like
        // $input['like'] = array('name' => 'abc');
        if ((isset($input['like'])) && $input['like']) {
            $this->db->like($input['like'][0], $input['like'][1]);
        }

        // Thêm sắp xếp dữ liệu thông qua biến $input['order'] 
        //(ví dụ $input['order'] = array('id','DESC'))
        if (isset($input['order'][0]) && isset($input['order'][1])) {
            $this->db->order_by($input['order'][0], $input['order'][1]);
        } else {
            //mặc định sẽ sắp xếp theo id giảm dần 
            $order = ($this->order == '') ? array($this->table . '.' . $this->key, 'desc') : $this->order;
            $this->db->order_by($order[0], $order[1]);
        }

        // Thêm điều kiện limit cho câu truy vấn thông qua biến $input['limit'] 
        //(ví dụ $input['limit'] = array('10' ,'0')) 
        if (isset($input['limit'][0]) && isset($input['limit'][1])) {
            $this->db->limit($input['limit'][0], $input['limit'][1]);
        }
    }

    /**
     * kiểm tra sự tồn tại của dữ liệu theo 1 điều kiện nào đó
     * $where : mang du lieu dieu kien
     */
    function check_exists($where = array()) {
        $this->db->where($where);
        //thuc hien cau truy van lay du lieu
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>