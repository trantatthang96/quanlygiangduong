<?php

Class MY_Controller extends CI_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();

        $controller = $this->uri->segment(1);
        switch ($controller) {
            case 'admin': {
                    $this->load->helper('admin');
                    $this->_check_login();

                    $user_id_login = $this->session->userdata('login');
                    $this->data['login'] = $user_id_login;
                    $this->data['isAdmin'] = $this->session->userdata('isAdmin');

                    //Neu dang nhap thanh cong
                    if ($user_id_login) {
                        $this->load->model('nguoidung_model');
                        $user_info = $this->nguoidung_model->get_info($user_id_login);
                        $this->data['user_info'] = $user_info;
                    }
                    break;
                }
            default: {
                    //$this->_check_user_login();
                    //Kiểm tra thành viên đăng nhập chưa
                    $user_id_login = $this->session->userdata('user_id_login');
                    //Neu dang nhap thanh cong
                    $quyen = 0;
                    if ($user_id_login) {
                        $quyen = 1;
                        $this->load->model('nguoidung_model');
                        $user_info = $this->nguoidung_model->get_info($user_id_login);
                        $this->data['user_info'] = $user_info;
                        if ($user_info->MaLoaiND == 1 || $user_info->MaLoaiND == 2 || $user_info->MaLoaiND == 3)
                            $quyen = 2;
                    }
                    $this->data['quyen'] = $quyen;
                }
        }
    }

    //Nếu người dùng không phải là nhân viên hoặc admin thì không cho đăng nhập
    private function _check_login() {
        $controller = $this->uri->rsegment('1');
        $controller = strtolower($controller);

        $login = $this->session->userdata('login');
        if ($login) {
            $this->load->model('nguoidung_model');
            $user = $this->nguoidung_model->get_info($login);
            $canAccess = 0;
            if ($user->MaLoaiND == 1 || $user->MaLoaiND == 2 || $user->MaLoaiND == 4)
                $canAccess = 1;
        }
        //pre($canAccess);

        if (!$login && $controller != 'login') {
            redirect(admin_url('login'));
        }

        if ($login && $controller != 'login' && $canAccess == 0) {
            redirect(admin_url('login'));
        }

        if ($login && $controller == 'login' && $canAccess) {
            redirect(admin_url('home'));
        }
    }

    private function _check_user_login() {
        $controller = $this->uri->rsegment('1');
        $controller = strtolower($controller);

        $login = $this->session->userdata('user_id_login');
        if (!$login && $controller != 'login')
            redirect(base_url('login'));

        if ($login && $controller == 'login')
            redirect(base_url('home'));
    }

}
