<?php $this->load->view('admin/chitietnhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">
 
        
        <div class="title">
            <h6>Thêm chi tiết nhật ký phản hồi mới </h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Phòng:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="maphong" id="param_maphong" _autocheck="true" width="300">
                            <?php foreach ($list_p as $row):?>
                            <option value="<?php echo $row->MaPhong ?>" <?php if($row->MaPhong == set_value('maphong')) echo 'selected';?>><?php echo $row->TenPhong ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="matb" id="param_matb" _autocheck="true" width="300">
                            <?php foreach ($list_tb as $row):?>
                            <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == set_value('matb')) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Số lượng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="soluong" id="param_soluong" _autocheck="true" type="text" value="<?php echo set_value('soluong') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('soluong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <!-- Tình trạng thiết bị -->
                <div class="formRow">
                    <label class="formLeft" for="param_name">Tình trạng thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="tinhtrang" id="param_tinhtrang" _autocheck="true" width="300">
                            <?php foreach ($list_tttb as $row):?>
                            <option value="<?php echo $row->MaTTTB ?>" <?php if($row->MaTTTB == set_value('matttb')) echo 'selected';?>><?php echo $row->TenTT ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả vị trí:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="motavitri" id="param_motavitri" _autocheck="true" type="text" value="<?php echo set_value('motavitri') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('motavitri') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 


                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mota" id="param_mota" _autocheck="true" type="text" value="<?php echo set_value('mota') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('mota') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                   <a class="button" type="button" href="<?php echo admin_url("nhatkyphanhoi/details/".$id) ?>">Quay lại</a>
                </div>


            </fieldset>
        </form>
    </div>

    <div class="clear mt30"></div>
</div>


