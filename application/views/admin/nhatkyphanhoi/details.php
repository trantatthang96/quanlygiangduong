<?php $this->load->view('admin/chitietnhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">

        <div class="title">
            <h6>
                Danh sách chi tiết phản hồi của mã <?php echo $maph?>       </h6>
            <div class="num f12">Số lượng: <b><?php echo count($listdb) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
            <thead class="filter"><tr><td colspan="11">
                        <form class="list_filter form" action="<?php echo admin_url('nhatkyphanhoi/details/'.$maph) ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Mã CTPH</label></td>
                                        <td class="item"><input name="mankph" value="<?php echo $this->input->get('mankph') ?>" id="filter_id" style="width:55px;" type="text"></td>

                                        <td class="label" style="width:90px;"><label for="filter_id">Phòng</label></td>
                                        <td class="item">
                                            <select name="maphong">
                                                <option value=""></option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_p as $row):?>
                                                <option value="<?php echo $row->MaPhong ?>" <?php if($row->MaPhong == $maphong) echo 'selected';?>><?php echo $row->TenPhong ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Thiết bị</label></td>
                                        <td class="item">
                                            <select name="matb">
                                                <option value=""></option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_tb as $row):?>
                                                <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == $matb) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('nhatkyphanhoi/details/'.$maph) ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>
            <thead>
                <tr>
                    <td style="width:70px;">Mã chi tiết phản hồi</td>
                    <td style="width:70px;">Mã phản hồi</td>
                    <td style="width:70px;">Phòng</td>
                    <td style="width:120px;">Thiết bị được phản hồi</td>
                    <td style="width:50px;">Số lượng</td>
                    <td style="width:200px;">Mô tả tình trạng</td>
                    <td style="width:200px;">Mô tả vị trí</td>
                    <td style="width:80px;">Mô tả</td>
                    <td style="width:100px;">Thời gian duyệt</td>
                    <td style="width:70px;">Trạng thái duyệt</td>
                    <td style="width:80px;">Hành động</td>

                </tr>
            </thead>

            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="12">
                        

                        <div class="list_action itemActions">
                            <a href="<?php echo admin_url('nhatkyphanhoi/addCTNKPH/' . $maph) ?>" class="button redB" url="">
                                <span style="color:white;">Thêm mới</span>
                            </a>
                        </div>
                        <div class="pagination">
                            <?php // if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

            <tbody class="list_item">
                <?php foreach ($listdb as $row): ?>
                    <tr class="row_9">

                    <td class="textC"><?php echo $row->MaNKPH?></td>         

                    <td class="textC"><?php echo $row->MaPH?></td>
                
                    <td class="textC"><?php echo $row->TenPhong?></td>

                    <td class="textC"><?php echo $row->TenTB?></td>

                    <td class="textC"><?php echo $row->SoLuong?></td>
         
                    <td class="textC"><?php echo $row->TenTT ?></td>

                    <td class="textC"><?php echo $row->MoTaViTri ?></td>

                    <td class="textC"><?php echo $row->MoTa?></td>

                    <td class="textC"><?php echo $row->ThoiGianDuyet; ?></td> 
         
                    <td class="textC"><?php 
                        if($row->TrangThai == 2) echo "Đã sửa chữa"; 
                        else if($row->TrangThai == 1) echo "Đã duyệt";
                        else echo "Chưa duyệt"; ?>
                            
                    </td>
                    
                    <td class="option textC">
                    
                    <a href="<?php echo admin_url('chitietnhatkyphanhoi/approval/'.$row->MaPH).'/'.$row->MaNKPH ?>" class="tipS " original-title="Duyệt" style="display: <?php if($row->Duyet == false) echo "none" ?>;">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/accept.png" style="display: <?php if($row->Duyet == false) echo "none" ?>;">
                    </a>

                    <a href="<?php echo admin_url('chitietnhatkyphanhoi/repaired/'.$row->MaPH).'/'.$row->MaNKPH ?>" class="tipS " original-title="Sữa chữa" style="display: <?php if($row->SuaChua == false) echo "none" ?>;">
                        <img src="<?php echo public_url('admin'); ?>/images/icons/color/cancel.png" style="display: <?php if($row->SuaChua == false) echo "none" ?>;">
                    </a>

                                      
                    <!-- <a href="<?php echo admin_url('nhatkyphanhoi/editCTNKPH/' . $row->MaNKPH) ?>" title="Chỉnh sửa" class="tipS">
                        <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                    </a> -->

                    <a href="<?php echo admin_url('chitietnhatkyphanhoi/delete/' . $row->MaNKPH)   ?>" title="Xóa" class="tipS verify_action" style="display: <?php if($row->Xoa == false) echo "none" ?>;">
                        <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png" style="display: <?php if($row->Xoa == false) echo "none" ?>;">
                    </a>

                                   
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    </div>

    <div class="clear mt30"></div>

</div>


