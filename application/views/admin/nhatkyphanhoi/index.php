<?php $this->load->view('admin/nhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <div class="widget">

        <div class="title">
            <span class="titleIcon"><div class="checker" id="uniform-titleCheck"><span><input type="checkbox" id="titleCheck" name="titleCheck" style="opacity: 0;"></span></div></span>
            <h6>Danh sách Phản hồi</h6>
            <div class="num f12"><b><?php echo count($list) ?></b> Phản hồi</div>
        </div>

        <!--<ul class="tabs">
            <li class="<?php echo ($xuly == '1') ? 'activeTab':'' ?>"><a href="<?php echo admin_url('dexuat?xuly=1')?>">Chưa xử lý</a></li>
            <li class="<?php echo ($xuly == '2') ? 'activeTab':'' ?>"><a href="<?php echo admin_url('dexuat?xuly=2')?>">Đã xử lý</a></li>
        </ul> -->

        <table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable taskWidget" id="checkAll">
            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('nhatkyphanhoi') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Mã PH</label></td>
                                        <td class="item"><input name="maph" value="<?php echo $this->input->get('maph') ?>" id="filter_id" style="width:60px;" type="text" placeholder="Mã PH"></td>

                                        <td class="label" style="width:90px;"><label for="filter_id">Giáo viên phản hồi</label></td>
                                        <td class="item">
                                            <select name="magvph" style="width:120px;">
                                                <option value="">Tất cả giáo viên</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_nd as $row):?>
                                                <option value="<?php echo $row->MaND ?>" <?php if($row->MaND == $magvph) echo 'selected';?>><?php echo $row->HoTenND ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Nhân viên duyệt</label></td>
                                        <td class="item">
                                            <select name="manvduyet" style="width:120px;">
                                                <option value="">Tất cả nhân viên</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_nd as $row):?>
                                                <option value="<?php echo $row->MaND ?>" <?php if($row->MaND == $manvduyet) echo 'selected';?>><?php echo $row->HoTenND ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('nhatkyphanhoi') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>
            <thead>
                <tr>
                    <td style="width:70px;">Mã phản hồi</td>
                    <td style="width:150px;">Giáo viên phản hồi</td>
                    <td style="width:150px;">Nhân viên duyệt</td>
                    <td style="width:100px;">Thời gian gửi</td>         
                    <!-- <td style="width:100px;">Thời gian duyệt</td>
                    <td style="width:70px;">Trạng thái</td> -->
                    <td style="width:150px;">Ghi chú</td>
                    <td style="width:80px;">Hành động</td>
                </tr>
            </thead>

            <tfoot class="auto_check_pages">
            </tfoot>

            <tbody>
                <?php foreach ($list as $row): ?>
                <tr class="row_<?php echo $row->MaPH ?>">
                    
                    <td class="textC"><?php echo $row->MaPH?></td>         
                
                    <td>
                        <a class="wUserPic" title="" href="#"><img src="<?php echo base_url('upload/nguoidung/' . $row->HinhAnh) ?>" width="60px" align="left"></a>
                        <ul class="leftList">
                            <li><strong><?php echo $row->HoTenND?></strong></li>
                        </ul>
                    </td>

                    <td>
                        <a class="wUserPic" title="" href="#"><img src="<?php echo base_url('upload/nguoidung/' . $row->HA_NVDuyet) ?>" width="60px" align="left"></a>
                        <ul class="leftList">
                            <li><strong><?php echo $row->NVDuyet?></strong></li>
                        </ul>
                    </td>

                    <td class="textC"><?php echo $row->ThoiGianGoi;?></td>

         <!--            <td class="textC"><?php echo date("d-m-Y", strtotime($row->ThoiGianDuyet)); ?></td> -->
         
                    <!-- <td class="textC"><?php if($row->TrangThai == 1) echo "Đã duyệt"; else echo "Chưa duyệt"; ?></td> -->

                    <td class="textC"><?php echo $row->GhiChu ?></td>

                    <td class="option" >
                         <a href="<?php echo admin_url('nhatkyphanhoi/details/'.$row->MaPH) ?>" class="tipS " original-title="Chi tiết" >
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/view.png">
                        </a>
                        <?php if($isAdmin) {?>

                       <!--  <a href="<?php echo admin_url('nhatkyphanhoi/approval/'.$row->MaPH) ?>" class="tipS " original-title="Duyệt" style="display: <?php if($row->TrangThai == 1) echo "none" ?>;">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/accept.png" style="display: <?php if($row->TrangThai == 1) echo "none" ?>;">
                        </a> -->

                       

                        <a href="<?php echo admin_url('nhatkyphanhoi/edit/'.$row->MaPH) ?>" class="tipS " original-title="Chỉnh sửa">
                        <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                        </a>
                        <a href="<?php echo admin_url('nhatkyphanhoi/delete/' . $row->MaPH) ?>" class="tipS verify_action" original-title="Xóa">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                        </a>
                        <?php } ?>
                    </td>
                </tr>
                <?php endforeach;?>
                
            </tbody>
            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="10">

                        <div class="pagination">
                            <?php if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
</div>
<div class="clear mt30"></div>
