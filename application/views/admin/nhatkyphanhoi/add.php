<?php $this->load->view('admin/nhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới phản hồi</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Nhân viên duyệt:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="manvduyet" id="param_manvduyet" _autocheck="true" >
                                <?php foreach ($list_nd as $row): ?>
                                    <option value="<?php echo $row->MaND ?>"<?php if($row->MaND == set_value('manvduyet')) echo 'selected';?>> <?php echo $row->HoTenND ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ghi chú:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ghichu" id="param_ghichu" _autocheck="true" type="text" value="<?php echo set_value('ghichu') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>                

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("nhatkyphanhoi") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>



