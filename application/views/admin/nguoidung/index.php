<?php $this->load->view('admin/nguoidung/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">

        <div class="title">
            <span class="titleIcon"><input id="titleCheck" name="titleCheck" type="checkbox"></span>
            <h6>
                Danh sách người dùng            </h6>
            <div class="num f12">Số lượng: <b><?php echo count($list) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">

            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('nguoidung') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Mã người dùng</label></td>
                                        <td class="item"><input name="mand" value="<?php echo $this->input->get('mand') ?>" id="filter_id" style="width:55px;" type="text" placeholder="Mã ND"></td>

                                        <td class="label" style="width:90px;"><label for="filter_id">Tên người dùng</label></td>
                                        <td class="item" style="width:140px;"><input name="tennd" value="<?php echo $this->input->get('tennd') ?>" id="filter_iname" style="width:155px;" type="text" placeholder="Tên người dùng"></td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Loại người dùng</label></td>
                                        <td class="item">
                                            <select name="maloaind" style="width:120px;">
                                                <option value="">Tất cả loại ND</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_lnd as $lnd):?>
                                                <option value="<?php echo $lnd->MaLoaiND ?>" <?php if($lnd->MaLoaiND == $malnd) echo 'selected';?>><?php echo $lnd->TenLoai ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('nguoidung') ?>';" type="reset">                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>

            <thead>
                <tr>
                    <td style="width:21px;"><img src="<?php echo public_url('admin'); ?>/images/icons/tableArrows.png"></td>
                    <td>Mã người dùng</td>
                    <td>Người dùng</td>
                    <td>Giới tính</td>
                    <td style="width:100px;">Ngày sinh</td>
                    <td>Chức vụ</td>
                    <td>Điện thoại</td>
                    <td>Email</td>
                    <td>Username</td>
                    <td style="width:100px;">Hành động</td>
                </tr>
            </thead>

            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="10">
                        <?php if($isAdmin) {?>
                        <div class="list_action itemActions">
                            <a href="#submit" id="submit" class="button blueB" url="<?php echo admin_url('admin/delete_all') ?>">
                                <span style="color:white;">Xóa hết</span>
                            </a>
                        </div>
                        <?php } ?>

                        <div class="pagination">
                            <?php if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

            <tbody class="list_item">
                <?php foreach ($list as $row): ?>
                    <tr class="row_<?php echo $row->MaND ?>">
                        <td><input name="id[]" value="<?php echo $row->MaND ?>" type="checkbox"></td>

                        <td class="textC"><?php echo $row->MaND ?></td>

                        <td>
                            <div class="image_thumb">
                                <?php if($row->HinhAnh) {?>
                                <img src="<?php echo base_url('upload/nguoidung/' . $row->HinhAnh) ?>" style="width: 70px; height: 70px;" >
                                <?php } else { ?>
                                <img src="<?php echo public_url('admin') ?>/images/user.png" style="width: 70px; height: 70px;">
                                <?php } ?>
                                <div class="clear"></div>
                            </div>

                            <b><?php echo $row->HoTenND ?></b>

                            <div class="f11">
                                <?php foreach ($list_lnd as $lnd) {
                                    if($row->MaLoaiND == $lnd->MaLoaiND)
                                        echo $lnd->TenLoai;
                                } ?>
                            </div>

                        </td>
                        
                        <td class="textC">
                            <?php if($row->GioiTinh) echo "Nam"; else echo "Nữ"; ?>
                        </td>
                                
                        <td class="textC"><?php echo date("d-m-Y", strtotime($row->NgaySinh)) ?></td>
                        
                        <td class="textC">
                            <?php echo $row->ChucVu ?>
                        </td>

                        <td class="textC">
                            <?php echo $row->DienThoai ?>
                        </td>

                        <td class="textC">
                            <?php echo $row->Email ?>
                        </td>

                        <td class="textC">
                            <?php echo $row->Username ?>
                        </td>


                        <td class="option textC">
                            <?php if($isAdmin) { ?>
                        
                            <a href="<?php echo admin_url('nguoidung/edit/' . $row->MaND) ?>" title="Chỉnh sửa" class="tipS">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <a href="<?php echo admin_url('nguoidung/delete/' . $row->MaND) ?>" title="Xóa" class="tipS verify_action">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    </div>
</div>

<div class="clear mt30"></div>
