<?php $this->load->view('admin/nguoidung/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">

    <div class="widget">
        <div class="title">
            <h6>Thêm mới người dùng</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Họ và tên:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="hotennd" id="param_hotennd" _autocheck="true" type="text" value="<?php echo set_value('hotennd') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('hotennd') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Loại người dùng:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="maloaind" id="param_maloaind" _autocheck="true" width="300">
                            <?php foreach ($list_lnd as $lnd):?>
                            <option value="<?php echo $lnd->MaLoaiND ?>" <?php if($lnd->MaLoaiND == set_value('maloaind')) echo 'selected';?>><?php echo $lnd->TenLoai ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Giới tính:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="gioitinh" id="param_gioitinh" _autocheck="true" >
                                <option value="1" <?php if(set_value('gioitinh') == "1") echo 'selected';?>>Nam</option>
                                <option value="0"<?php if(set_value('gioitinh') == "0") echo 'selected';?>>Nữ</option>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('gioitinh') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ngày sinh:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ngaysinh" id="param_ngaysinh" _autocheck="true" type="date" value="<?php echo set_value('ngaysinh') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('ngaysinh') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Chức vụ:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="chucvu" id="param_chucvu" _autocheck="true" type="text" value="<?php echo set_value('chucvu') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('chucvu') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Điện thoại:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="dienthoai" id="param_username" _autocheck="true" type="text" value="<?php echo set_value('dienthoai') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('dienthoai') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label class="formLeft" for="param_name">Email:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="email" id="param_username" _autocheck="true" type="text" value="<?php echo set_value('email') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('email') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left"><input type="file" id="image" name="image"></div>
                        <div name="image_error" class="clear error"></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label class="formLeft" for="param_name">Username:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="username" id="param_username" _autocheck="true" type="text" value="<?php echo set_value('username') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('username') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label class="formLeft" for="param_name">Mật khẩu:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="password" id="param_password" _autocheck="true" type="password"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Nhập lại mật khẩu:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="re_password" id="param_re_password" _autocheck="true" type="password"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('re_password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("nguoidung") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>

</div>


