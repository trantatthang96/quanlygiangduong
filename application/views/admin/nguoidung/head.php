<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Quản trị tài khoản</h5>
            <span>Quản lý tài khoản người dùng</span>
        </div>

        <div class="horControlB menu_action">
            <ul>
                <?php if($isAdmin) {?>
                <li><a href="<?php echo admin_url('nguoidung/add')?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/hire-me.png">
                        <span>Thêm người dùng</span>
                    </a></li>
                    <?php } ?>

                    <li><a href="<?php echo admin_url('nguoidung/index') ?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/list.png">
                        <span>Danh sách</span>
                    </a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>
