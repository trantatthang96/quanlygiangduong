
<div class="line"></div>

<div class="wrapper" style="margin-top: 50px">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới giảng đường</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div></div>
                <div class="formRow">
					<div class="formRow>
                    <label class="formLeft" for="param_name">Nhân viên quản lí:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="manvql" id="param_manvql" _autocheck="true" width="300">
                            <?php foreach ($list_nvql as $nvql):?>
                            <option value="<?php echo $nvql->MaND ?>" <?php if($nvql->MaND == set_value('manvql')) echo 'selected'; ?>><?php echo $nvql->HoTenND ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
					</div>
					</div>
                </div>
				

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên giảng đường:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tengd" id="param_tengiangduong" _autocheck="true" type="text" value="<?php echo set_value('tengd') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tengd') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label class="formLeft" for="param_name">Số tầng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="sotang" id="param_sotang" _autocheck="true" type="text" value="<?php echo set_value('sotang') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('sotang') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left"><input type="file" id="image" name="image"></div>
                        <div name="image_error" class="clear error"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("giangduong") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>

