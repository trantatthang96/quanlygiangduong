<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Giảng đường</h5>
            <span>Quản lý giảng đường</span>
        </div>

        <div class="horControlB menu_action">
            <ul>
                <?php if($isAdmin) {?>
                <li><a href="<?php echo admin_url('giangduong/add')?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/add.png">
                        <span>Thêm mới</span>
                    </a></li> 
                    <?php } ?>

                    <li><a href="<?php echo admin_url('giangduong/index') ?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/list.png">
                        <span>Danh sách</span>
                    </a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>