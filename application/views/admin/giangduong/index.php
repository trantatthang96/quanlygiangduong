<?php $this->load->view('admin/giangduong/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data) ?>
        <div class="title">
            <h6>Danh sách giảng đường</h6>
            <div class="num f12">Tổng số: <b><?php echo $total ?></b></div>
        </div>
        <form action="" method="get" class="form" name="filter">
            <table class="sTable mTable myTable withCheck" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
                <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('giangduong') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:90px;"><label for="filter_id">Tên GĐ</label></td>
                                        <td class="item"><input name="tengd" value="<?php echo $this->input->get('tengd') ?>" id="filter_id" style="width:70px;" type="text" placeholder="Tên GĐ"></td>

                                        <td class="label" style="width:90px;"><label for="filter_id">Nhân viên quản lý</label></td>
                                        <td class="item">
                                            <select name="manvql" style="width:125px;">
                                                <option value="">Tất cả nhân viên</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_nd as $row):?>
                                                <option value="<?php echo $row->MaND ?>" <?php if($row->MaND == $mand) echo 'selected';?>><?php echo $row->HoTenND ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('giangduong') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>

                <thead>
                    <tr>
                        <td style="width:200px;">Mã giảng đường</td>
                        <td>Tên giảng đường</td>
                        <td>Nhân viên quản lí</td>
                        <td>Số tầng</td>
                        <td>Số lượng phòng</td>
                        <td style="width:100px;">Hành động</td>
                    </tr>
                </thead>

                <tfoot>
                    
                </tfoot>

                <tbody>
                    <!-- Filter -->
                    <?php foreach ($list as $row):?>
                    <tr>
                     
                        <td class="textC"><?php echo $row->MaGD ?></td>
  
                        <td class="textC"><?php echo $row->TenGD ?></td>

                        <td class="textC"><?php echo $row->HoTenND?></td>

                        <td class="textC"><?php echo $row->SoTang ?></td>
                        
                        <td class="textC"><?php echo $row->totalRoom ?></td>

                        <td class="option">
                            <?php if($isAdmin) {?>
                            <a href="<?php echo admin_url('giangduong/edit/'.$row->MaGD) ?>" class="tipS " original-title="Chỉnh sửa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <a href="<?php echo admin_url('giangduong/delete/'.$row->MaGD) ?>" class="tipS verify_action" original-title="Xóa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </form>
    </div>
</div>
<div class="clear mt30"></div>
