<div id="leftSide" style="padding-top:30px;">

    <!-- Account panel -->

    <div class="sideProfile">
        <a href="#" title="" class="profileFace">
            <?php if($user_info->HinhAnh != '') {?>
            <img src="<?php echo base_url('upload/nguoidung/'.$user_info->HinhAnh) ?>" width="40">
            <?php } else { ?>
            <img src="<?php echo public_url('admin') ?>/images/user.png">
            <?php } ?>
        </a>
        <span>Xin chào: <strong><?php echo $user_info->HoTenND ?></strong></span>
        <?php if($user_info->MaLoaiND == '1'): ?>
        <span><?php echo "Quản trị viên"?></span>
        <?php else: ?>
        <span><?php echo "Nhân viên"?></span>
        <?php endif;?>
        <div class="clear"></div>
    </div>
    <div class="sidebarSep"></div>		    
    <!-- Left navigation -->

    <ul id="menu" class="nav">

        <li class="home">

            <a href="<?php echo admin_url('home') ?>" class="active" id="current">
                <span>Bảng điều khiển</span>
                <strong></strong>
            </a>


        </li>
        <li class="tran">

            <a href="admin/tran.html" class="exp inactive">
                <span>Quản lý giảng đường</span>
                <strong>3</strong>
            </a>

            <ul class="sub" style="display: none;">
                <li>
                    <a href="<?php echo admin_url('giangduong') ?>">
                        Giảng đường							</a>
                </li>
                <li>
                    <a href="<?php echo admin_url('phong') ?>">
                        Phòng học							</a>
                </li>
                <li>
                    <a href="<?php echo admin_url('phong_thietbi') ?>">
                        Phòng học - Thiết bị                </a>
                </li>
            </ul>

        </li>
        <li class="product">

            <a href="admin/product.html" class="exp inactive">
                <span>Quản lý trang thiết bị</span>
                <strong>5</strong>
            </a>

            <ul class="sub" style="display: none;">
                <li>
                    <a href="<?php echo admin_url('thietbi') ?>">
                        Thiết bị							</a>
                </li>

                <li>
                    <a href="<?php echo admin_url('loaithietbi') ?>">
                        Loại thiết bị                            </a>
                </li>

                <li>
                    <a href="<?php echo admin_url('tinhtrangthietbi') ?>">
                        Tình trạng thiết bị                           </a>
                </li>
                
                <li>
                    <a href="<?php echo admin_url('nhatkyphanhoi') ?>">
                        Nhật ký phản hồi							</a>
                </li>

                <li>
                    <a href="<?php echo admin_url('chitietnhatkyphanhoi') ?>">
                        Danh sách chi tiết nhật ký phản hồi          </a>
                </li>
            </ul>

        </li>
        <li class="account">

            <a href="admin/account.html" class="exp inactive">
                <span>Tài khoản</span>
                <strong>2</strong>
            </a>

            <ul class="sub" style="display: none;">
                <li>
                    <a href="<?php echo admin_url('nguoidung') ?>">
                        Người dùng
                    </a>
                </li>

                <li>
                    <a href="<?php echo admin_url('loainguoidung') ?>">
                        Loại người dùng                           </a>
                </li>
            </ul>

        </li>

    </ul>

</div>
<div class="clear"></div>
