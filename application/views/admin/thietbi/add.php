<?php $this->load->view('admin/thietbi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới thiết bị</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tentb" id="param_hotennd" _autocheck="true" type="text" value="<?php echo set_value('tentb') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tentb') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Loại thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="maloaitb" id="param_maloaitb" _autocheck="true" width="300">
                            <?php foreach ($list_ltb as $row):?>
                            <option value="<?php echo $row->MaLoaiTB ?>" <?php if($row->MaLoaiTB == set_value('maloaitb')) echo 'selected'; ?>><?php echo $row->TenLoai ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mota" id="param_mota" _autocheck="true" type="text" value="<?php echo set_value('mota') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:</label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" multiple="" name="image_list[]" id="image_list" size="25" >
                        </div>
                        <div class="clear error" name="image_list_error"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("thietbi") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>
