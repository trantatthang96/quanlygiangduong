<?php $this->load->view('admin/thietbi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">

        <div class="title">
            <h6>
                Danh sách trang thiết bị            </h6>
            <div class="num f12">Tổng số: <b><?php echo count($list) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('thietbi') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_status">Loại thiết bị</label></td>
                                        <td class="item">
                                            <select name="maloaitb">
                                                <option value="">Tất cả loại</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_ltb as $row):?>
                                                <option value="<?php echo $row->MaLoaiTB ?>" <?php if($row->MaLoaiTB == $maloaitb) echo 'selected';?>><?php echo $row->TenLoai ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('thietbi') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>

            <thead>
                <tr>
                    <td style="width:80px;">Mã thiết bị</td>
                    <td>Tên thiết bị</td>
                    <td>Tên loại thiết bị</td>
                    <td>Mô tả</td>
                    <td>Hình ảnh</td>
                    <td style="width:120px;">Hành động</td>

                </tr>
            </thead>

            <tfoot class="auto_check_pages">
            </tfoot>

            <tbody class="list_item">
                <?php foreach ($list as $row): ?>
                    <tr class="row_<?php echo $row->MaTB ?>">

                        <td class="textC"><?php echo $row->MaTB ?></td>

                        <td class="textC"><?php echo $row->TenTB ?></td>

                        <td class="textC"><?php echo $row->TenLoai ?></td>

                        <td class="textC"><?php echo $row->MoTa ?></td>

                        <td>
                            <?php $image = json_decode($row->HinhAnh); ?>
                            <?php if (is_array($image)): ?>
                                <?php foreach ($image as $img): ?>
                                    <div class="image_thumb">
                                        <img src="<?php echo base_url('upload/thietbi/' . $img) ?>" height="70">
                                        <div class="clear"></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>

                        <td class="option textC">  
                        <?php if($isAdmin) {?>                       
                            <a href="<?php echo admin_url('thietbi/edit/' . $row->MaTB) ?>" title="Chỉnh sửa" class="tipS">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <a href="<?php echo admin_url('thietbi/delete/' . $row->MaTB) ?>" title="Xóa" class="tipS verify_action">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    </div>
</div>

<div class="clear mt30"></div>
