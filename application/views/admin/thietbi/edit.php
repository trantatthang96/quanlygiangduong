<?php $this->load->view('admin/thietbi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <img src="<?php echo public_url('admin'); ?>/images/icons/dark/dialog.png" class="titleIcon">
            <h6>
                Hình ảnh thiết bị
            </h6>
        </div>

        <div class="gallery">            
            <ul>
                <?php $image = json_decode($info->HinhAnh); ?>          
                <?php if (is_array($image)): ?>
                <?php foreach ($image as $img): ?>
                <li id="1">      
                    <a class="lightbox cboxElement" title="<?php echo $info->TenTB ?>" href="<?php echo base_url('upload/thietbi/' . $img) ?>">
                        <img src="<?php echo base_url('upload/thietbi/' . $img) ?>" width="280px">
                    </a>
                    
                </li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <div class="clear" style="height:20px"></div>
        </div>      

        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="matb" id="param_matb" _autocheck="true" type="text" value="<?php echo $info->MaTB ?>" readonly></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error() ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tentb" id="param_tentb" _autocheck="true" type="text" value="<?php echo $info->TenTB ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tentb') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Loại thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="maloaitb" id="param_manvql" _autocheck="true" width="300">
                            <?php foreach ($list_ltb as $row):?>
                            <option value="<?php echo $row->MaLoaiTB ?>" <?php if($row->MaLoaiTB == $info->MaLoaiTB) echo "selected"?>><?php echo $row->TenLoai ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 
                
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mota" id="param_mota" _autocheck="true" type="text" value="<?php echo $info->MoTa ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>                

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" multiple="" id="image_list" name="image_list[]" size="25">
                        </div>
                        <div name="image_list_error" class="clear error"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("thietbi") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>


