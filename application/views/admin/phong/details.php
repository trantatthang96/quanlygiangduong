<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">

        <div class="title">
            <h6>
                Danh sách thiết bị phòng <?php echo $TenPhong ?>		</h6>
            <div class="num f12">Số lượng: <b><?php echo count($listdb) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('phong/details/'.$maphong) ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_status">Thiết bị</label></td>
                                        <td class="item">
                                            <select name="matb">
                                                <option value=""></option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_tb as $row):?>
                                                <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == $matb) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>
<!-- 
                                        <td class="label" style="width:80px;"><label for="filter_status">Tình trạng</label></td>
                                        <td class="item">
                                            <select name="matttb">
                                                <option value=""></option>
                                                 kiem tra danh muc co danh muc con hay khong
                                                <?php foreach ($list_tttb as $row):?>
                                                <option value="<?php echo $row->MaTTTB ?>" <?php if($row->MaTTTB == $matttb) echo 'selected';?>><?php echo $row->TenTT ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td> -->

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('phong/details/'.$maphong) ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>


            <thead>
                <tr>
                    <td style="width:50px;">Mã thiết bị</td>
                    <td>Tên thiết bị</td>
                    <td style="width:50px;">Số lượng</td>
                    <td style="width:80px;">Tình trạng</td>
                    <td style="width:80px;">Hình ảnh</td>
                    <td>Ghi chú</td>

                    <td style="width:120px;">Hành động</td>

                </tr>
            </thead>

            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="12">
                        

                        <div class="list_action itemActions">
                            <a href="<?php echo admin_url('phong/addThietBi/' . $maphong) ?>" class="button redB" url="">
                                <span style="color:white;">Thêm mới thiết bị</span>
                            </a>
                        </div>
                        <div class="pagination">
                            <?php // if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

            <tbody class="list_item">
                <?php foreach ($listdb as $row): ?>
                    <tr class="row_9">

                        <td class="textC"><?php echo $row->MaTB ?></td>

                        <td>
                            <a href="" class="tipS" title="" target="_blank">
                                <b><?php echo $row->TenTB ?></b>
                            </a>
                        </td>

                        <td class="textC"><?php echo $row->SoLuong ?></td>

                        <td class="textC"><?php echo $row->TTTB ?></td>

                        <td>
                            <?php $image = json_decode($row->HinhAnh); ?>
                            <?php if (is_array($image)): ?>
                                <?php foreach ($image as $img): ?>
                                    <div class="image_thumb">
                                        <img src="<?php echo base_url('upload/thietbi/' . $img) ?>" height="70">
                                        <div class="clear"></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>

                        <td>
                            <a <span class="tipS" original-title="<?php echo $row->GhiChu ?>">
                                    <?php echo word_limiter($row->GhiChu, 4) ?>
                                    </td>

                                    <td class="option textC">

                                        <?php if($isAdmin) {?>
                                        <a href="<?php echo admin_url('phong/editThietBi/' . $row->MaPTB) ?>" title="Chỉnh sửa" class="tipS">
                                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                                        </a>

                                        <a href="<?php echo admin_url('phong/delThietBi/'.$row->MaPTB)   ?>" title="Xóa" class="tipS verify_action">
                                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                                        </a>

                                        <?php } ?>

                                   
                                    </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                                </table>
                                </div>

                                <div class="clear mt30"></div>






                                </div>


