<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data) ?>
        <div class="title">
            <h6>
                Danh sách phòng         </h6>
            <div class="num f12">Số lượng: <b><?php echo count($list) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">

            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('phong') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Tên Phòng</label></td>
                                        <td class="item"><input name="tenphong" value="<?php echo $this->input->get('tenphong') ?>" id="filter_id" style="width:100px;" type="text" placeholder="Tên phòng"></td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Giảng đường</label></td>
                                        <td class="item">
                                            <select name="magd" style="width:140px;">
                                                <option value="">Tất cả giảng đường</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_gd as $row):?>
                                                <option value="<?php echo $row->MaGD ?>" <?php if($row->MaGD == $magd) echo 'selected';?>><?php echo $row->TenGD ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_id">Sức chứa</label></td>
                                        <td class="item"><input name="succhua" value="<?php echo $this->input->get('succhua') ?>" id="filter_id" style="width:55px;" type="text"></td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('phong') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>

            <thead>
                <tr>
                    <td style="width:80px;">Mã phòng</td>
                    <td>Tên phòng</td>
                    <td style="width:80px;">Giảng đường</td>
                    <td style="width:70px;">Sức chứa</td>
                    <td style="width:100px;">Tình trạng</td>
                    <td style="width:100px;">Hình ảnh</td>
                    <td style="width:120px;">Hành động</td>
                </tr>
            </thead>



            <tbody class="list_item">
                <?php foreach ($list as $row): ?>
                    <tr class="row_9">
                        

                        <td class="textC"><?php echo $row->MaPhong ?></td>

                        <td class="textC"><?php echo $row->TenPhong   ?></td>

                        <td class="textC"><?php echo $row->TenGD   ?></td>

                        <td class="textC"><?php echo $row->SucChua   ?></td>
                        
                        <?php if ($row->TinhTrang == 1): ?>
                            <td class="textC" style="color: green" >
                                <b><?php echo "Sử dụng được"?></b>
                            </td>
                        <?php else: ?>
                            <td class="textC" style="color: purple" >
                                <b><?php echo "Không sử dụng được"?></b>
                            </td>
                        <?php endif;?>

                        <td>
                            <?php $image = json_decode($row->HinhAnh); ?>
                            <?php if (is_array($image)): ?>
                                <?php foreach ($image as $img): ?>
                                    <div class="image_thumb">
                                        <img src="<?php echo base_url('upload/phong/' . $img) ?>" height="70">
                                        <div class="clear"></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                      


                        <td class="option textC">
                            <?php if($isAdmin) {?>
                            <a href="<?php echo admin_url('phong/details/'.$row->MaPhong)  ?>" title="Chi tiết" class="tipS">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/view.png">
                            </a>

                            <a href="<?php echo admin_url('phong/edit/'.$row->MaPhong)  ?>" title="Chỉnh sửa" class="tipS">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <a href="<?php echo admin_url('phong/deleteph/'.$row->MaPhong)   ?>" title="Xóa" class="tipS verify_action">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>



            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="10">

                        <div class="pagination">
                            <?php if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
</div>

<div class="clear mt30"></div>