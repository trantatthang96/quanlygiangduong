<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">

        <div class="title">
            <img src="<?php echo public_url('admin'); ?>/images/icons/dark/dialog.png" class="titleIcon">
            <h6>
                Hình ảnh thiết bị
            </h6>
        </div>

        <div class="gallery">            
            <ul>
                <?php $image = json_decode($info->HinhAnh); ?>          
                <?php if (is_array($image)): ?>
                <?php foreach ($image as $img): ?>
                <li id="1">      
                    <a class="lightbox cboxElement" title="<?php echo $info->TenTB ?>" href="<?php echo base_url('upload/thietbi/' . $img) ?>">
                        <img src="<?php echo base_url('upload/thietbi/' . $img) ?>" width="280px">
                    </a>
                    
                </li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <div class="clear" style="height:20px"></div>
        </div>      

    </div>
    <div class="widget">       
        <div class="title">
            <h6>Sửa thiết bị cho phòng</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="thietbi" id="param_thietbi" _autocheck="true" type="text" value="<?php echo $info->TenTB ?>" readonly></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Số lượng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="soluong" id="param_soluong" _autocheck="true" type="text" value="<?php echo $info->SoLuong ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('soluong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ghi chú:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ghichu" id="param_sotang" _autocheck="true" type="text" value="<?php echo $info->GhiChu ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('ghichu') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("phong/details/".$info->MaPhong) ?>">Quay lại</a>
                </div>


            </fieldset>
        </form>
    </div>

    <div class="clear mt30"></div>
</div>

