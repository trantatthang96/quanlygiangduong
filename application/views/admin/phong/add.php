<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới phòng học</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên phòng học:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tenphong" id="param_tenphong" _autocheck="true" type="text" value="<?php echo set_value('tenphong') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tenphong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label class="formLeft" for="param_succhua">Sức chứa:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="succhua" id="param_succhua" _autocheck="true" type="text" value="<?php echo set_value('succhua') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('succhua') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Giảng đường:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="magd" id="param_magd" _autocheck="true" >
                                <?php foreach ($listgd as $row): ?>
                                    <option value="<?php echo $row->MaGD ?>" <?php echo ($row->MaGD == set_value('magd')) ? 'selected' : ''; ?> ><?php echo $row->TenGD ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('manhom') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                  <div class="formRow">
                    <label class="formLeft" for="param_tinhtrang">Tình trạng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="tinhtrang" id="param_tinhtrang" _autocheck="true" >
                                    <option value="0" <?php echo (set_value('tinhtrang') == 0) ? 'selected' : ''; ?> >Không sử dụng</option>
                                    <option value="1" <?php echo (set_value('tinhtrang') == 1) ? 'selected' : ''; ?> >Đang sử dụng</option>
                                    
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tinhtrang') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:</label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" multiple="" name="image_list[]" id="image_list" size="25" >
                        </div>
                        <div class="clear error" name="image_list_error"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("phong") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>
