<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">
 
        
        <div class="title">
            <h6>Thêm thiết bị cho phòng</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                    <span class="oneTwo">
                        <select name="matb" id="param_matb" _autocheck="true" width="300">
                            <?php foreach ($list_tb as $row):?>
                            <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == set_value('matb')) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                        <?php endforeach; ?>    
                        </select>
                    </span>
                    <span name="name_autocheck" class="autocheck"></span>
                     <!--Check validation chỗ nãy éo biết dùng -->
                    <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <div class="formRow">
                    <label class="formLeft" for="param_name">Số lượng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="soluong" id="param_soluong" _autocheck="true" type="text" value="<?php echo set_value('soluong') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('soluong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ghi chú:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ghichu" id="param_sotang" _autocheck="true" type="text" value="<?php echo set_value('ghichu') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('ghichu') ?></div>
                    </div>
                    <div class="clear"></div>
                </div> 

                <!-- <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" multiple="" id="image_list" name="image_list[]" size="25">
                        </div>
                        <div name="image_list_error" class="clear error"></div>
                    </div>
                    <div class="clear"></div>
                </div> -->

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("phong/details/".$id) ?>">Quay lại</a>
                </div>


            </fieldset>
        </form>
    </div>

    <div class="clear mt30"></div>
</div>
