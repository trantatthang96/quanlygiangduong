<?php $this->load->view('admin/phong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">

        <div class="title">
            <img src="<?php echo public_url('admin'); ?>/images/icons/dark/dialog.png" class="titleIcon">
            <h6>
                Hình ảnh thiết bị
            </h6>
        </div>

        <div class="gallery">            
            <ul>
                <?php $image = json_decode($info->HinhAnh); ?>          
                <?php if (is_array($image)): ?>
                <?php foreach ($image as $img): ?>
                <li id="1">      
                    <a class="lightbox cboxElement" title="<?php echo $info->TenPhong ?>" href="<?php echo base_url('upload/phong/' . $img) ?>">
                        <img src="<?php echo base_url('upload/phong/' . $img) ?>" width="280px">
                    </a>
                    
                </li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <div class="clear" style="height:20px"></div>
        </div>      

    </div>
    <div class="widget">
        <div class="title">
            <h6>Cập nhật phòng học</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã phòng học:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="maphong" id="param_maphong" _autocheck="true" type="text" value="<?php echo $info->MaPhong ?>" readonly></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('maphong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên phòng học:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tenphong" id="param_tenphong" _autocheck="true" type="text" value="<?php echo $info->TenPhong ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tenphong') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label class="formLeft" for="param_name">Sức chứa:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="succhua" id="param_tenphong" _autocheck="true" type="text" value="<?php echo $info->SucChua ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('succhua') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label class="formLeft" for="param_name">Giảng đường:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="magd" id="param_magd" _autocheck="true" >
                                <?php foreach ($listgd as $row): ?>
                                    <option value="<?php echo $row->MaGD ?>" <?php echo ($row->MaGD == $info->MaGD) ? 'selected' : ''; ?> ><?php echo $row->TenGD ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('manhom') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                  <div class="formRow">
                    <label class="formLeft" for="param_tinhtrang">Tình trạng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="tinhtrang" id="param_tinhtrang" _autocheck="true" >
                                    <option value="0" <?php //echo ($row->TinhTrang == '0') ? 'selected' : ''; ?> >Không sử dụng</option>
                                    <option value="1" <?php //echo ($row->TinhTrang == '1') ? 'selected' : ''; ?> >Đang sử dụng</option>
                                    
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tinhtrang') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" multiple="" id="image_list" name="image_list[]" size="25">
                        </div>
                        <div name="image_list_error" class="clear error"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("phong") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>


