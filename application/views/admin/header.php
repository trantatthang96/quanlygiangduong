<div class="topNav">
    <div class="wrapper">
        <div class="welcome">
            <span>Xin chào: <b><?php echo $user_info->HoTenND ?></b></span>
        </div>

        <div class="userNav">
            <ul>
                <li><a href="<?php echo base_url('home') ?>" target="_blank">
                        <img style="margin-top:7px;" src="<?php echo public_url('admin'); ?>/images/icons/light/home.png">
                        <span>Trang người dùng</span>
                    </a></li>

                <!-- Logout -->
                <li><a href="<?php echo admin_url('nguoidung/logout') ?>">
                        <img src="<?php echo public_url('admin')?>/images/icons/topnav/logout.png" alt="">
                        <span>Đăng xuất</span>
                    </a></li>

            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>