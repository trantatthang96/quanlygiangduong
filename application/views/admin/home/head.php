<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Bảng điều khiển của nhân viên <?php echo $user->HoTenND ?></h5>
            <p>Danh sách phản hồi</p>
            <ul>
                <li style="color: red;"> <img src="<?php echo public_url('admin') ?>/images/icons/color/copy.png"> Có <?php echo $need_approval ?> phản hồi cần duyệt</li>
                <li> <img src="<?php echo public_url('admin') ?>/images/icons/color/calendar.png"> Có <?php echo $need_repair ?> phản hồi đã duyệt nhưng chưa được sửa chữa</li>
                <li> <img src="<?php echo public_url('admin') ?>/images/icons/color/star.png"> Có <?php echo $repaired ?> phản hồi đã xác nhận được sửa chữa</li>
            </ul>
        </div>

        <div class="horControlB menu_action">
            <ul>

                    <li><a href="<?php echo admin_url('chitietnhatkyphanhoi/index') ?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/list.png">
                        <span>Danh sách tất cả phản hồi</span>
                    </a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>