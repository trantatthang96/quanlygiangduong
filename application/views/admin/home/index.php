<?php $this->load->view('admin/home/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data) ?>
        <div class="title">
            <h6>Danh sách chi tiết phản hồi</h6>
            <div class="num f12"><b><?php echo count($list) ?></b> Phản hồi</div>
        </div>

        <table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable taskWidget" id="checkAll">
            <thead class="filter"><tr><td colspan="11">
                        <form class="list_filter form" action="<?php echo admin_url('home') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Mã CTPH</label></td>
                                        <td class="item"><input name="mankph" value="<?php echo $this->input->get('mankph') ?>" id="filter_id" style="width:90px;" type="text" placeholder="Mã CTNKPH"></td>

                                        <td class="label" style="width:90px;"><label for="filter_id">Phòng</label></td>
                                        <td class="item">
                                            <select name="maphong" style="width:125px;">
                                                <option value="">Tất cả các phòng</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_p as $row):?>
                                                <option value="<?php echo $row->MaPhong ?>" <?php if($row->MaPhong == $maphong) echo 'selected';?>><?php echo $row->TenPhong ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Thiết bị</label></td>
                                        <td class="item">
                                            <select name="matb" style="width:125px;">
                                                <option value="">Tất cả thiết bị</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_tb as $row):?>
                                                <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == $matb) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Trạng thái</label></td>
                                        <td class="item">
                                            <select name="trangthai" style="width:125px;">
                                                <option value="">Tất cả trạng thái</option>
                                                <option value="0" <?php if($trangthai == "0") echo "selected"; ?>>Chưa duyệt</option>
                                                <option value="1" <?php if($trangthai == "1") echo "selected"; ?>>Đã duyệt</option>
                                                <option value="2" <?php if($trangthai == "2") echo "selected"; ?>>Đã sửa</option>
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('home') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>

            <thead>
                <tr>
                    <td style="width:70px;">Mã chi tiết phản hồi</td>
                    <td style="width:70px;">Mã phản hồi</td>
                    <td style="width:70px;">Phòng</td>
                    <td style="width:120px;">Thiết bị được phản hồi</td>
                    <td style="width:50px;">Số lượng</td>
                    <td style="width:200px;">Mô tả tình trạng</td>
                    <td style="width:200px;">Mô tả vị trí</td>
                    <td style="width:80px;">Mô tả</td>
                    <td style="width:100px;">Thời gian duyệt</td>
                    <td style="width:70px;">Trạng thái duyệt</td>
                    <td style="width:80px;">Hành động</td>
                </tr>
            </thead>

            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="11">

                        <div class="pagination">
                            <?php  echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>

            <tbody>
                <?php foreach ($list as $row): ?>
                <tr class="row_<?php echo $row->MaPH ?>">
                    
                    <td class="textC"><?php echo $row->MaNKPH?></td>  

                    <td class="textC"><?php echo $row->MaPH?></td>       
                
                    <td class="textC"><?php echo $row->TenPhong?></td>

                    <td class="textC"><?php echo $row->TenTB?></td>

                    <td class="textC"><?php echo $row->SoLuong?></td>
         
                    <td class="textC"><?php echo $row->TenTT ?></td>

                    <td class="textC"><?php echo $row->MoTaViTri ?></td>

                    <td class="textC"><?php echo $row->MoTa?></td>

                    <td class="textC"><?php echo $row->ThoiGianDuyet; ?></td> 
         
                    <td class="textC"><?php 
                        if($row->TrangThai == 2) echo "Đã sửa chữa"; 
                        else if($row->TrangThai == 1) echo "Đã duyệt";
                        else echo "Chưa duyệt"; ?>
                            
                    </td>

                    <td class="option">
                        <a href="<?php echo admin_url('chitietnhatkyphanhoi/approval/'.$row->MaPH).'/'.$row->MaNKPH ?>" class="tipS " original-title="Duyệt" style="display: <?php if($row->Duyet == false) echo "none" ?>;">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/accept.png" style="display: <?php if($row->Duyet == false) echo "none" ?>;">
                        </a>

                        <a href="<?php echo admin_url('chitietnhatkyphanhoi/repaired/'.$row->MaPH).'/'.$row->MaNKPH ?>" class="tipS " original-title="Sữa chữa" style="display: <?php if($row->SuaChua == false) echo "none" ?>;">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/cancel.png" style="display: <?php if($row->SuaChua == false) echo "none" ?>;">
                        </a>

                        <a href="<?php echo admin_url('chitietnhatkyphanhoi/delete/' . $row->MaNKPH) ?>" class="tipS verify_action" original-title="Xóa" style="display: <?php if($row->Xoa == false) echo "none" ?>;">
                            <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png" style="display: <?php if($row->Xoa == false) echo "none" ?>;">
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
                
            </tbody>

        </table>
    </div>
</div>
<div class="clear mt30"></div>
