<?php $this->load->view('admin/giangduong/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Cập nhật giảng đường</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
       
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã loại phòng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="malp" id="param_mand" _autocheck="true" type="text" value="<?php echo $info->MaLP ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('malp') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên loại phòng:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tenlp" id="param_hotennd" _autocheck="true" type="text" value="<?php echo $info->TenLoaiPhong ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tenlp') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mục đích sử dụng:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mucdich" id="param_sotang" _autocheck="true" type="text" value="<?php echo $info->MucDichSuDung ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('mucdich') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("loaiphong") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>


