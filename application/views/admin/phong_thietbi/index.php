<?php  $this->load->view('admin/phong_thietbi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data) ?>
    <div class="widget">

        <div class="title">
            <h6>
                Danh sách thiết bị phòng học            </h6>
            <div class="num f12">Số lượng: <b><?php echo count($list) ?></b></div>
        </div>

        <table class="sTable mTable myTable" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
            <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('phong_thietbi') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>

                                        <td class="label" style="width:90px;"><label for="filter_id">Phòng</label></td>
                                        <td class="item">
                                            <select name="maphong" style="width:100px;">
                                                <option value="">Tất cả phòng</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_p as $row):?>
                                                <option value="<?php echo $row->MaPhong ?>" <?php if($row->MaPhong == $maphong) echo 'selected';?>><?php echo $row->TenPhong ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <td class="label" style="width:80px;"><label for="filter_status">Thiết bị</label></td>
                                        <td class="item">
                                            <select name="matb" style="width:110px;">
                                                <option value="">Tất cả thiết bị</option>
                                                <!-- kiem tra danh muc co danh muc con hay khong -->
                                                <?php foreach ($list_tb as $row):?>
                                                <option value="<?php echo $row->MaTB ?>" <?php if($row->MaTB == $matb) echo 'selected';?>><?php echo $row->TenTB ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td>

                                        <!-- <td class="label" style="width:80px;"><label for="filter_status">Tình trạng thiết bị</label></td>
                                        <td class="item">
                                            <select name="matttb">
                                                <option value=""></option>
                                                 kiem tra danh muc co danh muc con hay khong
                                                <?php foreach ($list_tttb as $row):?>
                                                <option value="<?php echo $row->MaTTTB ?>" <?php if($row->MaTTTB == $matttb) echo 'selected';?>><?php echo $row->TenTT ?></option> 
                                                <?php endforeach; ?> 
                                                </optgroup>

                                            </select>
                                        </td> -->

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('phong_thietbi') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>
    
            <thead>
                <tr>
                    <td style="width:21px;">Mã Phòng</td>
                    <td>Tên phòng</td>
                    <td style="width:50px;">Mã thiết bị</td>
                    <td>Tên thiết bị</td>
                    <td style="width:50px;">Số lượng</td>
                    <td style="width:80px;">Tình trạng</td>
                    <td style="width:80px;">Hình ảnh</td>
                    <td>Ghi chú</td>

                    <td style="width:120px;">Hành động</td>

                </tr>
            </thead>
            <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="10">
                        <div class="pagination">
                            <?php if ($showAll) echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
            </tfoot>
            <tbody class="list_item">
                <?php  foreach ($list as $row): ?>
                    <tr class="row_9">

                        <td class="textC"><?php echo $row->MaPhong ?></td>

                        <td class="textC">
                            <a href="<?php echo admin_url('phong/details/'.$row->MaPhong)   ?>" title="<?php echo $row->TenPhong ?>" class="tipS">
                                <?php echo $row->TenPhong ?>
                            </a>
                        </td>

                        <td class="textC"><?php echo $row->MaTB ?></td>

                        <td class="textC"><?php echo $row->TenTB ?></td>

                        <td class="textC"><?php echo $row->SoLuong ?></td>

                        <td class="textC"><?php echo $row->TTTB ?></td>

                        <td>
                            <?php $image = json_decode($row->HinhAnh); ?>
                            <?php if (is_array($image)): ?>
                                <?php foreach ($image as $img): ?>
                                    <div class="image_thumb">
                                        <img src="<?php echo base_url('upload/thietbi/' . $img) ?>" height="70">
                                        <div class="clear"></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                        
                        <td>
                        <a <span class="tipS" original-title="<?php echo $row->GhiChu ?>">
                            <?php echo word_limiter($row->GhiChu, 4) ?>
                        </td>
                        
                        <td class="option textC">
                            <?php if($isAdmin) {?>
                            <a href="<?php echo admin_url('phong_thietbi/delThietBi/'.$row->MaPTB)   ?>" title="Xóa" class="tipS verify_action">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php  endforeach; ?>
            </tbody>



        </table>
    </div>
</div>



<div class="clear mt30"></div>
