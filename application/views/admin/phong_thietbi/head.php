﻿<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Quản lý phòng học - thiết bị</h5>
            <span>Quản lý trang thiết bị phòng học</span>
        </div>

        <div class="horControlB menu_action">
            <ul>

                    <li><a href="<?php echo admin_url('phong_thietbi/index') ?>">
                        <img src="<?php echo public_url('admin') ?>/images/icons/control/16/list.png">
                        <span>Danh sách</span>
                    </a></li>
            </ul>
        </div>
        
        <div class="clear"></div>
    </div>
</div>