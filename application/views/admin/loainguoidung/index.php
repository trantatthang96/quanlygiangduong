<?php $this->load->view('admin/loainguoidung/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data) ?>
        <div class="title">
            <h6>Danh sách loại người dùng</h6>
            <div class="num f12">Tổng số: <b><?php count($list); ?></b></div>
        </div>
        <form action="" method="get" class="form" name="filter">
            <table class="sTable mTable myTable withCheck" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
                <thead class="filter"><tr><td colspan="10">
                        <form class="list_filter form" action="<?php echo admin_url('loainguoidung') ?>" method="get">
                            <table width="100%" cellspacing="0" cellpadding="0"><tbody>

                                    <tr>
                                        <td class="label" style="width:80px;"><label for="filter_id">Tên loại ND</label></td>
                                        <td class="item"><input name="tenloai" value="<?php echo $this->input->get('tenloai') ?>" id="filter_id" style="width:100px;" type="text" placeholder="Tên loại ND"></td>

                                        <td style="width:150px">
                                            <input class="button blueB" value="Lọc" type="submit">
                                            <input class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('loainguoidung') ?>';" type="reset">
                                        </td>

                                    </tr>
                                </tbody></table>
                        </form>
                    </td></tr></thead>
                <thead>
                    <tr>
                        <td style="width:200px;">Mã loại ND</td>
                        <td>Tên loại</td>
                        <td>Mô tả</td>
                        <td style="width:100px;">Hành động</td>
                    </tr>
                </thead>

                <tfoot>
                    
                </tfoot>

                <tbody>
                    <!-- Filter -->
                    <?php foreach ($list as $row):?>
                    <tr>
                     
                        <td class="textC"><?php echo $row->MaLoaiND ?></td>
  

                        <td class="textC"><?php echo $row->TenLoai ?></td>

                        <td class="textC"><?php echo $row->Mota ?></td>                      

                        <td class="option">
                            <?php if($isAdmin) { ?>
                            <a href="<?php echo admin_url('loainguoidung/edit/'.$row->MaLoaiND) ?>" class="tipS " original-title="Chỉnh sửa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <a href="<?php echo admin_url('loainguoidung/delete/'.$row->MaLoaiND) ?>" class="tipS verify_action" original-title="Xóa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </form>
    </div>
</div>
<div class="clear mt30"></div>
