
<div class="line"></div>

<div class="wrapper">
    <div class="widget" style="margin-top: 50px">
        <div class="title">
            <h6>Thêm mới loại thiết bị</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên loại thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tenloai" id="param_tenloai" _autocheck="true" type="text" value="<?php echo set_value('tenloai') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tenloai') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mota" id="param_mota" _autocheck="true" type="text" value="<?php echo set_value('mota') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("loaithietbi") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>



