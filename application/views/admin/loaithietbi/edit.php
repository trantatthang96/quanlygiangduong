<?php $this->load->view('admin/loaithietbi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Cập nhật loại thiết bị</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã loại TB:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="maloaitb" id="param_maloaitb" _autocheck="true" type="text" value="<?php echo $info->MaLoaiTB ?>" readonly></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('maloaitb') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Tên loại thiết bị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="tenloai" id="param_tenloai" _autocheck="true" type="text" value="<?php echo $info->TenLoai ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('tenloai') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Mô tả:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="mota" id="param_mota" _autocheck="true" type="text" value="<?php echo $info->MoTa ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('mota') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>                

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <a class="button" type="button" href="<?php echo admin_url("loaithietbi") ?>">Quay lại</a>
                </div>
            </fieldset>
        </form>
    </div>


</div>


