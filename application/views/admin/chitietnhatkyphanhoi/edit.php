<?php $this->load->view('admin/nhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Cập nhật phản hồi</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã phản hồi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="maph" id="param_maph" _autocheck="true" type="text" value="<?php echo $info->MaPH ?>" readonly></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('maph') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Giáo viên phản hồi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="magvph" id="param_magvph" _autocheck="true" >
                                <?php foreach ($list_nd as $row): ?>
                                    <option value="<?php echo $row->MaND ?>" <?php if($info->MaGVPH == $row->MaND) echo 'selected'; ?>> <?php echo $row->HoTenND ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Nhân viên duyệt:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="manvduyet" id="param_manvduyet" _autocheck="true" >
                                <?php foreach ($list_nd as $row): ?>
                                    <option value="<?php echo $row->MaND ?>" <?php if($info->MaNVDuyet == $row->MaND) echo 'selected'; ?>> <?php echo $row->HoTenND ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thời gian gửi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="thoigiangui" id="param_thoigiangui" _autocheck="true" type="date" value="<?php echo date("Y-m-d", strtotime($info->ThoiGianGoi)) ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('thoigiangui') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thời gian duyệt:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="thoigianduyet" id="param_thoigianduyet" _autocheck="true" type="date" value="<?php echo date("Y-m-d", strtotime($info->ThoiGianDuyet)) ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('thoigianduyet') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Trạng thái:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="trangthai" id="param_trangthai" _autocheck="true" type="text" value="<?php echo $info->TrangThai ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('trangthai') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ghi chú:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ghichu" id="param_ghichu" _autocheck="true" type="text" value="<?php echo $info->GhiChu ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                

                <div class="formSubmit">
                    <input value="Cập nhật" class="redB" type="submit">
                    <input value="Hủy" class="redB" type="reset">
                </div>
            </fieldset>
        </form>
    </div>


</div>

