<?php $this->load->view('admin/nhatkyphanhoi/head', $this->data) ?>
<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới phản hồi</h6>
        </div>
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="formRow">
                    <label class="formLeft" for="param_name">Mã phản hồi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="maph" id="param_maph" _autocheck="true" type="text" value="<?php echo set_value('maph') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('maph') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Giáo viên phản hồi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="magvph" id="param_magvph" _autocheck="true" >
                                <?php foreach ($list_nd as $row): ?>
                                    <option value="<?php echo $row->MaND ?>"> <?php echo $row->HoTenND ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Nhân viên duyệt:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo">
                            <select name="manvduyet" id="param_manvduyet" _autocheck="true" >
                                <?php foreach ($list_nd as $row): ?>
                                    <option value="<?php echo $row->MaND ?>"> <?php echo $row->HoTenND ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thời gian gửi:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="thoigiangui" id="param_thoigiangui" _autocheck="true" type="date" value="<?php echo set_value('thoigiangui') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('thoigiangui') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Thời gian duyệt:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="thoigianduyet" id="param_thoigianduyet" _autocheck="true" type="date" value="<?php echo set_value('thoigianduyet') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('thoigianduyet') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Trạng thái:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="trangthai" id="param_trangthai" _autocheck="true" type="text" value="<?php echo set_value('trangthai') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('trangthai') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft" for="param_name">Ghi chú:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input name="ghichu" id="param_ghichu" _autocheck="true" type="text" value="<?php echo set_value('ghichu') ?>"></span>
                        <span name="name_autocheck" class="autocheck"></span>
                        <div name="name_error" class="clear error"><?php echo form_error('') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                

                <div class="formSubmit">
                    <input value="Thêm mới" class="redB" type="submit">
                    <input value="Hủy" class="redB" type="reset">
                </div>
            </fieldset>
        </form>
    </div>
</div>


