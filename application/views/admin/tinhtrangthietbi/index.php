<?php $this->load->view('admin/tinhtrangthietbi/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data) ?>
        <div class="title">
            <h6>Danh sách tình trạng thiết bị</h6>
            <div class="num f12">Tổng số: <b><?php echo count($list); ?></b></div>
        </div>
        <form action="" method="get" class="form" name="filter">
            <table class="sTable mTable myTable withCheck" id="checkAll" width="100%" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:200px;">Mã tình trạng</td>
                        <td>Tên tình trạng</td>
                        <td>Mô tả</td>
                        <td style="width:100px;">Hành động</td>
                    </tr>
                </thead>

                <tfoot>
                    
                </tfoot>

                <tbody>
                    <!-- Filter -->
                    <?php foreach ($list as $row):?>
                    <tr>
                     
                        <td class="textC"><?php echo $row->MaTTTB ?></td>
  
                        <td class="textC"><?php echo $row->TenTT ?></td>

                        <td class="textC"><?php echo $row->MoTa ?></td>                     

                        <td class="option">
                            <?php if($isAdmin) {?>
                            <a href="<?php echo admin_url('tinhtrangthietbi/edit/'.$row->MaTTTB) ?>" class="tipS " original-title="Chỉnh sửa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/edit.png">
                            </a>

                            <!-- <a href="<?php echo admin_url('tinhtrangthietbi/delete/'.$row->MaTTTB) ?>" class="tipS verify_action" original-title="Xóa">
                                <img src="<?php echo public_url('admin'); ?>/images/icons/color/delete.png">
                            </a> -->

                            <?php } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </form>
    </div>
</div>
<div class="clear mt30"></div>
