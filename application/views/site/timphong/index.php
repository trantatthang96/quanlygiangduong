﻿<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-list-ul"></i> Yêu cầu phòng</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url('timphong') ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-2">
                                <label for="" class="control-label">Mã phòng:</label>
                                <input name="maphong" id="inputMaPhong" type="text" class="form-control" placeholder="" value="<?php echo $this->input->post('maphong') ?>">
                            </div>
                            <div class="col-md-2">
                                <label for="" class="control-label">Tên phòng:</label>
                                <input name="tenphong" id="inputTenPhong" type="text" class="form-control" placeholder="" value="<?php echo $this->input->post('tenphong') ?>">
                            </div>
                            <div class="col-md-2">
                                <label for="" class="control-label">Giảng đường:</label>
                                <select name="magd" class="form-control">
                                    <option value="">Tất cả giảng đường</option>
                                    <?php foreach ($giangduong as $row): ?>
                                        <option value="<?php echo $row->MaGD ?>" <?php echo ($this->input->post('magd') == $row->MaGD) ? 'selected' : ''; ?> ><?php echo $row->TenGD ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                            <div class="col-md-2">
                                <label for="" class="control-label">Sức chứa:</label>
                                <input name="succhua" type="number" class="form-control" id="inputSucChua" placeholder="" min="1" value="<?php echo $this->input->post('succhua') ?>">
                            </div>
                            <div class="col-md-2">
                                <label for="" class="control-label">Tình Trạng:</label>
                                <select name="tinhtrang" class="form-control" id="selTinhTrang">
                                    <option value="">Tất cả</option>
                                    <option value="1" <?php echo ($this->input->post('tinhtrang') == '1') ? 'selected' : ''; ?>>Sử dụng được</option>
                                    <option value="2" <?php echo ($this->input->post('tinhtrang') == '2') ? 'selected' : ''; ?>>Không sử dụng được</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="" class="control-label">Tìm</label>
                                <input type="submit" class="form-control btn btn-success" value="Tìm"></input>
                            </div>
                            <div class="col-md-5">
                                <label class="control-label">Trang bị</label>
                                <div>
                                    <div class="col-md-4 checkbox" style="padding: 0;">
                                        <label><input type="checkbox" name="maychieu" value="1">Máy chiếu</label>
                                    </div>
                                    <div class="col-md-4 checkbox" style="padding: 0;">
                                        <label><input type="checkbox" name="tivi" value="1">Tivi</label>
                                    </div>
                                    <div class="col-md-4 checkbox" style="padding: 0;">
                                        <label><input type="checkbox" name="loa" value="1">Loa</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center main-color-bg">Danh sách phòng học</div>
                            <table class="table table-bordered text-center">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Mã phòng</th>
                                        <th class="text-center">Tên phòng</th>
                                        <th class="text-center">Sức chứa</th>
                                        <th class="text-center">Tình trạng</th>
                                        <th class="text-center">Thiết bị</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($list as $row): ?>
                                        <tr>
                                            <th class="text-center" scope="row"><?php echo $i ?></th>
                                            <td><?php echo $row->MaPhong ?></td>
                                            <td><?php echo $row->TenPhong ?></td>
                                            <td><?php echo $row->SucChua ?></td>
                                            <td><?php
                                                if ($row->TinhTrang == 1)
                                                    echo '<span class="label label-success">Sử dụng được</span>';
                                                else {
                                                    echo '<span class="label label-danger">Không sử dụng được</span>';
                                                }
                                                ?>
                                            </td>
                                            <td><a href="<?php echo base_url('phong?mp=' . $row->MaPhong); ?>">Xem thiết bị</td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
