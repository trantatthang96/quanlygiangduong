<section id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-area">
                    <form role="form" method="post" action="<?php echo base_url('gopy') ?>" >
                        <br style="clear:both">
                        <h3 style="margin-bottom: 25px; margin-top: 0px; text-align: center;">Góp ý</h3>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nhập họ và tên" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Nhập vào email" required>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Nhập số điện thoại" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Nhập tiêu đề" required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" type="textarea" name="message" placeholder="Nội dung" maxlength="100" rows="5" required></textarea>

                        </div>
                        <span style="font-weight: bold; color: red"><?php echo $isSend ?></span>
                        <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right" style="margin-bottom: 25px;">Gởi góp ý</button>
                    </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
</section>



