<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center main-color-bg">
                        <i class="fa fa-desktop"></i> Loại thiết bị
                    </a>
                    <?php foreach ($loaitb as $row): ?>
                        <a href="<?php echo base_url('thietbi?maloaitb=') . $row->MaLoaiTB ?>" class="list-group-item"><?php echo $row->TenLoai ?>
                            <span class="badge"><?php echo $row->count ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-md-9">
                <div class="panel panel-info">
                    <div class="panel-heading hthietbi main-color-bg">
                        <form class="form-inline pull-left" action="<?php echo base_url('thietbi') ?>" accept-charset="UTF-8" method="get" style="margin-bottom: 0px;">
                            <input type="hidden" value="✓">
                            <div class="form-group">
                                <label>Tìm thiết bị </label>
                                <input type="search" name="search" id="search" value="" placeholder="Nhập tên thiết bị" class="form-control" spellcheck="false">
                            </div>
                            <button type="submit" class="btn btn-default" title="" style="padding-top: 9px;padding-bottom: 9px;">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                        <div class="btn-group pull-right">
                            <a href="#" id="list" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-th-list">
                                </span>List</a>
                            <a href="#" id="grid" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-th"></span>Grid</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div id="dsthietbi" class="row list-group">
                            <?php foreach ($thietbi as $row): ?>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img style="max-height: 150px !important; max-width: 250px !important;" class="group list-group-image" src="<?php echo base_url('upload/thietbi/' . $row->HinhDaiDien) ?>" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                <?php echo $row->TenTB ?></h4>
                                            <p class="group inner list-group-item-text">
                                                <?php echo $row->MoTa ?></p>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-6">
                                                    <p class="lead">
                                                    </p>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <button class="btn btn-success" data-toggle="modal" data-target="#btnChiTietThietBi" value="<?php echo $row->MaTB ?>" onclick="btnXemCTTB(this)">Xem chi tiết</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="panel-footer main-color-bg">
                        <div class="row">
                            <div class="col col-xs-4">
                            </div>
                            <div class="col col-xs-8">
                                <ul class="pagination hidden-xs pull-right">
                                    <?php if (isset($total_rows)) echo $this->pagination->create_links() ?>
                                </ul>
                                <ul class="pagination visible-xs pull-right">
                                    <li>
                                        <a href="#">«</a>
                                    </li>
                                    <li>
                                        <a href="#">»</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="btnChiTietThietBi" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Phòng được trang bị thiết bị</h4>
            </div>
            <div id="loadXemCTTB">
                <div id="sub">

                </div>
            </div>
        </div>
    </div>
</div>