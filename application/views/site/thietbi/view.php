<div id="loadXemCTTB">
    <div id="sub">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="Carousel" class="carousel slide carouseltb">
                        <ol class="carousel-indicators carousel-indicatorstb">
                            <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#Carousel" data-slide-to="1"></li>
                            <li data-target="#Carousel" data-slide-to="2"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php $image = json_decode($tb->HinhAnh); ?>
                                    <?php if (is_array($image)): ?>
                                        <?php foreach ($image as $img): ?>
                                            <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url('upload/thietbi/' . $img) ?>" alt="Image" style="max-width:100%;"></a></div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div><!--.row-->
                            </div><!--.item-->

                        </div><!--.carousel-inner-->
                        <a data-slide="prev" href="#Carousel" class="left carousel-control carousel-controltb">‹</a>
                        <a data-slide="next" href="#Carousel" class="right carousel-control carousel-controltb">›</a>
                    </div><!--.Carousel-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="tableThietBi">
                        <table class="table table-bordered tableShowNKPH">
                            <thead> 
                                <tr> 
                                    <th>STT</th> 
                                    <th class="nowrap">Mã phòng</th>
                                    <th class="nowrap">Tên phòng trang bị</th> 
                                    <th class="nowrap">Tình trạng</th>
                                    <th class="nowrap">Số lượng</th> 
                                </tr> 
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                <?php foreach ($phongtb as $rows): ?>
                                    <tr>
                                        <th scope="row"><?php echo $index ?></th> 
                                        <td class="text-center"><?php echo $rows->MaPhong ?></td> 
                                        <td class="text-center"><?php echo $rows->TenPhong ?></td> 
                                        <td class="text-center"><?php echo $rows->TTTB ?></td>
                                        <td class="text-center"><?php echo $rows->SoLuong ?></td> 
                                    </tr>
                                    <?php $index++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>