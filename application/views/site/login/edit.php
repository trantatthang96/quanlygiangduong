﻿<section id="main">
    <div class="container">
        <div class="row">
            <h3 class="page-header text-center" style="margin-top: 20px;"><strong>Thay đổi thông tin</strong></h3>
           <?php echo $message ?>
        </div>
        <div class="row">
            
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Họ và tên:</label>
                            <div class="col-md-6">
                                <input name="hotennd" type="text" class="form-control" id="hotennd"  value="<?php echo $info->HoTenND ?>">
                                <div name="name_error" class="clear error"><?php echo form_error('hotennd') ?></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputGioiTinh" class="col-md-4 control-label">Giới tính:</label>
                            <div class="col-md-6">
                                <select class="form-control" name="gioitinh">
                                    <option value="1" <?php echo ($info->GioiTinh == 1 ? 'selected' : '') ?>>Nam</option>
                                    <option value="0" <?php echo ($info->GioiTinh == 0 ? 'selected' : '') ?>>Nữ</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputNgaySinh" class="col-md-4 control-label">Ngày sinh:</label>
                            <div class="col-md-6">
                                <input name="ngaysinh" type="date" class="form-control" id="ngaysinh" value="<?php echo $info->NgaySinh ?>">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDienThoai" class="col-md-4 control-label">Điện thoại:</label>
                            <div class="col-md-6">
                                <input name="dienthoai" type="number" type="number" class="form-control" id="dienthoai"  value="<?php echo $info->DienThoai ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-md-4 control-label">Email:</label>
                            <div class="col-md-6">
                                <input name="email" type="email" class="form-control" id="email"  value="<?php echo $info->Email ?>"> 
                            </div>
                        </div>

            

                        <div class="form-group">
                            <label for="inputMatKhau" class="col-md-4 control-label">Mật khẩu:</label>
                            <div class="col-md-6">
                                <input name="password" type="password" class="form-control" id="password">
                                <div name="name_error" class="clear error"><?php echo form_error('password') ?></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Nhập lại mật khẩu:</label>
                            <div class="col-md-6">
                                <input name="re_password" type="password" class="form-control" id="repassword">
                                <div name="name_error" class="clear error"><?php echo form_error('re_password') ?></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button type="submit" class="btn btn-primary">Thay đổi</button>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <img width="320px" height="320px" class="img-rounded" src="<?php  echo base_url('upload/nguoidung/' . $user_info->HinhAnh) ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Chọn hình ảnh</label>
                        <input type="file" id="image" name="image">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>

