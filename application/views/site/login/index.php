<div class="jumbotron">
    <div class="container text-center">
        <h2><strong>TRƯỜNG ĐẠI HỌC NHA TRANG</strong></h2>
        <h3>Hệ thống quản lý trang thiết bị</h3>
        <!-- <p>
            <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
        </p> -->
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Đăng nhập vào hệ thống</h3>
                </div>
                <div class="panel-body">
                    <form role="form" id="form" action="" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Tên tài khoản" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Mật khẩu" name="password" type="password" value="">
                            </div>

                            <!-- Change this to a button or input when using this as a form -->
                            <h4 style="color: red; text-align: center"><?php echo form_error('login') ?></h4>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" >
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>