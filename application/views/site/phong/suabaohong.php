<div id="loadFormBaoHong">
    <div id="sub">
        <div class="modal-content">
            <div class="modal-header">
                <button id="btnBaoHongSended" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Cập nhật yêu cầu báo hỏng: <?php echo $thietbi->TenTB ?></h4>
            </div>
            <div class="modal-body">
                <div class="row form-horizontal">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tình trạng:</label>
                            <div class="col-md-6">
                                <select class="form-control" id="baohongTinhTrang">
                                    <?php foreach ($tinhtrangtb as $row): if ($row->MaTTTB != 1): ?>
                                            <option <?php echo ($row->MaTTTB == $listCTNK[0]->TinhTrang ? 'selected' : '' ) ?> value="<?php echo $row->MaTTTB ?>"><?php echo $row->TenTT ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Số lượng:</label>
                            <div class="col-md-6">
                                <input value="<?php echo $listCTNK[0]->SoLuong ?>" type="number" class="form-control" id="baohongSoLuong" min="1" max="<?php echo $phongtb->SoLuong ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Mô tả vị trí:</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="2" id="baohongMoTaVT"><?php echo $listCTNK[0]->MoTaViTri ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Mô tả:</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="2" id="baohongMoTa"><?php echo $listCTNK[0]->MoTa ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Ghi chú:</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" id="baohongGhiChu"><?php echo $phanhoi ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button type="submit" class="btn btn-primary" id="btnSendBaoHong">Cập nhật</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

