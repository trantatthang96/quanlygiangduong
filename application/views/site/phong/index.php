<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-list-ul"></i> Danh sách</div>
                    <div class="panel-body">
                        <!-- TREEVIEW CODE -->
                        <?php foreach ($giangduong as $row): ?>
                            <ul class="treeview">
                                <li>
                                    <a href="<?php echo base_url('giangduong/view?gd=' . $row->MaGD) ?>"><?php echo $row->TenGD ?></a>
                                    <ul>
                                        <?php if (count($row->subs) > 0): ?>
                                            <?php foreach ($row->subs as $sub): ?>
                                                <li>
                                                    <a href="<?php echo base_url('phong?mp=' . $sub->MaPhong) ?>"><?php echo $sub->TenPhong ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center main-color-bg">
                                <i class="fas fa-info-circle"></i> Chi tiết phòng học</div>
                            <table class="table table-bordered table-striped">
                                <colgroup>
                                    <col class="col-xs-5">
                                    <col class="col-xs-6"> </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-left">
                                            Giảng đường</th>
                                        <td id="layMaPhong"><?php echo $TenGD ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-left">
                                            Tên phòng</th>
                                        <td><?php echo $phong->TenPhong ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-left">
                                            Sức chứa</th>
                                        <td><?php echo $phong->SucChua ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-left">
                                            Tình trạng</th>
                                        <td><?php
                                            if ($phong->TinhTrang == 1)
                                                echo '<span class="label label-success">Sử dụng được</span>';
                                            else {
                                                echo '<span class="label label-danger">Không sử dụng được</span>';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-left">
                                            Nhân viên</th>
                                        <td><a href="<?php echo base_url('user') ?>"><?php echo $nhanvien->HoTenND ?></a></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center main-color-bg">
                                <i class="far fa-list-alt"></i> Nhật ký phản hồi
                            </div>
                            <div  id="tableNKPH">
                                <table class="table table-bordered tableShowNKPH">
                                    <thead> 
                                        <tr> 
                                            <th>STT</th> 
                                            <th class="nowrap">Tên thiết bị</th> 
                                            <th class="nowrap">Tình trạng</th>
                                            <th class="nowrap">Số lượng</th> 
                                            <th class="nowrap">Người gửi yêu cầu</th>
                                            <th class="nowrap">Thời gian duyệt</th> 
                                            <th class="nowrap">Trạng thái</th>
                                            <?php if ($this->session->userdata('user_id_login')): ?>
                                                <th class="nowrap text-center">Hành động</th>
                                            <?php endif; ?>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <?php $index = 1 ?>
                                        <?php foreach ($listNhatKyPH as $rows): ?>
                                            <tr> 
                                                <th scope="row"><?php echo $index ?></th> 
                                                <td><?php echo $rows->TenTB ?></td> 
                                                <td><?php echo $rows->TenTT ?></td> 
                                                <td><?php echo $rows->SoLuong ?></td>
                                                <td><?php echo $rows->HoTenND ?></td> 
                                                <td><?php
                                                    if ($rows->ThoiGianDuyet == 0)
                                                        echo 'Chưa cập nhật';
                                                    else
                                                        echo get_date($rows->ThoiGianDuyet)
                                                        ?>
                                                </td> 
                                                <td class="text-center"><?php
                                                    if ($rows->TrangThai == 1)
                                                        echo '<span class="label label-primary">Đã duyệt</span>';
                                                    elseif ($rows->TrangThai == 2) {
                                                        echo '<span class="label label-success">Đã sửa</span>';
                                                    } else {
                                                        echo '<span class="label label-danger">Chưa duyệt</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <?php if($this->session->userdata('user_id_login') == $rows->MaND && $rows->TrangThai == 0): ?>
                                                <td style="text-align: center">
                                                    <a type="button" onclick="btnEditBaoHong(this)"  data-toggle="modal" data-target="#modalBaoHong" value="<?php echo $rows->MaPTB.'|'.$rows->MaPH ?>"><i class="far fa-edit"></i></a>
                                                    <a type="button" onclick="btnDeleteBaoHong(this)" value="<?php echo $rows->MaPH ?>"><i class="fas fa-times"></i></a>
                                                </td>
                                                <?php else :?>
                                                <td></td>
                                                <?php endif; ?>
                                            </tr>
                                            <?php $index++; ?>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center main-color-bg">Danh sách thiết bị</div>
                            <table class="table table-bordered text-center tableShowDSTB" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Mã thiết bị</th>
                                        <th class="text-center">Tên thiết bị</th>
                                        <th class="text-center">Số lượng</th>
                                        <th class="text-center">Tình trạng</th>
                                        <th class="text-center">Ghi chú</th>
                                        <th class="text-center">Báo hỏng</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $index = 1 ?>
                                    <?php foreach ($dsthietbi as $row): ?>
                                        <tr>
                                            <th class="text-center" scope="row"><?php echo $index ?></th>
                                            <td><?php echo $row->MaTB ?></td>
                                            <td><?php echo $row->TenTB ?></td>
                                            <td><?php echo $row->SoLuong ?></td>
                                            <td><?php echo $row->TTTB ?></td>
                                            <td><?php echo $row->GhiChu ?></td>
                                            <td>
                                                <?php if ($quyen == 2): ?>
                                                    <button type="button" onclick="btnBaoHong(this)" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalBaoHong" value="<?php echo $row->MaPTB ?>">
                                                        <span class="glyphicon glyphicon-alert"></span>Báo hỏng thiết bị này
                                                    </button>
                                                <?php elseif ($quyen == 1): ?> 
                                                    <a type="button" class="btn btn-warning btn-xs">
                                                        <span class="glyphicon glyphicon-minus-sign"></span>Tài khoản không đủ quyền
                                                    </a>
                                                <?php else: ?>
                                                    <a type="button" class="btn btn-info btn-xs" href="<?php echo base_url('login') ?>">
                                                        <span class="glyphicon glyphicon-user"></span>Đăng nhập để sử dụng
                                                    </a>
                                                <?php endif; ?>
                                            </td>

                                        </tr>
                                        <?php $index++ ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalBaoHong" tabindex="-1" role="dialog" aria-labelledby="myModalLabel24">
    <div class="modal-dialog" role="document">
        <div id="loadFormBaoHong">
            <div id="sub">

            </div>
        </div>
    </div>
</div>

