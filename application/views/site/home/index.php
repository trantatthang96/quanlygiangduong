<section id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading main-color-bg">
                        <h3 class="panel-title text-center">Năm Học 2018-2019</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="<?php echo base_url('giangduong') ?>" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-building fa-5x" aria-hidden="true"></i>
                                    <br/> Giảng đường

                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="<?php echo base_url('thietbi') ?>" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-tv fa-5x" aria-hidden="true"></i>
                                    <br/> Thiết bị

                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="<?php echo base_url('user') ?>" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-users fa-5x" aria-hidden="true"></i>
                                    <br/> Nhân viên

                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="<?php echo base_url('baohongnhanh') ?>" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-bullhorn fa-5x" aria-hidden="true"></i>
                                    <br/> Báo hỏng nhanh

                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="<?php echo base_url('gopy') ?>" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-envelope fa-5x" aria-hidden="true"></i>
                                    <br/> Góp ý

                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 dash-box">
                                <a href="http://ntu.edu.vn/ttpvth/vi-vn/qu%E1%BA%A3ntr%E1%BB%8Bm%C3%B4itr%C6%B0%E1%BB%9Dng/c%C6%A1s%E1%BB%9Fv%E1%BA%ADtch%E1%BA%A5t.aspx?idcd=845" class="btn btn-sq-lg btn-info">
                                    <i class="fas fa-life-ring fa-5x" aria-hidden="true"></i>
                                    <br/> Hỗ trợ
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading main-color-bg">
                        <h3 class="panel-title text-center">Hình ảnh</h3>
                    </div>
                 
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?php echo base_url('upload/slide/a.jpg')?>" alt="Nha Trang 1" style="width:100%;">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url('upload/slide/b.jpg')?>" alt="Nha Trang 2" style="width:100%;">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url('upload/slide/c.jpg')?>" alt="Nha Trang 3" style="width:100%;">
                                </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
</section>

