<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-search"></i> Chọn phòng
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url('baohongnhanh') ?>" method="get">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Giảng đường</label>
                                <div class="col-sm-7">
                                    <select name="magd" class="form-control">
                                        <?php foreach ($giangduong as $row): ?>
                                            <option value="<?php echo $row->MaGD ?>" <?php echo ($this->input->get('magd') == $row->MaGD) ? 'selected' : ''; ?> ><?php echo $row->TenGD ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <?php if ($this->input->get('magd')): ?>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Chọn phòng</label>
                                    <div class="col-sm-7">
                                        <select name="maphong" class="form-control">
                                            <?php foreach ($phonghoc as $row): ?>
                                                <option value="<?php echo $row->MaPhong ?>" <?php echo ($this->input->get('maphong') == $row->MaPhong) ? 'selected' : ''; ?> ><?php echo $row->TenPhong ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div for="inputGD" class="col-sm-4 control-label"></div>
                                    <div class="col-sm-7">
                                        <button id="btnSearchGD" type="submit" class="btn btn-success">Chọn xong phòng học</button>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="form-group">
                                    <div for="inputGD" class="col-sm-4 control-label"></div>
                                    <div class="col-sm-7">
                                        <button id="btnSearchGD" type="submit" class="btn btn-success">Chọn xong giảng đường</button>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
                </div>
            </div>
            <?php if (isset($dsthietbi)): ?>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center main-color-bg">Danh sách thiết bị</div>
                        <table class="table table-bordered text-center" style="font-size: 13px;">
                            <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th class="text-center">Mã thiết bị</th>
                                    <th class="text-center">Tên thiết bị</th>
                                    <th class="text-center">Số lượng</th>
                                    <th class="text-center">Báo hỏng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                <?php foreach ($dsthietbi as $row): ?>
                                    <tr>
                                        <th class="text-center" scope="row"><?php echo $index ?></th>
                                        <td><?php echo $row->MaTB ?></td>
                                        <td><?php echo $row->TenTB ?></td>
                                        <td><?php echo $row->SoLuong ?></td>
                                   
                                        <td>
                                            <?php if ($quyen == 2): ?>
                                                <button type="button" onclick="btnBaoHong(this)" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalBaoHong" value="<?php echo $row->MaPTB ?>">
                                                    <span class="glyphicon glyphicon-alert"></span>Báo hỏng thiết bị này
                                                </button>
                                            <?php elseif ($quyen == 1): ?> 
                                                <a type="button" class="btn btn-warning btn-xs">
                                                    <span class="glyphicon glyphicon-minus-sign"></span>Tài khoản không đủ quyền
                                                </a>
                                            <?php else: ?>
                                                <a type="button" class="btn btn-info btn-xs" href="<?php echo base_url('login') ?>">
                                                    <span class="glyphicon glyphicon-user"></span>Đăng nhập để sử dụng
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php $index++ ?>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modalBaoHong" tabindex="-1" role="dialog" aria-labelledby="myModalLabel24">
    <div class="modal-dialog" role="document">
        <div id="loadFormBaoHong">
            <div id="sub">

            </div>
        </div>
    </div>
</div>

