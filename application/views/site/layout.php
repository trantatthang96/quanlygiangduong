<html>
    <head>
        <?php $this->load->view('site/head') ?>
    </head>
    <body>
        <?php $this->load->view('site/header', $this->data) ?> 
        <?php $this->load->view($temp, $this->data); ?>
        <footer class="footer text-center">
            <?php $this->load->view('site/footer') ?>
        </footer>
        <script src="<?php echo public_url() ?>pages/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo public_url() ?>pages/js/bootstrap.min.js"></script>
        <script src="<?php echo public_url() ?>pages/js/main.js"></script>
    </body>
</html>

