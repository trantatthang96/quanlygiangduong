﻿<div class="jumbotron">
    <div class="container text-center">
        <h2><strong>TRƯỜNG ĐẠI HỌC NHA TRANG</strong></h2>
        <h3>Hệ thống quản lý trang thiết bị giảng đường</h3>
        <!-- <p>
            <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
        </p> -->
    </div>
</div>
<div class="container">
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                        aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="<?php echo base_url('home') ?>">NTU</a>
                
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo base_url('home') ?>">Trang chủ</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Thông tin
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url('giangduong') ?>">Giảng đường</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('thietbi') ?>">Thiết bị</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('user') ?>">Nhân viên</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('timphong') ?>">Tìm phòng</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('lienhe') ?>">Liên hệ</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('huongdan') ?>">Hướng dẫn</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li class="active">
                        <a href="./">Default
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>-->
                    <?php if (isset($user_info)): ?>
                        <li>
                            <a href="<?php echo base_url('home/edit') ?>" class="text-uppercase">Tài khoản, <u><strong><?php echo $user_info->HoTenND ?></strong></u></a>
                        </li>
                      
                        <li>
                            <a href="<?php echo base_url('home/logout') ?>" class="" style=""><i class="fas fa-times-circle"></i> <strong>Thoát</strong></a>
                        </li>
                        
                        <?php if($user_info->MaLoaiND == '1' || $user_info->MaLoaiND == '2'):?>
                        <li>
                            <a href="<?php echo base_url('admin/home') ?>" class="" style=""><i class="far fa-calendar-check"></i> <strong>Quản trị</strong></a>
                        </li>
                        <?php endif;?>
                        
                    <?php else: ?>
                        <li>
                            <a href = "<?php echo base_url('login') ?>"><i class="fas fa-user"></i><strong> <u>Đăng nhập</u></strong></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->


</div>
<!-- /container -->
