<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h3>ĐỊA CHỈ</h3>
            <p>Số 02 - Nguyễn Đình Chiểu</p>
            <p>Nha Trang - Khánh Hòa</p>
        </div>
        <div class="col-md-4">
            <h3>LIÊN KẾT</h3>
            <ul class="list-inline">
                <li>
                    <a class="btns" href="https://www.facebook.com/infotechntu/">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a class="btns" href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a class="btns" href="https://www.youtube.com/channel/UCvED85BrJvtF2dfIDMjLmfQ">
                        <i class="fab fa-youtube"></i>
                    </a>
                </li>
                <li>
                    <a class="btns" href="#">
                        <i class="fab fa-google"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <h3>LIÊN HỆ</h3>
            <p>
                <i class="fas fa-phone"></i> Phone: 058 3 831 149</p>
            <p>
                <i class="far fa-envelope"></i> quanlygiangduong@ntu.edu.vn</p>
        </div>
    </div>
    <div class="row text-center">
        Copyright © 2018 - Developed by Khoa CNTT- Đại học Nha Trang
    </div>
</div>

