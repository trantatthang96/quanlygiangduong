﻿<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center main-color-bg">
                        <i class="fas fa-user-circle"></i> Loại người dùng
                    </a>
                    <?php foreach ($loainguoidung as $row): ?>
                        <?php if ($row->MaLoaiND != 1): ?>
                            <a href="<?php echo base_url('user/type?loai=' . $row->MaLoaiND) ?>" class="list-group-item"><?php echo $row->TenLoai ?>
                                <span class="badge"><?php echo $row->count ?></span>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-md-9">

                <div class="panel panel-info filterable">
                    <div class="panel-heading main-color-bg">
                        <h3 class="panel-title">Danh sách người dùng</h3>
                        <div class="pull-right">
                            <button class="btn btn-default btn-xs btn-filter">
                                <span class="glyphicon glyphicon-filter"></span> Lọc</button>
                        </div>
                    </div>
                    <table class="table tnhanvien">
                        <thead>
                            <tr class="filters">
                                <th style="width: 70px;">
                                    <input type="text" class="form-control text-center" placeholder="STT" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control" placeholder="Hình ảnh" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control" placeholder="Tên người dùng" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control" placeholder="Chức vụ" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control" placeholder="Điện thoại" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control text-center" placeholder="Thêm" disabled>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 1 ?>
                            <?php foreach ($nguoidung as $row): ?>
                                <?php if ($row->MaLoaiND != 1): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index ?></td>
                                        <td>
                                            <span class="chat-img pull-left">
                                                <img style="height: 60px; width: 60px;" class="img-circle" alt="User Avatar" src="<?php echo base_url('upload/nguoidung/' . $row->HinhAnh) ?>">
                                            </span>
                                        </td>
                                        <td><?php echo $row->HoTenND ?></td>
                                        <td><?php echo $row->ChucVu ?></td>
                                        <td><?php echo $row->DienThoai ?></td>
                                        <td class="text-center">
                                            <button id="btnXemCTNV" type="button" onclick="xemChiTietND(this)" class="btn btn-info btn-xs" data-toggle="modal" data-target="#btnChiTiet" value="<?php echo $row->MaND ?>">
                                                <span class="glyphicon glyphicon-edit"></span>Xem chi tiết
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $index++ ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="btnChiTiet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Chi tiết nhân viên</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="infoNguoiDung">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
