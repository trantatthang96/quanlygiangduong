<section id="main">
    <div class="" id="lienhe">
        <section id="team" class="pb-5">
            <div class="container">
                <h5 class="section-title h1">Liên hệ</h5>
                <div class="row">
                    <!-- Team member -->
                    <?php foreach ($nhanvien as $row) : ?>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p><img class=" img-fluid" src="<?php echo base_url('upload/nguoidung/' . $row->HinhAnh) ?>" alt="card image"></p>
                                                <h4 class="card-title"><?php echo $row->HoTenND ?></h4>
                                                <p class="card-text">Nhân viên phải làm việc chăm chỉ.</p>
                                                <a href="#"><span class="label label-primary">Phụ trách giảng đường <?php echo $row->TenGD ?></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="backside">
                                        <div class="card">
                                            <div class="card-body text-center mt-4">
                                                <h4 class="card-title">Thông tin</h4>
                                                <ul style="text-align: left; list-style-type: none; color: white; font-size: 18px;">
                                                    <li>
                                                        <i class="fas fa-birthday-cake"> <?php echo date("d-m-Y", strtotime($row->NgaySinh)) ?></i>
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-phone-square"> <?php echo $row->DienThoai ?></i>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-envelope"> <?php echo $row->Email ?></i>
                                                    </li>
                                                </ul>
                                                <p class="card-text"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </section>
    </div>
</div>
</section>



