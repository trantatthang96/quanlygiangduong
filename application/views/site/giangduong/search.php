<div id="loadView">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading text-center main-color-bg">Danh sách tìm được</div>
                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Mã phòng</th>
                            <th class="text-center">Tên phòng</th>
                            <th class="text-center">Sức chứa</th>
                            <th class="text-center">Tình trạng</th>
                            <th class="text-center">Danh sách thiết bị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($list_ph as $row): ?>
                            <tr>
                                <th class="text-center" scope="row"><?php echo $i ?></th>
                                <td><?php echo $row->MaPhong ?></td>
                                <td><?php echo $row->TenPhong ?></td>
                                <td><?php echo $row->SucChua ?></td>
                                <td><?php echo $row->TinhTrang ?></td>
                                <td>
                                    <button id="btnGiangDuongTB" onclick="loadTB(this)" value="<?php echo $row->MaPhong ?>" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#btnListTB">
                                        Xem
                                    </button>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
