<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-list-ul"></i> Danh sách</div>
                    <div class="panel-body">
                        <!-- TREEVIEW CODE -->
                        <?php foreach ($giangduong as $row): ?>
                            <ul class="treeview">
                                <li>
                                    <a href="<?php echo base_url('giangduong/view?gd=' . $row->MaGD) ?>"><?php echo $row->TenGD ?></a>
                                    <ul>
                                        <?php if (count($row->subs) > 0): ?>
                                            <?php foreach ($row->subs as $sub): ?>
                                                <li>
                                                    <a href="<?php echo base_url('phong?mp=' . $sub->MaPhong) ?>"><?php echo $sub->TenPhong ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-search"></i> Tìm kiếm
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputMP" class="col-sm-5 control-label">Mã phòng</label>
                                <div class="col-sm-7">
                                    <input id="inputMaPhong" type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTP" class="col-sm-5 control-label">Tên phòng</label>
                                <div class="col-sm-7">
                                    <input id="inputTenPhong" type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGD" class="col-sm-5 control-label">Thuộc khu</label>
                                <div class="col-sm-7">
                                    <select name="magd" class="form-control" id="selGiangDuong">
                                        <option value=""</option>
                                        <?php foreach ($giangduong as $row): ?>
                                            <option value="<?php echo $row->MaGD ?>" <?php //echo ($MaGD == $row->MaGD) ? 'selected' : '';    ?> ><?php echo $row->TenGD ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSC" class="col-sm-5 control-label">Sức chứa</label>
                                <div class="col-sm-7">
                                    <input type="number" class="form-control" id="inputSucChua" placeholder="" min="1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-5 control-label">Trạng thái</label>
                                <div class="col-sm-7">
                                    <select name="magd" class="form-control" id="selTinhTrang">
                                        <option value=""></option>
                                        <option value="1">Sử dụng được</option>
                                        <option value="2">Không sử dụng được</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-1 col-sm-4">
                                    <button type="submit" class="btn btn-default">Nhập lại</button>
                                </div>
                                <div class="col-sm-7">
                                    <button id="btnSearchGD" type="submit1" class="btn btn-success">Tìm</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div id="loadView">
                    <div id="sub">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="btnListTB" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Danh sách thiết bị</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered smallfont"> 
                    <thead> 
                        <tr> 
                            <th class="text-center">STT</th> 
                            <th class="text-center">Mã thiết bị</th> 
                            <th class="text-center">Tên thiết bị</th> 
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Tình trạng</th>
                        </tr> 
                    </thead> 
                    <tbody id='loadTablePTB'>
                    </tbody> 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-primary">Xem nhật ký thiết bị</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->