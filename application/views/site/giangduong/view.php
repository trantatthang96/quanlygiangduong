<div id="loadView">
    <div id="sub">
        <div class="row">
            <div class="col-md-7">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-info-circle"></i> Thông tin giảng đường G1</div>
                    <table class="table table-bordered table-striped">
                        <colgroup>
                            <col class="col-xs-4">
                            <col class="col-xs-6"> </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row" class="text-center">
                                    Mã giảng đường</th>
                                <td><?php echo $info->MaGD ?></td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-center">
                                    Tên giảng đường</th>
                                <td><?php echo $info->TenGD ?></td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-center">
                                    Số tầng</th>
                                <td><?php echo $info->SoTang ?></td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-center">
                                    Số phòng</th>
                                <td><?php echo count($list) ?></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">
                        <i class="fas fa-image"></i> Hình ảnh</div>

                    <img style="width: 100%; max-height: 200px;" class="" src="<?php echo base_url('upload/giangduong/' . $info->HinhAnh) ?>">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading text-center main-color-bg">Danh sách phòng học</div>
                    <table class="table table-bordered text-center">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Mã phòng</th>
                                <th class="text-center">Tên phòng</th>
                                <th class="text-center">Sức chứa</th>
                                <th class="text-center">Tình trạng</th>
                                <th class="text-center">Thiết bị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <th class="text-center" scope="row"><?php echo $i ?></th>
                                    <td><?php echo $row->MaPhong ?></td>
                                    <td><?php echo $row->TenPhong ?></td>
                                    <td><?php echo $row->SucChua ?></td>
                                    <td><?php
                                        if ($row->TinhTrang == 1)
                                            echo '<span class="label label-success">Sử dụng được</span>';
                                        else {
                                            echo '<span class="label label-danger">Không sử dụng được</span>';
                                        }
                                        ?>
                                    </td>
                                    <td><a href="<?php echo base_url('phong?mp=' . $row->MaPhong); ?>">Xem thiết bị</td>
                                </tr>
                                <?php $i++; ?>
<?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
