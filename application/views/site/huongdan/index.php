<section id="main">
    <div class="" id="lienhe">
        <section id="team" class="pb-5">
            <div class="container">
                <h5 class="section-title h1">Hướng dẫn</h5>
                <h6 class="section-title h3" style="text-align: left; margin-bottom: 20px">Xem thông tin</h6>
                <p style="font-size: 18px">Chọn thẻ thông tin trên thanh menu sau đó chọn vào danh mục thông tin cần xem: Giảng đường, Thiết bị và Nhân viên</p>
                <p style="font-size: 18px; padding-left: 30px">Danh mục giảng đường: chọn vào giảng đường muốn xem, phần mềm sẽ hiển thị danh sách các phòng học trong từng giảng đường. Chọn vào 1 phòng học để xem chi tiết thông tin của phòng học đó.</p>
                <p style="font-size: 18px; padding-left: 30px">Danh mục thiết bị: Xem thông tin của tất cả các thiết bị có trong tất cả các giảng đường</p>
                <p style="font-size: 18px; padding-left: 30px">Danh mục nhân viên: Xem thông tin cơ bản của các nhân viên và giảng viên.</p>
                <h6 class="section-title h3" style="text-align: left; margin-bottom: 20px">Tìm phòng</h6>
                <p style="font-size: 18px">Để sử dụng chức năng tìm phòng, người sử dụng chọn vào thẻ Tìm phòng trên thanh menu sau đó nhập vào các tiêu chí tìm phòng đã được cố định sẵn.</p>
                <h6 class="section-title h3" style="text-align: left; margin-bottom: 20px">Báo hỏng thiết bị</h6>
                <p style="font-size: 18px; margin-bottom: 100px;">Để sử dụng chức năng này người sử dụng có thể vào từng phòng học để báo hỏng thiết bị tương ứng hoặc sử dụng nút lệnh Báo hỏng nhanh. Chức năng này yêu cầu phải đăng nhập. Ở đây người sử dụng chọn thông tin tương ứng với thiết bị hỏng để gửi yêu cầu báo hỏng lên nhân viên phụ trách giảng đường duyệt.</p>
            </div>
        </section>
    </div>
</div>
</section>



