<?php
Class Phong_thietbi extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('phong_thietbi_model');
    }

    function index() {
        $this->load->model('phong_model');
        $this->load->model('thietbi_model');
        $this->load->model('tinhtrangthietbi_model');

        $showAll = false;
        $input = array();
        $where = array();

        $total = $this->phong_thietbi_model->get_total();
        $this->data['total'] = $total;

        $maphong = $this->input->get('maphong');
        if ($maphong) {
            $where['phong-thietbi.MaPhong'] = $maphong;
            $this->data['maphong'] = $maphong;
        } else{
            $this->data['maphong'] = '';
        }
        
        //Tình trạng thiết bị
        // $matttb = $this->input->get('matttb');
        // if ($matttb) {
        //     $where['phong-thietbi.MaTTTB'] = $matttb;
        //     $this->data['matttb'] = $matttb;
        // } else{
        //     $this->data['matttb'] = '';
        // }
        
        $matb = $this->input->get('matb');
        if ($matb) {
            $where['phong-thietbi.MaTB'] = $matb;
            $this->data['matb'] = $matb;
        } else{
            $this->data['matb'] = '';
        }

        $input['where'] = $where;

        //Phan trang.
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = admin_url('phong_thietbi/index');
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $this->pagination->initialize($config);

        //load du lieu

        $segment = $this->uri->segment(4);
        $segment = intval($segment);

        if(!$maphong && !$matb)
        {
            $showAll = true;
            $input['limit'] = array($config['per_page'], $segment);
        }

        $list = $this->phong_thietbi_model->getList_Where($input);

        foreach ($list as $row) {          
            if(!empty($row->TTTB))
            {
                $s = "";
                $arr_temp = json_decode($row->TTTB);
                foreach ($arr_temp as $key => $value) {
                    $tttb = $this->tinhtrangthietbi_model->get_info($key);
                    $s .= $tttb->TenTT . ':' . $value . '<br/>';
                }
                $row->TTTB = $s;
            }           
        }

        $this->data['list'] = $list;
        $this->data['showAll'] = $showAll;

        //Lấy ra các danh sách cho các thẻ select trong form lọc data
        $input_p = array();
        $list_p = $this->phong_model->get_list($input_p);
        $this->data['list_p'] = $list_p;

        $input_tb = array();
        $list_tb = $this->thietbi_model->get_list($input_tb);
        $this->data['list_tb'] = $list_tb;

        // $input_tttb = array();
        // $list_tttb = $this->tinhtrangthietbi_model->get_list($input_tttb);
        // $this->data['list_tttb'] = $list_tttb;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/phong_thietbi/index';
        $this->load->view('admin/main', $this->data);
    }

     function check_key() {
        $maphong = $this->input->post('maphong');
        $matb = $this->input->post('matb');
        $where = array('MaPhong' => $maphong, 'MaTB' => $matb);
        if ($this->phong_thietbi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Thiết bị đã tồn tại trong phòng');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('maphong', 'Mã phòng', 'required | call_back_check_key');
            $this->form_validation->set_rules('matb', 'Mã thiết bị', 'required | call_back_check_key');
            $this->form_validation->set_rules('soluong', 'Sức chứa', 'numeric | required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaPhong = $this->input->post('maphong');
                $MaTB = $this->input->post('matb');
                //$TTTB = $this->input->post('matttb');
                $SoLuong = $this->input->post('soluong');
                $GhiChu = $this->input->post('ghichu');

                $this->load->library('upload_library');
                $upload_path = './upload/phong-thietbi';
                $upload_data = $this->upload_library->upload($upload_path, 'image');

                $TTTB = array();
                $TTTB['1'] = $SoLuong;

                $data = array(
                    'MaPhong' => $MaPhong,
                    'MaTB' => $MaTB,
                    'TTTB' => json_encode($TTTB),
                    'SoLuong' => $SoLuong,
                    'GhiChu' => $GhiChu,
                );

                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                    $data['HinhAnh'] = $image;
                }
                //pre($data);
                if ($this->phong_thietbi_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm được');
                redirect(admin_url("phong_thietbi"));
            }
        }

        //Lấy danh sách phòng
        $this->load->model('phong_model');
        $listp = $this->phong_model->get_list();
        $this->data['listp'] = $listp;

        //Lấy danh sách thiết bị
        $this->load->model('thietbi_model');
        $listtb = $this->thietbi_model->get_list();
        $this->data['listtb'] = $listtb;

        //Lấy danh sách tình trạng thiết bị
        $this->load->model('tinhtrangthietbi_model');
        $listttb = $this->tinhtrangthietbi_model->get_list();
        $this->data['listttb'] = $listttb;

        $this->data['temp'] = 'admin/phong_thietbi/add';    
        $this->load->view('admin/main', $this->data);
    }

    function delThietBi()
    {

        $id = $this->uri->rsegment('3');

        $info = $this->phong_thietbi_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('phong_thietbi'));
        }

        $this->phong_thietbi_model->delete_p_tb($id);
        $this->session->set_flashdata('message', 'Xóa thiết bị thành công');
        redirect(admin_url('phong_thietbi'));
    }
}