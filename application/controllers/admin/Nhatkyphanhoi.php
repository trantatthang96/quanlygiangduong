<?php

Class Nhatkyphanhoi extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('nhatkyphanhoi_model');
    }

    function index() {
        $this->load->model('nguoidung_model');

        $showAll = false;
        $where = array();

        $maph = $this->input->get('maph');
        if ($maph) {
            $where['MaPH like'] = $maph;
        }
        
        $magvph = $this->input->get('magvph');
        if ($magvph) {
            $where['nhatkyphanhoi.MaGVPH'] = $magvph;
            $this->data['magvph'] = $magvph;
        } else{
            $this->data['magvph'] = '';
        }
        
        $manvduyet = $this->input->get('manvduyet');
        if ($manvduyet) {
            $where['nhatkyphanhoi.MaNVDuyet'] = $manvduyet;
            $this->data['manvduyet'] = $manvduyet;
        } else{
            $this->data['manvduyet'] = '';
        }

        ///////////////////////////////////////pagination
        $total_rows = $this->nhatkyphanhoi_model->get_total();
        $this->data['total_rows'] = $total_rows;

        //load ra thu vien phan trang
        $this->load->library('pagination');
        $config = array();
        $config['total_rows'] = $total_rows; //tong tat ca cac bài viết tren website
        $config['base_url'] = admin_url('nhatkyphanhoi/index'); //link hien thi ra danh sach bài viết
        $config['per_page'] = 10; //so luong bài viết hien thi tren 1 trang
        $config['uri_segment'] = 4; //phan doan hien thi ra so trang tren url
        $config['next_link'] = 'Trang kế tiếp';
        $config['prev_link'] = 'Trang trước';
        $this->pagination->initialize($config);
        $segment = $this->uri->segment(4);
        $segment = intval($segment);
       // pre($segment);
        $input = array();
        $input['where'] = $where;

        if(!$maph && !$magvph &&!$manvduyet)
        {
            $showAll = true;
            $input['limit'] = array($config['per_page'], $segment);
        }

        $list = $this->nhatkyphanhoi_model->get_List_Join($input);
        
        foreach($list as $row)
        {
            if($row->ThoiGianGoi == null)
                $row->ThoiGianGoi = '';
            else $row->ThoiGianGoi = get_date($row->ThoiGianGoi);
        }

        $this->data['list'] = $list;
        $this->data['showAll'] = $showAll;

        $input_nd = array();
        $list_nd = $this->nguoidung_model->get_list($input_nd);
        $this->data['list_nd'] = $list_nd;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/nhatkyphanhoi/index';
        $this->load->view('admin/main', $this->data);
    }
    
    
    function delete() {
        $id = $this->uri->rsegment('3');
        $this->_del($id);
        
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('dexuat'));
    }


    private function _del($id, $redirect = true) {
        $this->load->model('chitietnhatkyphanhoi_model');

        $info = $this->nhatkyphanhoi_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại phản hồi này');
            if ($redirect) {
                redirect(admin_url('nhatkyphanhoi'));
            }else{
                return false;
            }          
        }      

        $input['where'] = array('MaPH' => $id);
        $CTNKPH = $this->chitietnhatkyphanhoi_model->get_list($input);
        if($CTNKPH){
            foreach ($CTNKPH as $row) {  
                $this->chitietnhatkyphanhoi_model->delete_key($row->MaNKPH);
            }
        }

        $this->nhatkyphanhoi_model->delete_key($id);

        redirect(admin_url('nhatkyphanhoi'));
    }

    function check_MaPH() {
        $maph = $this->input->post('maph');
        $where = array('MaPH' => $maph);
        if ($this->nhatkyphanhoi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('nguoidung_model');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            //them vao csdl
            $MaGVPH = $this->session->userdata('login');
            $MaNVDuyet = $this->input->post('manvduyet');
            $ThoiGianGui = now();
            $GhiChu = $this->input->post('ghichu');

            $data = array(
                'MaGVPH' => $MaGVPH,
                'MaNVDuyet' => $MaNVDuyet,
                'ThoiGianGoi' => $ThoiGianGui,
                'GhiChu' => $GhiChu,
            );
                   
            if ($this->nhatkyphanhoi_model->create($data))
                $this->session->set_flashdata('message', 'Thêm mới thành công');
            else
                $this->session->set_flashdata('message', 'Không thêm đươc');

            redirect(admin_url('nhatkyphanhoi'));
            
        }


        //Thêm dữ liệu cho list box chọn nhân viên quản lí, show hết luôn, có điều kiện gì thì mấy chú thêm sau v
        $input = array();
        $list_nd = $this->nhatkyphanhoi_model->getNVQL();
        $this->data['list_nd'] = $list_nd;

        $this->data['temp'] = 'admin/nhatkyphanhoi/add';
        $this->load->view('admin/main', $this->data);
    } 


    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('nguoidung_model');

        $id = $this->uri->rsegment('3');

        $info =  $this->nhatkyphanhoi_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại phản hồi này');
            redirect(admin_url('nhatkyphanhoi'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {
            $this->form_validation->set_rules('maph', 'Mã phản hồi', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaPH = $this->input->post('maph');
                $MaGVPH = $this->input->post('magvph');
                $MaNVDuyet = $this->input->post('manvduyet');
                $ThoiGianGui = $info->ThoiGianGoi;
                $GhiChu = $this->input->post('ghichu');

                $ThoiGianGui = strtotime($ThoiGianGui);

                $data = array(
                    'MaGVPH' => $MaGVPH,
                    'MaNVDuyet' => $MaNVDuyet,
                    'ThoiGianGoi' => $ThoiGianGui,
                    'GhiChu' => $GhiChu,
                );

                if ($this->nhatkyphanhoi_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('nhatkyphanhoi'));
           }
        }

        $input = array();
        $list_nd = $this->nguoidung_model->get_list($input);
        $this->data['list_nd'] = $list_nd;

        $this->data['temp'] = 'admin/nhatkyphanhoi/edit';
        $this->load->view('admin/main', $this->data);
    }  

    function details() {
        $this->load->model('chitietnhatkyphanhoi_model');
        $this->load->model('phong_model');
        $this->load->model('thietbi_model');

        $where = array();

        $maph = $this->uri->rsegment('3');
        //Kiểm tra xem có tồn tại phản hồi được gửi tới qua link không
        if ($maph) {
            $where['nhatkyphanhoi.MaPH'] = $maph;
            $this->data['maph'] = $maph;
        }
        else{
            $this->session->set_flashdata('message', 'Không tìm thấy phản hồi có mã '. $maph);
        }

        $mankph = $this->input->get('mankph');
        if ($mankph) {
            $where['MaNKPH like'] = $mankph;
        }
        
        $maphong = $this->input->get('maphong');
        if ($maphong) {
            $where['phong.MaPhong'] = $maphong;
            $this->data['maphong'] = $maphong;
        } else{
            $this->data['maphong'] = '';
        }
        
        $matb = $this->input->get('matb');
        if ($matb) {
            $where['thietbi.MaTB'] = $matb;
            $this->data['matb'] = $matb;
        } else{
            $this->data['matb'] = '';
        }

        $input['where'] = $where;

        $user_id_logined = $this->session->userdata('login');
        $user = $this->nguoidung_model->get_info($user_id_logined);
        $listdb = $this->chitietnhatkyphanhoi_model->get_list_mulTable($input);
        

        $user_id_logined = $this->session->userdata('login');
        // $list = $this->chitietnhatkyphanhoi_model->get_list_mulTable($input);

        foreach($listdb as $row){
            $MaNVDuyet = $this->nhatkyphanhoi_model->get_info($row->MaPH)->MaNVDuyet;
            if($row->ThoiGianDuyet == null)
                $row->ThoiGianDuyet = '';
            else $row->ThoiGianDuyet = get_date($row->ThoiGianDuyet);

            //Xét khả năng duyệt của Admin và nhân viên sửa chữa
            if($this->data['isAdmin'] || $user->MaLoaiND == 4) {
                if($row->TrangThai == 0)
                    $row->Duyet = true;
                else $row->Duyet = false;

                if($row->TrangThai == 1)
                    $row->SuaChua = true;
                else $row->SuaChua = false;

                if($row->TrangThai == 2)
                {
                    if($this->data['isAdmin'])
                        $row->Xoa = true;
                    else 
                        $row->Xoa = false;
                }
                else $row->Xoa = true;
            } else {
                //Nhân viên trực giảng đường và giáo viên, người dùng còn lại
                if($row->TrangThai == 0 && $MaNVDuyet == $user_id_logined)
                    $row->Duyet = true;
                else $row->Duyet = false;

                //Xét cả năng cập nhật khi sửa chữa của user
                //Không cho nhân viên trực giảng đường sửa chữa, chỉ cho duyệt
                if($row->TrangThai == 1 && $MaNVDuyet == $user_id_logined)
                    $row->SuaChua = false;
                else $row->SuaChua = false;

                if($row->TrangThai != 2 && $MaNVDuyet == $user_id_logined)
                    $row->Xoa = true;
                else $row->Xoa = false;
            }
            
        }

        $this->data['listdb'] = $listdb;
        $this->data['user'] = $user;

        //pre($listdb);
        $input_p_tb = array();
        $list_p = $this->phong_model->get_list($input_p_tb);
        $this->data['list_p'] = $list_p;

        $list_tb = $this->thietbi_model->get_list($input_p_tb);
        $this->data['list_tb'] = $list_tb;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/nhatkyphanhoi/details';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaNKPH() {

        $mankph = $this->input->post('mankph');
        $where = array('MaNKPH' => $mankph);
        if ($this->chitietnhatkyphanhoi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function addCTNKPH(){
        $this->load->model('phong_thietbi_model');
        $this->load->model('chitietnhatkyphanhoi_model');
        $this->load->model('tinhtrangthietbi_model');

        $id_PH = $this->uri->rsegment('3');
       
        //Lấy thông tin quản trị viên        

        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('soluong', 'Số lượng thiết bị', 'numeric|required');
            $this->form_validation->set_rules('motavitri', 'Mô tả vị trí thiết bị hỏng', 'required');
            $this->form_validation->set_rules('mota', 'Mô tả sơ lược', 'required');

            if ($this->form_validation->run()) {

                $MaTB = $this->input->post('matb');
                $MaPhong = $this->input->post('maphong');

                $p_tb = $this->phong_thietbi_model->getListWhere(array('phong-thietbi.MaPhong' => $MaPhong, 'phong-thietbi.MaTB' => $MaTB));

                if(!$p_tb->MaPTB) 
                {
                    $this->session->set_flashdata('message', 'Thiết bị bạn phản hồi không tồn tại trong phòng');
                    redirect(admin_url('nhatkyphanhoi/details/'.$id_PH));
                }

                $soLuong = json_decode($p_tb->TTTB, true);

                $MaPTB = $p_tb->MaPTB;

                $TinhTrang = $this->input->post('tinhtrang');
                $SoLuong = $this->input->post('soluong');
                $MoTaViTri = $this->input->post('motavitri');
                $MoTa = $this->input->post('mota');
                $ThoiGianDuyet = null;
                $TrangThai = 0;

                $data = array(
                    'MaPTB' => $MaPTB,
                    'MaPH' => $id_PH,
                    'SoLuong' => $SoLuong,
                    'TinhTrang' => $TinhTrang,
                    'MoTaViTri' => $MoTaViTri,
                    'MoTa' => $MoTa,
                    'TrangThai' => $TrangThai,
                    'ThoiGianDuyet' => $ThoiGianDuyet
                );

                if($SoLuong > $soLuong[1])
                    $this->session->set_flashdata('message', 'Số lượng bạn phản hồi lớn hơn số lượng thiết bị có trạng thái tốt trong phòng ');
                else
                {
                    if ($this->chitietnhatkyphanhoi_model->create($data))
                        $this->session->set_flashdata('message', 'Thêm mới thành công');
                    else
                        $this->session->set_flashdata('message', 'Không thêm đươc');
                }

                redirect(admin_url('nhatkyphanhoi/details/'.$id_PH));
  
            }
        }

        $this->data['id'] = $id_PH;

        $input = array();
        $list_p = $this->phong_thietbi_model->getListPhong();
        $this->data['list_p'] = $list_p;

        $list_tb = $this->phong_thietbi_model->getListTB();
        $this->data['list_tb'] = $list_tb;

        $list_tttb = $this->tinhtrangthietbi_model->get_list();
        $this->data['list_tttb'] = $list_tttb;

        $this->data['temp'] = 'admin/nhatkyphanhoi/addCTNKPH';
        $this->load->view('admin/main', $this->data);
    }

    function delete_NKPH() {
        $this->load->model('chitietnhatkyphanhoi_model');
        $id_PH = $this->uri->rsegment('3');
        $id_NKPH = $this->uri->rsegment('4');
        $this->chitietnhatkyphanhoi_model->delete_key($id_NKPH);       
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('nhatkyphanhoi/details/'.$id_PH));
    }

    function editCTNKPH(){ 
        $this->load->model('phong_thietbi_model');
        $this->load->model('chitietnhatkyphanhoi_model');
        $this->load->model('tinhtrangthietbi_model');

        $id_PH = $this->uri->rsegment('3');

        $info = $this->chitietnhatkyphanhoi_model->get_list_mulTable(array('MaNKPH' => $id_PH));

        if (!$info) {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('nhatkyphanhoi/details/' .$id_PH));
        }
        foreach ($info as $row) {
            $info = $row;
            break;
        }

        $this->data['info'] = $info;

        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {

            $this->form_validation->set_rules('soluong', 'Số lượng thiết bị', 'numeric|required');
            $this->form_validation->set_rules('motavitri', 'Mô tả vị trí thiết bị hỏng', 'required');
            $this->form_validation->set_rules('mota', 'Mô tả sơ lược', 'required');

            if ($this->form_validation->run()) {

                $MaTB = $this->input->post('matb');
                $MaPhong = $this->input->post('maphong');

                $p_tb = $this->phong_thietbi_model->getListWhere(array('phong-thietbi.MaPhong' => $MaPhong, 'phong-thietbi.MaTB' => $MaTB));
                $MaPTB = $p_tb->MaPTB;

                $TinhTrang = $this->input->post('tinhtrang');
                $SoLuong = $this->input->post('soluong');
                $MoTaViTri = $this->input->post('motavitri');
                $MoTa = $this->input->post('mota');
                $ThoiGianDuyet = $this->input->post('thoigianduyet');
                $TrangThai = $this->input->post('trangthai');
            

                //Convert $ThoiGianDuyet to date
                $ThoiGianDuyet = Date($ThoiGianDuyet);

                $data = array(
                    'MaPTB' => $MaTB,
                    'MaPH' => $info->MaPH,
                    'SoLuong' => $SoLuong,
                    'TinhTrang' => $TinhTrang,
                    'MoTaViTri' => $MoTaViTri,
                    'MoTa' => $MoTa,
                    'ThoiGianDuyet' => $ThoiGianDuyet,
                    'TrangThai' => $TrangThai
                );

                if($SoLuong > $p_tb->SoLuong)
                    $this->session->set_flashdata('message', 'Số lượng bạn phản hồi lớn hơn số lượng thiết bị có trong phòng');
                else
                {
                    if ($this->chitietnhatkyphanhoi_model->update($id_PH, $data))            
                        $this->session->set_flashdata('message', 'Đã cập nhật thành công');
                    else
                        $this->session->set_flashdata('message', 'Lỗi cập nhật'); 
                }

                $maph = $info->MaPH;
                redirect(admin_url('nhatkyphanhoi/details/'.$maph));
            }
        }
            $this->data['id'] = $id_PH;

            $input = array();
            $list_p = $this->phong_thietbi_model->getListPhong();
            $this->data['list_p'] = $list_p;

            $list_tb = $this->phong_thietbi_model->getListTB();
            $this->data['list_tb'] = $list_tb;

            $list_tttb = $this->tinhtrangthietbi_model->get_list();
            $this->data['list_tttb'] = $list_tttb;

            $this->data['temp'] = 'admin/nhatkyphanhoi/editCTNKPH';
            $this->load->view('admin/main', $this->data);
        
    }
    
}
