<?php

Class Phong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('phong_model');
    }

    function index() {

        $this->load->model('giangduong_model');

        $showAll = false;
        $input = array();
        $where = array();

        $total = $this->phong_model->get_total();
        $this->data['total'] = $total;

        $tenphong = $this->input->get('tenphong');
        if ($tenphong) {
            $where['TenPhong like'] = $tenphong;
        }
        
        $magd = $this->input->get('magd');
        if ($magd) {
            $where['phong.MaGD'] = $magd;
            $this->data['magd'] = $magd;
        } else{
            $this->data['magd'] = '';
        }
        
        $succhua = $this->input->get('succhua');
        if ($succhua) {
            $where['phong.SucChua >='] = $succhua;
            $this->data['succhua'] = $succhua;
        } else{
            $this->data['succhua'] = '';
        }

        $input['where'] = $where;

        //Phan trang.
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = admin_url('phong/index');
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $this->pagination->initialize($config);

        //load du lieu

        $segment = $this->uri->segment(4);
        $segment = intval($segment);

        if(!$tenphong && !$magd &&!$succhua)
        {
            $showAll = true;
            $input['limit'] = array($config['per_page'], $segment);
        }

        $list = $this->phong_model->get_Join_P_GD($input);  
        $this->data['list'] = $list;
        $this->data['showAll'] = $showAll;
     
        $input_gd = array();
        $list_gd = $this->giangduong_model->get_list($input_gd);
        $this->data['list_gd'] = $list_gd;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/phong/index';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaPhong() {
        $maphong = $this->input->post('maphong');
        $where = array('MaPhong' => $maphong);
        if ($this->phong_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('tenphong', 'Tên phòng', 'required');
            $this->form_validation->set_rules('succhua', 'Sức chứa', 'numeric');
            $this->form_validation->set_rules('tinhtrang', 'Tình trạng', 'numeric');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaGD = $this->input->post('magd');
                $TenPhong = $this->input->post('tenphong');
                $SucChua = $this->input->post('succhua');
                $TinhTrang = $this->input->post('tinhtrang');

                $this->load->library('upload_library');
                $image_list = array();
                $upload_path = './upload/phong';
                $image_list = $this->upload_library->upload_file($upload_path, 'image_list');
                $image_list = json_encode($image_list);

                $data = array(
                    'TenPhong' => $TenPhong,
                    'MaGD' => $MaGD,
                    'SucChua' => $SucChua,
                    'TinhTrang' => $TinhTrang,
                    'HinhAnh' => $image_list,
                );
;
                if ($this->phong_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');
                redirect(admin_url("phong"));
            }
        }

        //Lấy danh sách giảng đường
        $this->load->model('giangduong_model');
        $listgd = $this->giangduong_model->get_list();
        $this->data['listgd'] = $listgd;

        $this->data['temp'] = 'admin/phong/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $id = $this->uri->rsegment('3');
        $info = $this->phong_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại phòng học này');
            redirect(admin_url('phong'));
        }

        $this->data['info'] = $info;

        if ($this->input->post()) {
            $this->form_validation->set_rules('maphong', 'Mã phòng', 'required');
            $this->form_validation->set_rules('tenphong', 'Tên phòng', 'required');
            $this->form_validation->set_rules('succhua', 'Sức chứa', 'numeric');
            if ($this->form_validation->run()) {
                //them vao csdl
                $MaPhong = $this->input->post('maphong');
                $TenPhong = $this->input->post('tenphong');
                $MaGD = $this->input->post('magd');
                $SucChua = $this->input->post('succhua');
                $TinhTrang = $this->input->post('tinhtrang');

                $this->load->library('upload_library');
                $image_list = array();
                $upload_path = './upload/phong';
                $image_list = $this->upload_library->upload_file($upload_path, 'image_list');
                $image_list_json = json_encode($image_list);

                $data = array(
                    'MaPhong' => $MaPhong,
                    'TenPhong' => $TenPhong,
                    'MaGD' => $MaGD,
                    'SucChua' => $SucChua,
                    'TinhTrang' => $TinhTrang,
                );

                if (!empty($image_list)) {
                    $data['HinhAnh'] = $image_list_json;
                }

                //pre($data);
                if ($this->phong_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');
                redirect(admin_url("phong"));
            }
        }

        $this->load->model('giangduong_model');
        $listgd = $this->giangduong_model->get_list();
        $this->data['listgd'] = $listgd;

        $this->data['temp'] = 'admin/phong/edit';
        $this->load->view('admin/main', $this->data);
    }

    function deleteph() {
        $this->load->model('phong_thietbi_model');

        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->phong_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại phòng học này');
            redirect(admin_url('phonghoc'));
        }
        //Thuc hien xoa
        //Xóa phòng - thiết bị
        $input['where'] = array('MaPhong' => $id);
        $p_tb = $this->phong_thietbi_model->get_list($input);

        if ($p_tb) {
            foreach ($p_tb as $row) {
                //Xoa anh cua thiet bi trong phong
                $image_list = json_decode($row->HinhAnh);
                if (is_array($image_list)) {
                    foreach ($image_list as $img) {
                        $image_link = './upload/phong-thietbi/' . $img;
                        if (file_exists($image_link)) {
                            unlink($image_link);
                        }
                    }
                }
            }
        }

        $this->phong_model->delete_key($id);
        //Xoa anh cua phong
        $image_list = json_decode($info->HinhAnh);
        if (is_array($image_list)) {
            foreach ($image_list as $img) {
                $image_link = './upload/phong/' . $img;
                if (file_exists($image_link)) {
                    unlink($image_link);
                }
            }
        }
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('phong'));
    }


    function details() {
        $this->load->model('phong_thietbi_model');
        $this->load->model('thietbi_model');
        $this->load->model('tinhtrangthietbi_model');

        $DieuKien = array();
        $total = $this->phong_thietbi_model->get_total();
        $this->data['total'] = $total;

        $maphong = $this->uri->rsegment('3');

        //Lấy tên phòng đó bà con
        $phong = $this->phong_model->get_info($maphong);
        $this->data['TenPhong'] = $phong->TenPhong;
 
        $input['where'] = array();
        if ($maphong) {
            $DieuKien['phong.MaPhong'] = $maphong;
            $this->data['maphong'] = $maphong;
        }
        else{
            $this->session->set_flashdata('message', 'Không tìm thấy phòng có mã '. $maphong);
        }

        $matb = $this->input->get('matb');
        if ($matb) {
            $DieuKien['phong-thietbi.MaTB'] = $matb;
            $this->data['matb'] = $matb;
        }else{
            $this->data['matb'] = '';
        }

        $input['where'] = $DieuKien;

        $listdb = $this->phong_thietbi_model->getList_Where($input);

        foreach ($listdb as $row) {          
            if(!empty($row->TTTB))
            {
                $s = "";
                $arr_temp = json_decode($row->TTTB);
                foreach ($arr_temp as $key => $value) {
                    $tttb = $this->tinhtrangthietbi_model->get_info($key);
                    $s .= $tttb->TenTT . ':' . $value . '<br/>';
                }
                $row->TTTB = $s;
            }           
        }

        $this->data['listdb'] = $listdb;

        //pre($listdb);
        $input_tb = array();
        $list_tb = $this->thietbi_model->get_list($input_tb);
        $this->data['list_tb'] = $list_tb;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/phong/details';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaTB() {
        $matb = $this->input->post('matb');
        $where = array('MaTB' => $matb);
        if ($this->thietbi_model->check_exists($where)) {
            return true;
        }
        $this->form_validation->set_message(__FUNCTION__, 'Không có mã thiết bị này');
        return false;
    }

    function check_exist($maPhong, $maTB){
        $where = array('MaPhong'=> $maPhong, 'MaTB' => $maTB);
        if($this->phong_thietbi_model->check_exists($where)){
            return true;
        }
        return false;
    }
    
    //Thêm thiết bị cho phòng
    function addThietBi() {
        $this->load->model('phong_thietbi_model');
        $this->load->model('thietbi_model');
        $id = $this->uri->rsegment('3');
       
        //Lấy thông tin quản trị viên        

        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('matb', 'Mã thiết bị', 'required|callback_check_MaTB');
            $this->form_validation->set_rules('soluong', 'Số lượng thiết bị', 'numeric');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaTB = $this->input->post('matb');
                // $MaTTTB = $this->input->post('matttb');
                $SoLuong = $this->input->post('soluong');
                $GhiChu = $this->input->post('ghichu');

                // $this->load->library('upload_library');
                // $image_list = array();
                // $upload_path = './upload/phong-thietbi';
                // $image_list = $this->upload_library->upload_file($upload_path, 'image_list');
                // $image_list = json_encode($image_list);

                $TTTB = array();
                $TTTB['1'] = $SoLuong;

                $data = array(
                    'MaTB' => $MaTB,
                    'TTTB' => json_encode($TTTB),
                    'MaPhong' => $id,
                    'SoLuong' => $SoLuong,
                    'GhiChu' => $GhiChu,
                );
                
                if($this->check_exist($id,$MaTB)){
                    $this->session->set_flashdata('message', 'Thiết bị đã tồn tại trong phòng, vui lòng sử dụng tính năng cập nhật');
                    //$this->phong_thietbi_model->update($id, $data)
                }else{                                   
                    if ($this->phong_thietbi_model->create($data))
                        $this->session->set_flashdata('message', 'Thêm mới thành công');
                    else
                        $this->session->set_flashdata('message', 'Không thêm được');
                }


                redirect(admin_url('phong/details/'.$id));
  
            }
        }

        $input = array();
        $list_tb = $this->thietbi_model->get_list($input);
        $this->data['list_tb'] = $list_tb;
        $this->data['id'] = $id;

        $this->data['temp'] = 'admin/phong/addthietbi';
        $this->load->view('admin/main', $this->data);
    }
    
    function editThietBi(){ 
        $this->load->model('phong_thietbi_model');

        $id_PTB = $this->uri->rsegment('3');

        $info = $this->phong_thietbi_model->getListWhere(array('MaPTB' => $id_PTB));

        if (!$info) {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('phong/details/' .$info->MaPhong));
        }

        $this->data['info'] = $info;


        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('soluong', 'Số lượng thiết bị', 'numeric');

            if ($this->form_validation->run()) {
                $SoLuong = $this->input->post('soluong');
                $GhiChu = $this->input->post('ghichu');
                $TTTB = json_encode(array('1' => $SoLuong));
            

                $data = array(
                        'SoLuong' => $SoLuong,
                        'GhiChu' => $GhiChu,
                        'TTTB' => $TTTB
                    );

                if ($this->phong_thietbi_model->update($id_PTB, $data))
                {
                    $this->session->set_flashdata('message', 'Đã cập nhật thành công');
                }else{
                        $this->session->set_flashdata('message', 'Lỗi cập nhật');                   
                }
                redirect(admin_url('phong/details/'.$info->MaPhong));
            }
        }

        $this->data['temp'] = 'admin/phong/editthietbi';
        $this->load->view('admin/main', $this->data);
    }
    
    function delThietBi()
    {
        $this->load->model('phong_thietbi_model');

        $id_PTB = $this->uri->rsegment('3');

        $info = $this->phong_thietbi_model->get_info($id_PTB);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('phong/details/' .$info->MaPhong));
        }

        $this->phong_thietbi_model->delete_p_tb($id_PTB);

        $this->session->set_flashdata('message', 'Xóa thiết bị thành công');
        redirect(admin_url('phong/details/'.$info->MaPhong));
    }
    
    
}
