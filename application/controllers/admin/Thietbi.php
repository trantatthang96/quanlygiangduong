<?php

Class Thietbi extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('thietbi_model');
    }

    function index() {
        $this->load->model('loaithietbi_model');
        $where = array();

        $maloaitb = $this->input->get('maloaitb');
        if ($maloaitb) {
            $where['thietbi.MaLoaiTB'] = $maloaitb;
            $this->data['maloaitb'] = $maloaitb;
        } else{
            $this->data['maloaitb'] = $maloaitb;
        }

        $list =  $this->thietbi_model->get_Join_TB_LTB($where);
        $this->data['list'] = $list;
        // pre($list);

        //pre($data);
        $input_loaitb = array();
        $list_ltb = $this->loaithietbi_model->get_list($input_loaitb);
        $this->data['list_ltb'] = $list_ltb;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/thietbi/index';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaTB() {
        $matb = $this->input->post('matb');
        $where = array('MaTB' => $matb);
        if ($this->thietbi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('loaithietbi_model');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('tentb', 'Tên thiết bị', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaLoaiTB = $this->input->post('maloaitb');
                $TenTB = $this->input->post('tentb');
                $MoTa = $this->input->post('mota');

                $this->load->library('upload_library');
                $image_list = array();
                $upload_path = './upload/thietbi';
                $image_list = $this->upload_library->upload_file($upload_path, 'image_list');
                $image_list = json_encode($image_list);
                
                $data = array(
                    'MaLoaiTB' => $MaLoaiTB,
                    'TenTB' => $TenTB,
                    'MoTa' => $MoTa,
                    'HinhAnh' => $image_list,
                );
        
                if ($this->thietbi_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('thietbi'));
            }
        }


        //Thêm dữ liệu cho list box chọn nhân viên quản lí, show hết luôn, có điều kiện gì thì mấy chú thêm sau v
        $input = array();
        $list_ltb = $this->loaithietbi_model->get_list($input);
        $this->data['list_ltb'] = $list_ltb;

        $this->data['temp'] = 'admin/thietbi/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('loaithietbi_model');

        $id = $this->uri->rsegment('3');

        $info = $this->thietbi_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại giảng đường này');
            redirect(admin_url('giangduong'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {

            $this->form_validation->set_rules('tentb', 'Tên giảng đường', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaTB = $this->input->post('matb');
                $TenTB = $this->input->post('tentb');
                $MaLoaiTB = $this->input->post('maloaitb');
                $MoTa = $this->input->post('mota');

                $this->load->library('upload_library');
                $image_list = array();
                $upload_path = './upload/thietbi';
                $image_list = $this->upload_library->upload_file($upload_path, 'image_list');
                $image_list_json = json_encode($image_list);

                $image = '';
                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                }
                //pre($data);
                $data = array(
                    'MaLoaiTB' => $MaLoaiTB,
                    'TenTB' => $TenTB,
                    'MoTa' => $MoTa,
                ); 

                if (!empty($image_list)) {
                    $data['HinhAnh'] = $image_list_json;
                }

                if ($this->thietbi_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('thietbi'));
           }
        }

        $input = array();
        $list_ltb = $this->loaithietbi_model->get_list($input);
        $this->data['list_ltb'] = $list_ltb;

        $this->data['temp'] = 'admin/thietbi/edit';
        $this->load->view('admin/main', $this->data);
    }

    function delete() {
        $this->load->model('phong_thietbi_model');

        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->thietbi_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại thiết bị này');
            redirect(admin_url('thietbi'));
        }
        //Thuc hien xoa
        $input_p_tb['where'] = array('MaTB' => $id);
        $list_p_tb = $this->phong_thietbi_model->get_list($input_p_tb);
        if ($list_p_tb) 
        {
            foreach($list_p_tb as $row)
            {
                //Xoa anh cua thiet bi
                $image_list = json_decode($p_tb->HinhAnh);
                if (is_array($image_list)) {
                    foreach ($image_list as $img) {
                        $image_link = './upload/phong-thietbi/' . $img;
                        if (file_exists($image_link)) {
                            unlink($image_link);
                        }
                    }
                }
            }
        }
        
        $this->thietbi_model->delete_key($id);

        //Xoa anh cua thiet bi
        $image_list = json_decode($info->HinhAnh);
        if (is_array($image_list)) {
            foreach ($image_list as $img) {
                $image_link = './upload/thietbi/' . $img;
                if (file_exists($image_link)) {
                    unlink($image_link);
                }
            }
        }

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('thietbi'));
    }

    // Xem chi tiết giảng đường

}
