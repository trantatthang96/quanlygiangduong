<?php

Class Giangduong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('giangduong_model');
    }

    function index() {
        $this->load->model('nguoidung_model');
        $this->load->model('phong_model');
        //Dem so giang duong
        $total = $this->giangduong_model->get_total();
        $this->data['total'] = $total;

        $input = array();
        $input['order'] = array('MaGD', 'asc');
        // $input_gd['order'] = array('MaGD', 'asc');
        //$thu = $this->giangduong_model->get_total($input);
        //pre($thu);
        $where = array();
        // $where['order'] = array('MaGD', 'asc');

        $tengd = $this->input->get('tengd');
        if ($tengd) {
            $where["TenGD"] = $tengd;
            // $input_gd["like"] = $where["TenGD"] ;
        }
        
        $manvql = $this->input->get('manvql');
        if ($manvql) {
            $where['MaNVQL'] = $manvql;
            // $input_gd["where"] = $where['MaNVQL'];
            $this->data['mand'] = $manvql;
        } else{
            $this->data['mand'] = '';
        }

        $list =  $this->giangduong_model->get_Join_where_GD('nguoidung','MaND', 'MaNVQL',$where);
        foreach($list as $row){
            $input['where'] = array('MaGD' => $row->MaGD);
            $totalRoom = $this->phong_model->get_list($input);
            $row->totalRoom = count($totalRoom);
        }
        
        $this->data['list'] = $list;

        //pre($data);
        $input_nd = array();
        $list_nd = $this->nguoidung_model->get_list($input_nd);
        $this->data['list_nd'] = $list_nd;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/giangduong/index';
        $this->load->view('admin/main', $this->data);
    }

    function countRoom($maGD){
        return $this->giangduong_model->countRoom($maGD);
    }

    function check_MaGD() {
        $magd = $this->input->post('magd');
        $where = array('MaGD' => $magd);
        if ($this->giangduong_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->library('upload_library');
        $this->load->helper('form');
        $this->load->model('nguoidung_model');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {

            $this->form_validation->set_rules('tengd', 'Tên giảng đường', 'required');
            //$this->form_validation->set_rules('nvql', 'Nhân viên quản lí', 'required');
            $this->form_validation->set_rules('sotang', 'Số tầng', 'numeric|required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaNVQL = $this->input->post('manvql');
                $TenGD = $this->input->post('tengd');
                $SoTang = $this->input->post('sotang');
                
                $upload_path = './upload/giangduong';
                $upload_data = $this->upload_library->upload($upload_path, 'image');
                //pre($upload_data);
                //$image = '';
                $data = array(
                    'MaNVQL' => $MaNVQL,
                    'TenGD' => $TenGD,
                    'SoTang' => $SoTang
                );


                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                    $data['HinhAnh'] = $image;
                }
                
        
                if ($this->giangduong_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('giangduong'));
            }
        }


        //Thêm dữ liệu cho list box chọn nhân viên quản lí, show hết luôn, có điều kiện gì thì mấy chú thêm sau v
        $input['where']['MaLoaiND'] = '2';
        $list_nvql = $this->nguoidung_model->get_list($input);
        $this->data['list_nvql'] = $list_nvql;

        $this->data['temp'] = 'admin/giangduong/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('nguoidung_model');

        $id = $this->uri->rsegment('3');

        $info = $this->giangduong_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại giảng đường này');
            redirect(admin_url('giangduong'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {
            $this->form_validation->set_rules('magd', 'Mã giảng đường', 'required');
            $this->form_validation->set_rules('tengd', 'Tên giảng đường', 'required');
            $this->form_validation->set_rules('manvql', 'Nhân viên quản lí', 'required');           
            $this->form_validation->set_rules('sotang', 'Số tầng', 'numeric');
            //pre($upload_data);
            //pre($image);

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaGD = $this->input->post('magd');
                $TenGD = $this->input->post('tengd');
                $MaNVQL = $this->input->post('manvql');
                $SoTang = $this->input->post('sotang');
            
                $this->load->library('upload_library');
                $upload_path = './upload/giangduong';
                $upload_data = $this->upload_library->upload($upload_path, 'image');

                $image = '';
                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                }
                //pre($data);
                $data = array(
                    'TenGD' => $TenGD,
                    'MaNVQL' => $MaNVQL,
                    'SoTang' => $SoTang
                ); 
                
                if($image != '')
                {
                    $data['HinhAnh'] = $image;
                    $image_del = './upload/giangduong/' . $info->HinhAnh;
                    if (file_exists($image_del)) {
                        unlink($image_del);
                    }
                }

                if ($this->giangduong_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('giangduong'));
           }
        }
        
        $input['where']['MaLoaiND'] = '2';
        $list_nvql = $this->nguoidung_model->get_list($input);
        
        $this->data['list_nvql'] = $list_nvql;

        $this->data['temp'] = 'admin/giangduong/edit';
        $this->load->view('admin/main', $this->data);
    }

    function delete() {
        $this->load->model('phong_model');
        $this->load->model('phong_thietbi_model');

        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->giangduong_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại giảng đường này');
            redirect(admin_url('giangduong'));
        }
        //Thuc hien xoa 
        $input['where'] = array('MaGD' => $info->MaGD);
        $list_phong = $this->phong_model->get_list($input);
        //Nếu có tồn tại phòng, xóa phòng trước
        if($list_phong)
        {
            foreach($list_phong as $phong)
            {
                //Xóa hình ảnh của phòng
                $image_list = json_decode($phong->HinhAnh);
                if (is_array($image_list)) {
                    foreach ($image_list as $img) {
                        $image_link = './upload/phong/' . $img;
                        if (file_exists($image_link)) {
                            unlink($image_link);
                        }
                    }
                }

                //Xóa liên kết giữa phòng đó - thiết bị trong phòng
                $input_ptb['where'] = array('MaPhong' => $phong->MaPhong);
                $p_tb = $this->phong_thietbi_model->get_list($input_ptb);

                if ($p_tb) {
                    foreach ($p_tb as $row) {
                        //Xoa anh cua thiet bi trong phong
                        $image_list = json_decode($row->HinhAnh);
                        if (is_array($image_list)) {
                            foreach ($image_list as $img) {
                                $image_link = './upload/phong-thietbi/' . $img;
                                if (file_exists($image_link)) {
                                    unlink($image_link);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        $this->giangduong_model->delete_key($id);
        //Xoa anh cua thiet bi
        $HinhAnh = './upload/giangduong/' . $info->HinhAnh;
        if (file_exists($HinhAnh)) {
            unlink($HinhAnh);
        }
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('giangduong'));
    }

    // Xem chi tiết giảng đường

}
