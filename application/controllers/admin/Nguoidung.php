<?php

Class Nguoidung extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('nguoidung_model');
    }

    //Lay danh sach nguoi dung
    function index() {
        $this->load->model('loainguoidung_model');
        //Kiem tra co lay tat ca du lieu khong
        $showAll = false;
        //Lay tat ca cac dong
        $total = $this->nguoidung_model->get_total();
        $this->data['total'] = $total;
        //Lay trang theo dieu kien
        $input = array();
        $mand = $this->input->get('mand');
        $input['where'] = array();
        if ($mand) {
            $input['where']['MaND'] = $mand;
        }

        $tennd = $this->input->get('tennd');
        if ($tennd) {
            $input['like'] = array('HoTenND', $tennd);
        }

        $maloaind = $this->input->get('maloaind');
        if ($maloaind) {
            $input['where']['MaLoaiND'] = $maloaind;
        }
        $this->data['malnd'] = $maloaind;
        //Phan trang.
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = admin_url('nguoidung/index');
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $this->pagination->initialize($config);

        //load du lieu

        $segment = $this->uri->segment(4);
        $segment = intval($segment);

        if (!$tennd && !$maloaind && !$mand) {
            $showAll = true;
            $input['limit'] = array($config['per_page'], $segment);
        }
        $list = $this->nguoidung_model->get_list($input);
        $this->data['list'] = $list;
        //pre($list);

        $input_lnd = array();
        $list_lnd = $this->loainguoidung_model->get_list($input_lnd);
        $this->data['list_lnd'] = $list_lnd;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['showAll'] = $showAll;

        $this->data['temp'] = 'admin/nguoidung/index';
        $this->load->view('admin/main', $this->data);
    }

    //Kiểm tra username tồn tại chưa
    function check_username() {
        $username = $this->input->post('username');
        $where = array('Username' => $username);
        if ($this->nguoidung_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Tài khoản đã tồn tại');
            return false;
        }
        return true;
    }

    function check_MaND() {
        $mand = $this->input->post('mand');
        $where = array('MaND' => $mand);
        if ($this->nguoidung_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    //Them moi quan tri vien
    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('loainguoidung_model');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('hotennd', 'Họ và tên người dùng', 'required');
            $this->form_validation->set_rules('username', 'Tài khoản người dùng', 'required|callback_check_username');
            $this->form_validation->set_rules('password', 'Mật khẩu người dùng', 'required|min_length[6]');
            $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
            $this->form_validation->set_rules('chucvu', 'Chức vụ', 'required');
            $this->form_validation->set_rules('dienthoai', 'Điện thoại', 'numeric');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');
            if ($this->form_validation->run()) {
                //them vao csdl
                $HoTenND = $this->input->post('hotennd');
                $NgaySinh_f = $this->input->post('ngaysinh');
                $GioiTinh_f = $this->input->post('gioitinh');
                $DienThoai = $this->input->post('dienthoai');
                $Email = $this->input->post('email');
                $Username = $this->input->post('username');
                $Password = $this->input->post('password');
                $ChucVu = $this->input->post('chucvu');
                $MaLoaiND = $this->input->post('maloaind');

                $NgaySinh = date("Y-m-d", strtotime($NgaySinh_f));
                if ($GioiTinh_f == "1")
                    $GioiTinh = TRUE;
                else
                    $GioiTinh = FALSE;

                $this->load->library('upload_library');
                $upload_path = './upload/nguoidung';
                $upload_data = $this->upload_library->upload($upload_path, 'image');

                $data = array(
                    'HoTenND' => $HoTenND,
                    'NgaySinh' => $NgaySinh,
                    'GioiTinh' => $GioiTinh,
                    'DienThoai' => $DienThoai,
                    'Email' => $Email,
                    'ChucVu' => $ChucVu,
                    'Username' => $Username,
                    'Password' => md5($Password),
                    'MaLoaiND' => $MaLoaiND,
                );

                $image = '';
                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                    $data['HinhAnh'] = $image;
                }

                if ($this->nguoidung_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('nguoidung'));
            }
        }

        //Lay danh sach loại người dùng

        $input['order'] = array('MaLoaiND', 'ASC');
        $list_lnd = $this->loainguoidung_model->get_list($input);
        $this->data['list_lnd'] = $list_lnd;

        $this->data['temp'] = 'admin/nguoidung/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        //Lấy id quản trị viên cần chỉnh sửa
        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->nguoidung_model->get_info($id);
        //pre($info);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại người dùng này');
            redirect(admin_url('admin'));
        }
        $this->data['info'] = $info;

        if ($this->input->post()) {

            $this->form_validation->set_rules('hotennd', 'Họ và tên người dùng', 'required');
            $this->form_validation->set_rules('username', 'Tài khoản người dùng', 'required');
            $this->form_validation->set_rules('chucvu', 'Chức vụ', 'required');
            $this->form_validation->set_rules('dienthoai', 'Điện thoại', 'numeric');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');

            $password = $this->input->post('password');

            if ($password != '') {
                $this->form_validation->set_rules('password', 'Mật khẩu người dùng', 'min_length[6]');
                $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
            }


            if ($this->form_validation->run()) {
                $MaND = $this->input->post('mand');
                $HoTenND = $this->input->post('hotennd');
                $NgaySinh_f = $this->input->post('ngaysinh');
                $GioiTinh_f = $this->input->post('gioitinh');
                $DienThoai = $this->input->post('dienthoai');
                $Email = $this->input->post('email');
                $Username = $this->input->post('username');
                //$Password = $this->input->post('password');
                $ChucVu = $this->input->post('chucvu');
                $MaLoaiND = $this->input->post('maloaind');

                $NgaySinh = date("Y-m-d", strtotime($NgaySinh_f));
                if ($GioiTinh_f == "1")
                    $GioiTinh = TRUE;
                else
                    $GioiTinh = FALSE;

                $this->load->library('upload_library');
                $upload_path = './upload/nguoidung';
                $upload_data = $this->upload_library->upload($upload_path, 'image');
                $image = '';

                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                    //$data['HinhAnh'] = $image;
                }

                $data = array(
                    'MaND' => $MaND,
                    'HoTenND' => $HoTenND,
                    'NgaySinh' => $NgaySinh,
                    'GioiTinh' => $GioiTinh,
                    'DienThoai' => $DienThoai,
                    'Email' => $Email,
                    'ChucVu' => $ChucVu,
                    'Username' => $Username,
                    'MaLoaiND' => $MaLoaiND,
                );

                if ($password) {
                    $data['Password'] = md5($password);
                }

                if ($image != '') {
                    $data['HinhAnh'] = $image;
                    $image_del = './upload/nguoidung/' . $info->HinhAnh;
                    if (file_exists($image_del)) {
                        unlink($image_del);
                    }
                }

                //pre($data);
                if ($this->nguoidung_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Không cập nhật được thêm đươc');

                redirect(admin_url('nguoidung'));
            }
        }
        $this->load->model('loainguoidung_model');
        $input = array();
        $list_lnd = $this->loainguoidung_model->get_list($input);
        $this->data['list_lnd'] = $list_lnd;

        $this->data['temp'] = 'admin/nguoidung/edit';
        $this->load->view('admin/main', $this->data);
    }

    //Hàm xóa
    function delete() {
        $id = $this->uri->rsegment('3');
        $this->_del($id);
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('nguoidung'));
    }

    //xoa nhieu san pham
    function delete_all() {
        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            $this->_del($id, false);
        }
    }

    //Xóa người sử dụng
    private function _del($id, $redirect = true) {

        $this->load->model('nhatkyphanhoi_model');

        //Kiểm tra xem có tồn tại người dùng hay không
        $info = $this->nguoidung_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại người dùng này');
            if ($redirect) {
                redirect(admin_url('admin'));
            } else {
                return false;
            }
        }

        //Kiểm tra xem người dùng này có đang được sử dụng khóa ngoại trong bảng 
        //nhật ký phản hổi k, nếu có thì phải xóa NKPH trước khi xóa người dùng
        $nkph = $this->nhatkyphanhoi_model->get_ND_NKPH($id);
        if ($nkph) {
            $this->session->set_flashdata('message', 'Người dùng có mã ' . $id . ' hiện đang được sử dụng trong cơ sở dữ liệu, xin hãy xóa các đối tượng có liên quan trước khi xóa người dùng này.');
            if ($redirect) {
                redirect(admin_url('admin'));
            } else {
                return false;
            }
        }
        $image = './upload/nguoidung/' . $info->HinhAnh;
        if (file_exists($image)) {
            unlink($image);
        }

        $this->nguoidung_model->delete_key($id);
    }

    function logout() {
        if ($this->session->userdata('login'))
        {
            $this->session->unset_userdata('login');
            $this->session->unset_userdata('user_id_login');
        }
        redirect(base_url('home'));
    }

}
