<?php

Class Tinhtrangthietbi extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('tinhtrangthietbi_model');
    }

    //Lay ra danh sach cac trang thiet bi
    function index() {
        $input[] = array();

        //Xuat du lieu ra
        $list = $this->tinhtrangthietbi_model->get_list($input);
        $this->data['list'] = $list;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/tinhtrangthietbi/index';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaTTTB() {
        $matttb = $this->input->post('matttb');
        $where = array('MaTTTB' => $matttb);
        if ($this->tinhtrangthietbi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('tentt', 'Tên tình trạng', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $TenTTB = $this->input->post('tentt');
                $MoTa = $this->input->post('mota');

                $data = array(
                    'TenTT' => $TenTTB,
                    'MoTa' => $MoTa,
                );
                if ($this->tinhtrangthietbi_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('tinhtrangthietbi'));
            }
        }

        $this->data['temp'] = 'admin/tinhtrangthietbi/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $id = $this->uri->rsegment(3);
        $info = $this->tinhtrangthietbi_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại tình trạng này');
            redirect(admin_url('tinhtrangthietbi'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('matttb', 'Mã tình trạng thiết bị', 'required');
            $this->form_validation->set_rules('tentt', 'Tên tình trạng', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaTTTB = $this->input->post('matttb');
                $TenTT = $this->input->post('tentt');
                $MoTa = $this->input->post('mota');

                $data = array(
                    'TenTT' => $TenTT,
                    'MoTa' => $MoTa,
                );

                if ($this->tinhtrangthietbi_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('tinhtrangthietbi'));
            }
        }


        $this->data['temp'] = 'admin/tinhtrangthietbi/edit';
        $this->load->view('admin/main', $this->data);
    }

    // function delete() {
    //     $id = $this->uri->rsegment('3');
    //     $this->_del($id);
    //     $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
    //     redirect(admin_url('tinhtrangthietbi'));
    // }

    // //xoa nhieu san pham
    // //Hàm này k có dùng
    // function delete_all() {
    //     $ids = $this->input->post('ids');
    //     foreach ($ids as $id) {
    //         $this->_del($id, false);
    //     }
    // }

    // private function _del($id, $redirect = true) {
    //     //kiểm tra các phòng có sử dụng thiết bị này không
    //     $this->load->model('chitietnhatkyphanhoi_model');

    //     $info = $this->tinhtrangthietbi_model->get_info($id);
    //     if (!$info) {
    //         $this->session->set_flashdata('message', 'Không tồn tại tình trạng thiết bị này');
    //         if ($redirect) {
    //             redirect(admin_url('tinhtrangthietbi'));
    //         }else{
    //             return false;
    //         }
            
    //     }
    //     //Thuc hien xoa

    //     $ctnk = $this->chitietnhatkyphanhoi_model->get_info_rule(array('TinhTrang' => $id), 'TinhTrang');
    //     if ($ctnk) {
    //         $this->session->set_flashdata('message', 'Tình trạng này đang hiện hữu!');
    //         if ($redirect) {
    //             redirect(admin_url('tinhtrangthietbi'));
    //         }else{
    //             return false;
    //         }
    //     }

    //     $this->tinhtrangthietbi_model->delete_key($id);
    // }

}
