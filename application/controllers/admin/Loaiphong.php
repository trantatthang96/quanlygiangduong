<?php

Class Loaiphong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('loaiphong_model');
    }

    function index() {
        //Dem so giang duong
        $total = $this->loaiphong_model->get_total();
        $this->data['total'] = $total;

        $input = array();
        $input['order'] = array('MaLP', 'asc');
        //$thu = $this->loaiphong_model->get_total($input);
        //pre($thu);
        $list = $this->loaiphong_model->get_list($input);
        $this->data['list'] = $list;

        //pre($data);
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/loaiphong/index';
        $this->load->view('admin/main', $this->data);
    }

    function check_MaLP() {
        $malp = $this->input->post('malp');
        $where = array('MaLP' => $malp);
        if ($this->loaiphong_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('malp', 'Mã loại phòng', 'required|callback_check_MaLP');
            $this->form_validation->set_rules('tenlp', 'Tên loại phòng', 'required');
            if ($this->form_validation->run()) {
                //them vao csdl
                $MaLP = $this->input->post('malp');
                $TenLP = $this->input->post('tenlp');
                $MucDichSuDung = $this->input->post('mucdich');
                $data = array(
                    'MaLP' => $MaLP,
                    'TenLoaiPhong' => $TenLP,
                    'MucDichSuDung' => $MucDichSuDung
                        
                );
        
                if ($this->loaiphong_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('loaiphong'));
            }
        }


        $this->data['temp'] = 'admin/loaiphong/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $id = $this->uri->rsegment('3');
        $info = $this->loaiphong_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại phòng này');
            redirect(admin_url('loaiphong'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {
            $this->form_validation->set_rules('malp', 'Mã loại phòng', 'required');
            $this->form_validation->set_rules('tenlp', 'Tên loại phòng', 'required');
            //pre($upload_data);
            //pre($image);

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaLP = $this->input->post('malp');
                $TenLP = $this->input->post('tenlp');
                $MucDichSuDung = $this->input->post('mucdich');
                $data = array(
                    'MaLP' => $MaLP,
                    'TenLoaiPhong' => $TenLP,
                    'MucDichSuDung' => $MucDichSuDung
                        
                );

                if ($this->loaiphong_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('loaiphong'));
           }
        }

        $this->data['temp'] = 'admin/loaiphong/edit';
        $this->load->view('admin/main', $this->data);
    }

    function delete() {
        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->loaiphong_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại phòng này');
            redirect(admin_url('loaiphong'));
        }
        //Thuc hien xoa
        $this->load->model('phonghoc_model');
        $phonghoc = $this->phonghoc_model->get_info_rule(array('MaLP' => $info->MaLP), 'MaLP');
        if ($phonghoc) {
            $this->session->set_flashdata('message', '<b><font color="red">' . $info->TenLoaiPhong . '</font></b> đang sử dụng, bạn hãy xóa phòng có kiểu loại phòng này trước');
                redirect(admin_url('loaiphong'));
            }
        
        $this->loaiphong_model->deleteLoaiPhong($id);
        //Xoa anh cua thiet bi
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('loaiphong'));
    }

    // Xem chi tiết giảng đường

}
