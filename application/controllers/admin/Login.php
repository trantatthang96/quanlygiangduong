<?php
Class Login extends MY_Controller
{
    function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        if ($this->input->post())
        {
            $this->form_validation->set_rules('login', 'login', 'callback_check_login');
            if ($this->form_validation->run())
            {
                $user = $this->_get_user_info();
                $this->session->set_userdata('login', $user->MaND);
                $isAdmin = 0;
                if($user->MaLoaiND == '1')
                    $isAdmin = 1;
                $this->session->set_userdata('isAdmin', $isAdmin);
                redirect (admin_url('home'));
            }
        }
        $this->load->view('admin/login/index');
    }
    
    function check_login()
    {
        $user = $this->_get_user_info();
        
        if ($user)
        {
            if($user->MaLoaiND == '1' || $user->MaLoaiND == '2')
                return true;
            $this->form_validation->set_message(__FUNCTION__,'Chỉ có admin và nhân viên mới có quyền truy cập trang quản trị');
            return false;

        }
        $this->form_validation->set_message(__FUNCTION__,'Không đăng nhập thành công');
        return false;
    }
    
     function _get_user_info(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password = md5($password);
        //pre($username);
        $this->load->model('nguoidung_model');
        $where = array('username' => $username, 'password' => $password);
        $user = $this->nguoidung_model->get_info_rule($where);
        return $user;
    }
}
