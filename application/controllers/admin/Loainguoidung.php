<?php

Class Loainguoidung extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('loainguoidung_model');
    }

    function index() {

        $input = array();
        $input['order'] = array('MaLoaiND', 'asc');
        //$thu = $this->giangduong_model->get_total($input);
        //pre($thu);
        $where = array();

        $tenloai = $this->input->get('tenloai');
        if ($tenloai) {
            $where['TenLoai like'] = $tenloai;
        }

        $input['where'] = $where;

        $list =  $this->loainguoidung_model->get_list($input);
        $this->data['list'] = $list;

        //pre($data);

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/loainguoidung/index';
        $this->load->view('admin/main', $this->data);
    }

    function check_id() {
        $id = $this->input->post('maloaind');
        $where = array('MaLoaiND' => $id);
        if ($this->loainguoidung_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('tenloai', 'Tên loại người dùng', 'required');

            $this->form_validation->set_rules('mota', 'Mô tả loại người dùng', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $TenLoai = $this->input->post('tenloai');
                $Mota = $this->input->post('mota');

                $data = array(
                    'TenLoai' => $TenLoai,
                    'Mota' => $Mota,
                );
        
                if ($this->loainguoidung_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('loainguoidung'));
            }
        }


        $this->data['temp'] = 'admin/loainguoidung/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $id = $this->uri->rsegment('3');

        $info =  $this->loainguoidung_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại người dùng này');
            redirect(admin_url('loainguoidung'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {
            $this->form_validation->set_rules('maloaind', 'Mã loại người dùng', 'required');
            $this->form_validation->set_rules('tenloai', 'Tên loại người dùng', 'required');
            $this->form_validation->set_rules('mota', 'Mô tả loại người dùng', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $MaLoaiND = $this->input->post('maloaind');
                $TenLoai = $this->input->post('tenloai');
                $Mota = $this->input->post('mota');
            
                $data = array(
                    'TenLoai' => $TenLoai,
                    'Mota' => $Mota,
                );

                if ($this->loainguoidung_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('loainguoidung'));
           }
        }

        $this->data['temp'] = 'admin/loainguoidung/edit';
        $this->load->view('admin/main', $this->data);
    }

    function delete() {
        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->loainguoidung_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại người dùng này');
            redirect(admin_url('loainguoidung'));
        }
        //Thuc hien xoa
        
        $this->loainguoidung_model->delete_key($id);

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('loainguoidung'));
    }
}
