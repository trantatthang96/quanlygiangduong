<?php
Class Home extends MY_Controller
{
    // function index(){
    //     $this->data['temp'] = 'admin/home/index';
    //     $this->load->view('admin/main', $this->data);
    //     redirect(admin_url('giangduong'));
    // }


    function __construct() {
        parent::__construct();
        $this->load->model('chitietnhatkyphanhoi_model');
        $this->load->model('nguoidung_model');
    }

    function index() {

        $user_id_logined = $this->session->userdata('login');
        $user = $this->nguoidung_model->get_info($user_id_logined);

        $this->data['user'] = $user;

        if($user->MaLoaiND == '1')
        {
            $this->data['temp'] = 'admin/giangduong/index';
            $this->load->view('admin/main', $this->data);
            redirect(admin_url('giangduong'));
        }


        $this->load->model('phong_model');
        $this->load->model('thietbi_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('nhatkyphanhoi_model');
        
        $showAll = false;
        $input = array();
        $where = array();

        //Lấy danh sách phản hồi theo mã nhân viên đăng nhập nếu là nhân viên quản lý
        if($user->MaLoaiND == 2)
            $where['MaNVDuyet'] = $user_id_logined;


        //Get total theo ds lay duoc
        // $total = $this->chitietnhatkyphanhoi_model->get_total();
        // $this->data['total'] = $total;

        $mankph = $this->input->get('mankph');
        if ($mankph) {
            $where['MaNKPH like'] = $mankph;
        }
        
        $maphong = $this->input->get('maphong');
        if ($maphong) {
            $where['phong.MaPhong'] = $maphong;
            $this->data['maphong'] = $maphong;
        } else{
            $this->data['maphong'] = '';
        }
        
        $matb = $this->input->get('matb');
        if ($matb) {
            $where['thietbi.MaTB'] = $matb;
            $this->data['matb'] = $matb;
        } else{
            $this->data['matb'] = '';
        }

        $trangthai = $this->input->get('trangthai');
        if($trangthai != ''){
            $where['chitietnhatkyphanhoi.TrangThai'] = $trangthai;
            $this->data['trangthai'] = $trangthai;
        }else {
            $this->data['trangthai'] = '';
        }

        $input['where'] = $where;

        $count_list = $this->chitietnhatkyphanhoi_model->get_list_mulTable($input);

        $need_approval = 0;
        $need_repair = 0;
        $repaired = 0;
        foreach($count_list as $row)
        {
            $row->TrangThai == 0 ? $need_approval++ : ($row->TrangThai == 1 ? $need_repair++ : $repaired++);
        }

        $this->data['need_approval'] = $need_approval;
        $this->data['need_repair'] = $need_repair;
        $this->data['repaired'] = $repaired;

        //Phan trang.
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = admin_url('home/index');
        $config['per_page'] = 10;
        $config['total_rows'] = count($count_list);
        // $config['per_page'] = 1;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $this->pagination->initialize($config);

        //load du lieu

        $segment = $this->uri->segment(4);
        $segment = intval($segment);

        if(!$mankph && !$maphong && !$matb && $trangthai == '')
        {
            $showAll = true;
            $input['limit'] = array($config['per_page'], $segment);
        }

        //Sắp xếp theo thời gian gửi của nhật ký phản hồi
        $input['order'] = array('nhatkyphanhoi.ThoiGianGoi','DESC');
        $input['order'] = array('chitietnhatkyphanhoi.TrangThai','ASC');
        
        $list = $this->chitietnhatkyphanhoi_model->get_list_mulTable($input);
        
        foreach($list as $row){
            $MaNVDuyet = $this->nhatkyphanhoi_model->get_info($row->MaPH)->MaNVDuyet;
            if($row->ThoiGianDuyet == null)
                $row->ThoiGianDuyet = '';
            else $row->ThoiGianDuyet = get_date($row->ThoiGianDuyet);

            if($user->MaLoaiND == 4) 
            {
                if($row->TrangThai == 0)
                    $row->Duyet = true;
                else $row->Duyet = false;

                if($row->TrangThai == 1)
                    $row->SuaChua = true;
                else $row->SuaChua = false;

                
                if($row->TrangThai == 2)
                {
                    if($this->data['isAdmin'])
                        $row->Xoa = true;
                    else 
                        $row->Xoa = false;
                }
                else $row->Xoa = true;
            } else {
                //Xét khả năng duyệt của user
                if($row->TrangThai == 0 && $MaNVDuyet == $user_id_logined)
                    $row->Duyet = true;
                else $row->Duyet = false;

                //Xét cả năng cập nhật khi sửa chữa của user
                if($row->TrangThai == 1 && $MaNVDuyet == $user_id_logined)
                    $row->SuaChua = true;
                else $row->SuaChua = false;
  
                if($row->TrangThai != 2 && $MaNVDuyet == $user_id_logined)
                    $row->Xoa = true;
                else $row->Xoa = false;
            }
            
        }   

        //Xét cả năng cập nhật khi sửa chữa của user
        $this->data['list'] = $list;
        $this->data['user_id'] = $this->session->userdata('login');
        $this->data['showAll'] = $showAll;

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        //Lấy data đổ xuống các select cho form lọc

        $input_p_tb = array();
        $list_p = $this->phong_model->get_list($input_p_tb);
        $this->data['list_p'] = $list_p;

        $list_tb = $this->thietbi_model->get_list($input_p_tb);
        $this->data['list_tb'] = $list_tb;

        $this->data['temp'] = 'admin/home/index';
        $this->load->view('admin/main', $this->data);
    }

    
    function delete() {
        $id = $this->uri->rsegment('3');
        $this->chitietnhatkyphanhoi_model->delete_key($id);       
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('chitietnhatkyphanhoi'));
    }

    /*
        Duyệt một chi tiết nhật ký phản hồi
    */
    function approval(){

        $status = 0;
        /*
            url: "localhost:89/qlgq/admin/..../{id_PH}/{id_NKPH}"
        */
        $this->load->model('nguoidung_model');
        $this->load->model('nhatkyphanhoi_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('tinhtrangthietbi_model');

        //Get id of NKPH from URI
        $id_PH = $this->uri->rsegment('3');
        $id_NKPH = $this->uri->rsegment('4');

        //Lấy dữ liệu từ bảng nhatkyphanhoi & chitietnhatkyphanhoi
        $info_PH = $this->nhatkyphanhoi_model->get_info($id_PH);
        $info_CT = $this->chitietnhatkyphanhoi_model->get_info($id_NKPH);

        //Get id user login
        $user_id_login = $this->session->userdata('login');

        //kiểm tra dữ liệu lấy được từ bảng chitietnhatkyphanhoi
        if(!$info_CT)
        {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('home'));
        } else if($info_CT->TrangThai == 1){
            $this->session->set_flashdata('message', 'Dữ liệu đã được duyệt');
            redirect(admin_url('home'));
        } else if($info_PH->MaNVDuyet != $user_id_login){
            $this->session->set_flashdata('message', 'Bạn không phải là người dùng có quyền duyệt phản hồi này');
            redirect(admin_url('home'));
        }       

        //Check id của người đang đăng nhập
        if(!$user_id_login){
            $this->session->set_flashdata('message', 'Không tìm thấy người dùng đăng nhập');
            redirect(admin_url('home'));
        }

        //Lấy ra danh sách các tình trạng thiết bị
        $input = array();
        $tttb = $this->tinhtrangthietbi_model->get_list($input);

        //Lấy danh sách các id của đối tượng tttb (tình trạng thiết bị)
        $list = array();
        foreach ($tttb as $row) 
        {
            $list[] = $row->MaTTTB;
        }

        //Lấy tình trạng thiết bị sẵn có của bảng phong-thietbi
        $p_tb = $this->phong_thietbi_model->get_info($info_CT->MaPTB);

        /*
            params: 
                $soluong: số lượng của thiết bị trong phản hồi
                $tinhtrang: tình trạng được phản hồi
        */
        $soluong = $info_CT->SoLuong;
        $tinhtrang = $info_CT->TinhTrang;

        $list_TTTB = array();

        if($p_tb)
        {
            //Nếu mảng TTTB chưa có, khởi tạo một mảng mới với tất cả các thiết bị có tình trạng tốt
            //Mặc định tình trạng tốt có key là 1.
            if(empty($p_tb->TTTB))
                $list_TTTB['1'] = $p_tb->SoLuong;  //output: ['1' => SoLuong]~
            else 
            {
                $list_TTTB = json_decode($p_tb->TTTB, true);
            }
            /* 
                Duyệt qua mảng các maTTTB
                Nếu tình trạng trong mảng trùng với tình trạng được post lên thì cộng dồn thêm vào tình trạng đó
                Nếu không tồn tại tình trạng nào đó thì khởi tạo nó 0.
            */

            foreach($list as $item)
            {
                if($item == $tinhtrang)
                {
                    $list_TTTB[$item] += $soluong;

                    //reset the array
                    $list_TTTB['1'] -= $soluong; 
                }
            }


            //encode the $list_TTTB and send to phong_thietbi_model
            if($this->phong_thietbi_model->approval(json_encode($list_TTTB), $info_CT->MaPTB))
                $status++;

            //Gọi approval (duyệt) trong model sau khi đã cập nhật thành công trạng thái của thiết bị trong phòng
            if($this->chitietnhatkyphanhoi_model->approval($id_NKPH))
                $status++;

            //if both chitietnhatkyphanhoi_model and phong_thietbi_model successed
            if($status == 2)
                $this->session->set_flashdata('message', 'Đã duyệt thành công');
            else $this->session->set_flashdata('message', 'Duyệt thất bại');
        }
        else $this->session->set_flashdata('message', 'Không tìm thấy đối tượng duyệt');

        redirect(admin_url('home'));

    }

    /*
        Cập nhật lại trạng thái khi product đã được sửa chữa
    */
    function repaired(){

        $status = 0;
        /*
            url: "localhost:89/qlgq/admin/..../{id_PH}/{id_NKPH}"
        */
        $this->load->model('nguoidung_model');
        $this->load->model('nhatkyphanhoi_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('tinhtrangthietbi_model');

        //Get id of NKPH from URL
        $id_PH = $this->uri->rsegment('3');
        $id_NKPH = $this->uri->rsegment('4');

        //Lấy dữ liệu từ bảng nhatkyphanhoi & chitietnhatkyphanhoi
        $info_PH = $this->nhatkyphanhoi_model->get_info($id_PH);
        $info_CT = $this->chitietnhatkyphanhoi_model->get_info($id_NKPH);

        //Get id user login
        $user_id_login = $this->session->userdata('login');

        //kiểm tra dữ liệu lấy được từ bảng chitietnhatkyphanhoi
        if(!$info_CT)
        {
            $this->session->set_flashdata('message', 'Không lấy được dữ liệu');
            redirect(admin_url('home'));
        } else if($info_CT->TrangThai == 0){
            $this->session->set_flashdata('message', 'Dữ liệu chưa được duyệt');
            redirect(admin_url('home'));
        } else if($info_CT->TrangThai == 2){
            $this->session->set_flashdata('message', 'Thiết bị đã được sửa chữa');
            redirect(admin_url('home'));
        } else if($info_PH->MaNVDuyet != $user_id_login){
            $this->session->set_flashdata('message', 'Bạn không phải là người dùng có quyền duyệt phản hồi này');
            redirect(admin_url('home'));
        }       

        //Check id của người đang đăng nhập
        if(!$user_id_login){
            $this->session->set_flashdata('message', 'Không tìm thấy người dùng đăng nhập');
            redirect(admin_url('home'));
        }

        //Lấy ra danh sách các tình trạng thiết bị
        $input = array();
        $tttb = $this->tinhtrangthietbi_model->get_list($input);

        //Lấy danh sách các id của đối tượng tttb (tình trạng thiết bị)
        $list = array();
        foreach ($tttb as $row) 
        {
            $list[] = $row->MaTTTB;
        }

        //Lấy tình trạng thiết bị có sẵn trong bảng phong-thietbi
        $p_tb = $this->phong_thietbi_model->get_info($info_CT->MaPTB);

        /*
            params: 
                $soluong: số lượng của thiết bị trong phản hồi
                $tinhtrang: tình trạng của thiết bị trong phản hồi
        */
        $soluong = $info_CT->SoLuong;
        $tinhtrang = $info_CT->TinhTrang;

        $list_TTTB = array();

        if($p_tb)
        {

            $list_TTTB = json_decode($p_tb->TTTB, true);
            /* 
                Duyệt qua mảng các maTTTB
                Nếu tình trạng trong mảng trùng với tình trạng được post lên thì cộng dồn thêm vào tình trạng đó
                Nếu không tồn tại tình trạng nào đó thì khởi tạo nó 0.
            */
            foreach($list as $item)
            {
                if($item == $tinhtrang)
                {
                    $list_TTTB[$item] -= $soluong;

                    //reset the array
                    $list_TTTB['1'] += $soluong; 
                }
            }

            //encode the $list_TTTB and send to phong_thietbi_model
            if($this->phong_thietbi_model->repaired(json_encode($list_TTTB), $info_CT->MaPTB))
                $status++;

            //Gọi repaired (đã sửa) trong model
            if($this->chitietnhatkyphanhoi_model->repaired($id_NKPH))
                $status++;

            //if both chitietnhatkyphanhoi_model and phong_thietbi_model successed
            if($status == 2)
                $this->session->set_flashdata('message', 'Đã cập nhật thành công');
            else $this->session->set_flashdata('message', 'Cập nhật thất bại');
        }
        else $this->session->set_flashdata('message', 'Không tìm thấy đối tượng duyệt');

        redirect(admin_url('home'));

    }
}

