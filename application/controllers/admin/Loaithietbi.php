<?php

Class Loaithietbi extends MY_Controller {

	function __construct() {
	        parent::__construct();
	        $this->load->model('loaithietbi_model');
	    }
	function index() {

        $input = array();

        $tenloai = $this->input->get('tenloai');
        if ($tenloai) {
            $input['like'] = array('TenLoai',$tenloai);
        }

        $list =  $this->loaithietbi_model->get_list($input);
        $this->data['list'] = $list;

        //pre($data);
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'admin/loaithietbi/index';
        $this->load->view('admin/main', $this->data);
    }

    function add() {
        $this->load->library('form_validation');
        $this->load->helper('form');
       
        //Neu ma submit co dư lieu post len thi kt
        if ($this->input->post()) {
            $this->form_validation->set_rules('tenloai', 'Tên loại thiết bị', 'required');

            if ($this->form_validation->run()) {
                //them vao csdl
                $TenLoai = $this->input->post('tenloai');
                $MoTa = $this->input->post('mota');
                
                $data = array(
                    'MaLoaiTB' => $MaLoaiTB,
                    'TenLoai' => $TenLoai,
                    'MoTa' => $MoTa,
                );
        
                if ($this->loaithietbi_model->create($data))
                    $this->session->set_flashdata('message', 'Thêm mới thành công');
                else
                    $this->session->set_flashdata('message', 'Không thêm đươc');

                redirect(admin_url('loaithietbi'));
            }
        }

        $this->data['temp'] = 'admin/loaithietbi/add';
        $this->load->view('admin/main', $this->data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $id = $this->uri->rsegment('3');

        //Lấy thông tin của quyền theo MaQuyen
        $info = $this->loaithietbi_model->get_info($id);

        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại thiết bị này');
            redirect(admin_url('loaithietbi'));
        }
        $this->data['info'] = $info;
        //Neu ma submit co dư lieu post len thi kt

        if ($this->input->post()) {
            $this->form_validation->set_rules('tenloai', 'Tên quyền', 'required');

            if ($this->form_validation->run()) {

                $MaLoaiTB = $this->input->post('maloaitb');
                $TenLoai = $this->input->post('tenloai');
                $MoTa = $this->input->post('mota');
            
                $data = array(
                    'TenLoai' => $TenLoai,
                    'MoTa' => $MoTa,
                );

                if ($this->loaithietbi_model->update($id, $data))
                    $this->session->set_flashdata('message', 'Cập nhật thành công');
                else
                    $this->session->set_flashdata('message', 'Lỗi cập nhật');

                redirect(admin_url('loaithietbi'));
           }
        }

        $this->data['temp'] = 'admin/loaithietbi/edit';
        $this->load->view('admin/main', $this->data);
    }

    function delete() {
        $id = $this->uri->rsegment('3');
        //Lấy thông tin quản trị viên
        $info = $this->loaithietbi_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại loại thiết bị này');
            redirect(admin_url('loaithietbi'));
        }
        //Thuc hien xoa
        $this->load->model('thietbi_model');
        $thietbi = $this->thietbi_model->get_info_rule(array('MaLoaiTB' => $info->MaLoaiTB), 'MaLoaiTB');
        
        $this->loaithietbi_model->delete_key($id);

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');

        redirect(admin_url('loaithietbi'));
    }

    function check_id() {
        $id = $this->input->post('maloaitb');
        $where = array('MaLoaiTB' => $id);
        if ($this->loaithietbi_model->check_exists($where)) {
            $this->form_validation->set_message(__FUNCTION__, 'Mã đã tồn tại');
            return false;
        }
        return true;
    }
}