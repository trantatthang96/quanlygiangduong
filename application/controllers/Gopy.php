<?php

class Gopy extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('email');
    }

    function index() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com'; //neu sử dụng gmail
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'ntuquanlygiangduong@gmail.com';
        $config['smtp_pass'] = 'Nhatrang1234';
        $config['mailtype'] = 'html'; //nếu sử dụng gmail
        $config['charset'] = 'utf-8'; //nếu sử dụng gmail
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $this->email->initialize($config);

        $this->email->from($email, $name);
        $this->email->to('tatthang96@gmail.com');
        $this->email->subject($subject);
        $this->email->message($message);

        $isSend = '';
        if ($this->input->post()) {
            if (!$this->email->send()) {
                $isSend = "Không thể gởi góp ý. Lỗi: " . $this->email->print_debugger();
                $this->email->clear();
            } else {
                $isSend = "Gởi góp ý thành công";
                $this->email->clear();
            }
        };

        $this->data['isSend'] = $isSend;
        $this->data['temp'] = 'site/gopy/index';
        $this->load->view('site/layout', $this->data);
    }

}
