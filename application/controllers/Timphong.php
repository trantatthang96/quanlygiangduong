﻿<?php

Class Timphong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('giangduong_model');
        $this->load->model('phong_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('thietbi_model');
    }

    function index() {
        //giangduong
        $sapxepgd['order'] = array('MaGD', 'ASC');
        $giangduong = $this->giangduong_model->get_list($sapxepgd);

        //Tim Phong
        $list = array();

        $maphong = $this->input->post('maphong');

        $tenphong = $this->input->post('tenphong');

        $magd = $this->input->post('magd');

        $succhua = $this->input->post('succhua');

        $tinhtrang = $this->input->post('tinhtrang');

        $loaithietbi = $this->input->post('loaithietbi');
        
        $maychieu = $this->input->post('maychieu');
        
        $tivi = $this->input->post('tivi');
        
        $loa = $this->input->post('loa');

        $DieuKien = array();
        if ($maphong) {
            $DieuKien['where']['phong.MaPhong'] = $maphong;
        }

        if ($tenphong) {
            $DieuKien['like'] = array('phong.TenPhong', $tenphong);
        }

        if ($magd) {
            $DieuKien['where']['phong.MaGD'] = $magd;
        }

        if ($succhua) {
            $DieuKien['where']['phong.SucChua' . '>='] = $succhua;
        }

        if ($tinhtrang) {
	    if($tinhtrang == 2){
		$tinhtrang = 0;
	    }
            $DieuKien['where']['phong.TinhTrang'] = $tinhtrang;
        }

        if ($maychieu == 1) {
            $DieuKien['like'] = array('thietbi.TenTB', 'máy chiếu');
        }

        if ($tivi == 1) {
            $DieuKien['like'] = array('thietbi.TenTB', 'tivi');
        }

        if ($loa == 1) {
            $DieuKien['like'] = array('thietbi.TenTB', 'loa');
        }

        if ($this->input->post()) {
            $list = $this->phong_model->getRoom($DieuKien);
        }


        $this->data['giangduong'] = $giangduong;
        $this->data['list'] = $list;
        $this->data['temp'] = 'site/timphong/index';
        $this->load->view('site/layout', $this->data);
    }

}
