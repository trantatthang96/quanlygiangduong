<?php

Class Login extends MY_Controller {

    function index() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        if ($this->input->post()) {
            $this->form_validation->set_rules('login', 'login', 'callback_check_login');

            if ($this->form_validation->run()) {
                $user = $this->_get_user_info();
                $this->session->set_userdata('user_id_login', $user->MaND);


                //Login Cho Admin
                $this->session->set_userdata('login', $user->MaND);
                $isAdmin = 0;
                if ($user->MaLoaiND == '1')
                    $isAdmin = 1;
                $this->session->set_userdata('isAdmin', $isAdmin);

                if ($this->session->userdata('page_url')){
                    //redirect($this->session->userdata('page_url'));
                    //pre($this->session->userdata('page_url'));
                    $page_url = $this->session->userdata('page_url');
                    redirect(base_url("$page_url"));
                }
                else
                    redirect(base_url('home'));
            }
        }

        //$this->load->view('site/login/index');
        $this->data['temp'] = 'site/login/index';
        $this->load->view('site/login_layout', $this->data);
    }

    function check_login() {
        $user = $this->_get_user_info();
        if ($user)
            return true;
        $this->form_validation->set_message(__FUNCTION__, 'Đăng nhập không thành công');
        return false;
    }

    function _get_user_info() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password = md5($password);
        //pre($username);
        $this->load->model('nguoidung_model');
        $where = array('Username' => $username, 'Password' => $password);
        $user = $this->nguoidung_model->get_info_rule($where);
        return $user;
    }

}
