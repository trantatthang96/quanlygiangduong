<?php

class Baohongnhanh extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('giangduong_model');
        $this->load->model('phong_model');
        $this->load->model('nguoidung_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('tinhtrangthietbi_model');
        $this->load->model('nhatkyphanhoi_model');
        $this->load->model('thietbi_model');
        $this->load->model('chitietnhatkyphanhoi_model');
    }

    /*
     * Hien thi danh sach bài viết
     */

    function index() {
        $input['order'] = ['MaGD', 'ASC'];
        $inputPH['order'] = ['MaPhong', 'ASC'];
        $temp_url = '';
        
        $magd = $this->input->get('magd');
        $maphong = $this->input->get('maphong');
        if ($magd) {
            $inputPH['where']['MaGD'] = $magd;
            $temp_url="?magd=".$magd;
        }


        $giangduong = $this->giangduong_model->get_list($input);
        $phonghoc = $this->phong_model->get_list($inputPH);

        if ($maphong && $magd) {
            $DieuKien['MaPhong'] = $maphong;
            $dsthietbi = $this->phong_thietbi_model->get_Join_where('thietbi', 'MaTB', 'MaTB', $DieuKien);
            $this->data['dsthietbi'] = $dsthietbi;
            $temp_url="?magd=".$magd."&maphong=".$maphong;
        }
        
        $this->session->set_userdata('page_url',  uri_string().$temp_url);

        $this->data['giangduong'] = $giangduong;
        $this->data['phonghoc'] = $phonghoc;
        $data['temp'] = 'site/baohongnhanh/index';
        $this->load->view('site/layout', $data);
    }

}
