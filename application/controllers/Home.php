﻿<?php

class Home extends MY_Controller {

    function index() {
        $data = array();
        $data['temp'] = 'site/home/index';
        $this->load->view('site/layout', $data);
    }

    function edit() {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $user_id_login = $this->session->userdata('user_id_login');
        $info = $this->nguoidung_model->get_info($user_id_login);
        $this->data['info'] = $info;

        if ($this->input->post()) {
            $this->form_validation->set_rules('hotennd', 'Họ và tên người dùng', 'required');


            $password = $this->input->post('password');
            if ($password) {
                $this->form_validation->set_rules('password', 'Mật khẩu người dùng', 'required|min_length[6]');
                $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
            }

            if ($this->form_validation->run()) {
                $MaND = $this->input->post('mand');
                $HoTenND = $this->input->post('hotennd');
                $NgaySinh = $this->input->post('ngaysinh');
                $GioiTinh = $this->input->post('gioitinh');
                $DienThoai = $this->input->post('dienthoai');
                $Email = $this->input->post('email');
                $this->load->library('upload_library');
                $upload_path = './upload/nguoidung';
                $upload_data = $this->upload_library->upload($upload_path, 'image');
                $image = '';
                if (isset($upload_data['file_name'])) {
                    $image = $upload_data['file_name'];
                    //$data['HinhAnh'] = $image;
                }

		if($GioiTinh == 0)
                    $GioiTinh = 0;
                $data = array(
                    'HoTenND' => $HoTenND,
                    'Username' => $info->Username,
                    'NgaySinh' => $NgaySinh,
                    'GioiTinh' => $GioiTinh,
                    'DienThoai' => $DienThoai,
                    'Email' => $Email,
                );

                if ($password) {
                    $data['Password'] = md5($password);
                }

                if ($image != '') {
                    $data['HinhAnh'] = $image;
                    $image_del = './upload/nguoidung/' . $info->HinhAnh;
                    if (file_exists($image_del)) {
                        unlink($image_del);
                    }
                }
                //pre($data);

                if ($this->nguoidung_model->update($info->MaND, $data)) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Cập nhật dữ liệu thành công</div>');
                    redirect(base_url('home/edit'));
                }
            }
        }

        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['temp'] = 'site/login/edit';
        $this->load->view('site/layout', $this->data);
    }

    function logout() {
        if ($this->session->userdata('user_id_login')) {
            $this->session->unset_userdata('user_id_login');
        }
        if ($this->session->userdata('login')) {
            $this->session->unset_userdata('login');
        }
        if ($this->session->userdata('page_url'))
        {
            $this->session->unset_userdata('page_url');
        }

        redirect(base_url('home'));
    }

}
