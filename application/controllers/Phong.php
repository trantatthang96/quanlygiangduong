<?php

Class Phong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('giangduong_model');
        $this->load->model('phong_model');
        $this->load->model('nguoidung_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('tinhtrangthietbi_model');
        $this->load->model('nhatkyphanhoi_model');
        $this->load->model('thietbi_model');
        $this->load->model('chitietnhatkyphanhoi_model');
    }

    function index() {
        $MaPhong = $this->input->get('mp');

        $phong = $this->phong_model->get_info($MaPhong);
        if (!$phong) {
            redirect(base_url('giangduong'));
        }

        //pre($phong);
        //Nhan Vien
        $gd = $this->giangduong_model->get_info($phong->MaGD);
        $nhanvien = $this->nguoidung_model->get_info($gd->MaNVQL);

        $DieuKien = array();
        $DieuKien['MaPhong'] = $MaPhong;
        $dsthietbi = $this->phong_thietbi_model->get_Join_where('thietbi', 'MaTB', 'MaTB', $DieuKien);


        $user = $this->session->userdata('user_id_login');

        if ($user) {
//            pre($listNhatKyPH);
            $listNhatKyPH = $this->nhatkyphanhoi_model->get_Join_muchTable($DieuKien, $user);
        } else {
            $listNhatKyPH = $this->nhatkyphanhoi_model->get_Join_muchTable($DieuKien);
        }
        //pre($listNhatKyPH);

        foreach ($dsthietbi as $rows) {
            $s = "";
            $dstinhtrang = json_decode($rows->TTTB);
            foreach ($dstinhtrang as $key => $value) {

                $tttb = $this->tinhtrangthietbi_model->get_info($key);
                $s .= $tttb->TenTT . ':' . $value . ' | ';
            }
            $s = substr_replace($s, "", -2);
            $rows->TTTB = $s;
        }

        $input['order'] = array('MaGD', 'asc');
        $list_gd = $this->giangduong_model->get_list($input);
        $this->data['list_gd'] = $list_gd;
        //pre($list_gd);
        //$list = $this->giangduong_model->get_Join_where('phong', 'MaGD', 'MaGD');
        //pre($list);
        $giangduong = $this->giangduong_model->get_list($input);
        foreach ($giangduong as $row) {
            $input['where'] = array('MaGD' => $row->MaGD);
            $subs = $this->phong_model->get_list($input);
            $row->subs = $subs;
        }

        $this->session->set_userdata('page_url', uri_string() . "?mp=" . $MaPhong);

        $this->data['phong'] = $phong;
        $this->data['giangduong'] = $giangduong;
        $this->data['TenGD'] = $gd->TenGD;
        $this->data['nhanvien'] = $nhanvien;
        $this->data['dsthietbi'] = $dsthietbi;
        $this->data['listNhatKyPH'] = $listNhatKyPH;
        $this->data['temp'] = 'site/phong/index';
        $this->load->view('site/layout', $this->data);
    }

    function baohong() {
        $maPTB = $this->input->get('maPTB');
        if (!$maPTB) {
            redirect(base_url('giangduong'));
        }
        $phongtb = $this->phong_thietbi_model->get_info($maPTB);
        $thietbi = $this->thietbi_model->get_info($phongtb->MaTB);
        //pre($phongtb)
        $phong = $this->phong_model->get_info($phongtb->MaPhong);
        $giangduong = $this->giangduong_model->get_info($phong->MaGD);
        $tinhtrangtb = $this->tinhtrangthietbi_model->get_list();


        $this->data['thietbi'] = $thietbi;
        $this->data['tinhtrangtb'] = $tinhtrangtb;
        $this->data['phongtb'] = $phongtb;
        $this->data['temp'] = 'site/phong/baohong';
        $this->load->view('site/phong/baohong', $this->data);
    }

    function suabaohong() {
        $maPTB = $this->input->get('maPTB');
        $maPH = $this->input->get('maPH');
        if (!$maPTB) {
            redirect(base_url('giangduong'));
        }

        $phongtb = $this->phong_thietbi_model->get_info($maPTB);
        $thietbi = $this->thietbi_model->get_info($phongtb->MaTB);
        //pre($phongtb)

        $phong = $this->phong_model->get_info($phongtb->MaPhong);
        $giangduong = $this->giangduong_model->get_info($phong->MaGD);
        $tinhtrangtb = $this->tinhtrangthietbi_model->get_list();
        $phanhoi = $this->nhatkyphanhoi_model->get_info($maPH);

        $input['where'] = array('MaPH' => $maPH);
        $listMaNKPH = $this->chitietnhatkyphanhoi_model->get_list($input);
        //pre($MaNKPH);   

        $this->data['thietbi'] = $thietbi;
        $this->data['tinhtrangtb'] = $tinhtrangtb;
        $this->data['phongtb'] = $phongtb;
        $this->data['phanhoi'] = $phanhoi->GhiChu;
        $this->data['listCTNK'] = $listMaNKPH;
        $this->data['temp'] = 'site/phong/suabaohong';
        $this->load->view('site/phong/suabaohong', $this->data);
    }

    function editbaohong() {
        $maPTB = $this->input->post('maPTB');
        $maPH = $this->input->post('maPH');
        if (!$maPTB) {
            redirect(base_url('giangduong'));
        }
        $ghichu = $this->input->post('ghichu');
        $soluong = $this->input->post('soluong');
        $tinhtrang = $this->input->post('tinhtrang');
        $MoTaViTri = $this->input->post('MoTaViTri');
        $MoTa = $this->input->post('MoTa');

        //-----------Them vao bang nhatkyphanhoi
        $phongtb = $this->phong_thietbi_model->get_info($maPTB);
        $phong = $this->phong_model->get_info($phongtb->MaPhong);
        $giangduong = $this->giangduong_model->get_info($phong->MaGD);
        $user_id_login = $this->session->userdata('user_id_login');
        $info = $this->nguoidung_model->get_info($user_id_login);

        if (!$user_id_login) {
            echo 'fail';
            return;
        }

        if ($info->MaLoaiND == 1 || $info->MaLoaiND == 2 || $info->MaLoaiND == 3) {
            $nkphData = array(
                'ThoiGianGoi' => now(),
                'GhiChu' => $ghichu
            );

            if ($this->nhatkyphanhoi_model->update($maPH, $nkphData)) {
                //-----------Them vao chitietnhatkyphanhoi


                $input['where'] = array('MaPH' => $maPH);
                $listMaNKPH = $this->chitietnhatkyphanhoi_model->get_list($input);
                //pre($MaNKPH);
                $MaNKPH = $listMaNKPH[0]->MaNKPH;

                $chitietnkphData = array(
                    'SoLuong' => $soluong,
                    'TinhTrang' => $tinhtrang,
                    'MoTaViTri' => $MoTaViTri,
                    'MoTa' => $MoTa,
                );
                if ($this->chitietnhatkyphanhoi_model->update($MaNKPH, $chitietnkphData)) {
                    echo 'success';
                    return;
                } else {
                    echo 'fail';
                    return;
                }
                //----------Them vao chitietnhatkyphanhoi
            } else {
                echo 'fail';
                return;
            }
        } else {
            echo 'BlockAccess';
            return;
        }
        //       ------------Them vao bang nhatkyphanhoi
        //pre($test);
    }

    function deleteBaoHong() {
        $maPH = $this->input->get('maPH');
        $input['where'] = array('MaPH' => $maPH);
        $listMaNKPH = $this->chitietnhatkyphanhoi_model->get_list($input);
        //pre($MaNKPH);
        $MaNKPH = $listMaNKPH[0]->MaNKPH;
        if ($this->chitietnhatkyphanhoi_model->delete($MaNKPH)) {
            if ($this->nhatkyphanhoi_model->delete($maPH)) {
                echo 'success';
                return;
            }
        } else {
            echo 'fail';
            return;
        }
        echo 'fail';
        return;
    }

    function post() {
        $maPTB = $this->input->post('maPTB');
        if (!$maPTB) {
            redirect(base_url('giangduong'));
        }
        $ghichu = $this->input->post('ghichu');
        $soluong = $this->input->post('soluong');
        $tinhtrang = $this->input->post('tinhtrang');
        $MoTaViTri = $this->input->post('MoTaViTri');
        $MoTa = $this->input->post('MoTa');

        //-----------Them vao bang nhatkyphanhoi
        $phongtb = $this->phong_thietbi_model->get_info($maPTB);
        $phong = $this->phong_model->get_info($phongtb->MaPhong);
        $giangduong = $this->giangduong_model->get_info($phong->MaGD);
        $user_id_login = $this->session->userdata('user_id_login');
        $info = $this->nguoidung_model->get_info($user_id_login);


        if (!$user_id_login) {
            echo 'fail';
            return;
        }
//        
        if ($info->MaLoaiND == 1 || $info->MaLoaiND == 2 || $info->MaLoaiND == 3) {
            $nkphData = array(
                'MaGVPH' => $info->MaND,
                'MaNVDuyet' => $giangduong->MaNVQL,
                'ThoiGianGoi' => now(),
                'GhiChu' => $ghichu
            );

            if ($this->nhatkyphanhoi_model->create($nkphData)) {
                //-----------Them vao chitietnhatkyphanhoi
                $getLastMaPH = $this->nhatkyphanhoi_model->get_Last_MaPH();
                $MaPH = $getLastMaPH[0]->MaPH;

//        Them vao chi tiet nhat ky phan hoi
                $chitietnkphData = array(
                    'MaPH' => $MaPH,
                    'SoLuong' => $soluong,
                    'TinhTrang' => $tinhtrang,
                    'MoTaViTri' => $MoTaViTri,
                    'MoTa' => $MoTa,
                    'ThoiGianDuyet' => 0,
                    'TrangThai' => 0,
                    'MaPTB' => $maPTB
                );
                if ($this->chitietnhatkyphanhoi_model->create($chitietnkphData)) {
                    echo 'success';
                    return;
                } else {
                    echo 'fail';
                    return;
                }
                //----------Them vao chitietnhatkyphanhoi
            } else {
                echo 'fail';
                return;
            }
        } else {
            echo 'BlockAccess';
            return;
        }
        //       ------------Them vao bang nhatkyphanhoi
        //pre($test);
    }

}
