<?php

Class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('loainguoidung_model');
        $this->load->model('nguoidung_model');
    }

    function index() {
        $loainguoidung = $this->loainguoidung_model->get_list();
        $this->data['loainguoidung'] = $loainguoidung;
        foreach ($loainguoidung as $row) {
            $input['where'] = array('MaLoaiND' => $row->MaLoaiND);
            $count = $this->nguoidung_model->get_list($input);
            $row->count = count($count);
        }

        $nguoidung = $this->nguoidung_model->get_list();
        $this->data['nguoidung'] = $nguoidung;

        $this->data['temp'] = 'site/user/index';
        $this->load->view('site/layout', $this->data);
    }

    function xemchitiet() {
        $mand = $this->input->post('manguoidung');
        $info = $this->nguoidung_model->get_info($mand);
        $result = json_encode($info);
        echo $result;
    }

    function type() {
        $loainguoidung = $this->loainguoidung_model->get_list();
        $this->data['loainguoidung'] = $loainguoidung;
        foreach ($loainguoidung as $row) {
            $input['where'] = array('MaLoaiND' => $row->MaLoaiND);
            $count = $this->nguoidung_model->get_list($input);
            $row->count = count($count);
        }


        $maloai = $this->input->get('loai');
        $input['where']['MaLoaiND'] = $maloai;
        $nguoidung = $this->nguoidung_model->get_list($input);
        $this->data['nguoidung'] = $nguoidung;

        $this->data['temp'] = 'site/user/index';
        $this->load->view('site/layout', $this->data);
    }

//    function login(){
//        
//        $this->load->library('form_validation');
//        $this->load->helper('form');
//        if ($this->input->post())
//        {
//            $this->form_validation->set_rules('login', 'login', 'callback_check_login');
//            if ($this->form_validation->run())
//            {
//                $user = $this->_get_user_info();
//                $this->session->set_userdata('user_id_login', $user->MaND);
//                
//                redirect(base_url('home'));
//            }
//        }
//        
//        $this->data['temp'] = 'site/user/login';
//        $this->load->view('site/user/login', $this->data);
//    }
//    
//    function check_login()
//    {
//        $user = $this->_get_user_info();
//        if ($user)
//            return true;
//        $this->form_validation->set_message(__FUNCTION__,'Không đăng nhập thành công');
//        return false;
//    }
//
//    function _get_user_info(){
//        $username = $this->input->post('username');
//        $password = $this->input->post('password');
//        $password = md5($password);
//        //pre($username);
//        $this->load->model('nguoidung_model');
//        $where = array('username' => $username, 'password' => $password);
//        $user = $this->nguoidung_model->get_info_rule($where);
//        return $user;
//    }
//    
//    function logout() {
//        if ($this->session->userdata('user_id_login'));
//        {
//            $this->session->unset_userdata('user_id_login');
//        }
//        redirect(base_url('home'));
//    }
//    
//    function edit(){
//        $this->load->library('form_validation');
//        $this->load->helper('form');
//        
//        $user_id_login = $this->session->userdata('user_id_login');
//        $info = $this->nguoidung_model->get_info($user_id_login);
//        $this->data['info'] = $info;
//
//        if ($this->input->post()) {
//            $this->form_validation->set_rules('hotennd', 'Họ và tên người dùng', 'required');
//            $this->form_validation->set_rules('username', 'Tài khoản người dùng', 'required');
//          
//          
//            $password = $this->input->post('password');
//            if ($password) {
//                $this->form_validation->set_rules('password', 'Mật khẩu người dùng', 'required|min_length[6]');
//                $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'matches[password]');
//            }
//
//            if ($this->form_validation->run()) {
//                $MaND = $this->input->post('mand');
//                $HoTenND = $this->input->post('hotennd');
//                $Username = $this->input->post('username');
//                $NgaySinh = $this->input->post('ngaysinh');
//                $GioiTinh = $this->input->post('gioitinh');
//                $DienThoai = $this->input->post('dienthoai');
//                $Email = $this->input->post('email');
//                $MaNhom = $this->input->post('manhom');
//
//                $this->load->library('upload_library');
//                $upload_path = './upload/nguoidung';
//                $upload_data = $this->upload_library->upload($upload_path, 'image');
//                $image = '';
//                if (isset($upload_data['file_name'])) {
//                    $image = $upload_data['file_name'];
//                    //$data['HinhAnh'] = $image;
//                }
//
//                $data = array(
//                    'MaND' => $info->MaND,
//                    'HoTenND' => $HoTenND,
//                    'Username' => $Username,
//                    'NgaySinh' => $NgaySinh,
//                    'GioiTinh' => $GioiTinh,
//                    'DienThoai' => $DienThoai,
//                    'Email' => $Email,
//                    'MaNhom' => $info->MaNhom,
//                );
//
//                if ($password) {
//                    $data['Password'] = md5($password);
//                }
//
//                if ($image != '') {
//                    $data['HinhAnh'] = $image;
//                    $image_del = './upload/nguoidung/' . $info->HinhAnh;
//                    if (file_exists($image_del)) {
//                        unlink($image_del);
//                    }
//                }
//                //pre($data);
//
//                if ($this->nguoidung_model->update($info->MaND, $data))
//                    redirect (base_url('user/edit'));
//
//            }
//        }
//        
//        $this->data['temp'] = 'site/user/edit';
//        $this->load->view('site/layout', $this->data);
//    }
}
