<?php

Class Thietbi extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('phong_model');
        $this->load->model('phong_thietbi_model');
        $this->load->model('tinhtrangthietbi_model');
        $this->load->model('thietbi_model');
        $this->load->model('loaithietbi_model');
    }

    function index() {
        //Loai thiet bi

        $loaitb = $this->loaithietbi_model->get_list();
        $MaLoaiTB = $this->input->get('maloaitb');
        
        foreach ($loaitb as $row) {
            $input['where'] = array('MaLoaiTB' => $row->MaLoaiTB);
            $soTB = $this->thietbi_model->get_list($input);
            $row->count = count($soTB);
        }
        $this->data['loaitb'] = $loaitb;

        //Thietbi

        $dieukien = array();
        $tentb = $this->input->get('search');
        if ($tentb) {
            //pre($tentb);
            $dieukien['like'] = array('TenTB', $tentb);
        } elseif ($MaLoaiTB) {
            $dieukien['where'] = array('MaLoaiTB' => intval($MaLoaiTB));
        } else {
            $total_rows = $this->thietbi_model->get_total();
            $this->data['total_rows'] = $total_rows;

            //load ra thu vien phan trang
            $this->load->library('pagination');
            $config = array();
            $config['total_rows'] = $total_rows; //tong tat ca cac bài viết tren website
            $config['base_url'] = base_url('thietbi/index'); //link hien thi ra danh sach bài viết
            $config['per_page'] = 20; //so luong bài viết hien thi tren 1 trang
            $config['uri_segment'] = 3; //phan doan hien thi ra so trang tren url
            $config['next_link'] = 'Trang kế tiếp';
            $config['prev_link'] = 'Trang trước';
            $this->pagination->initialize($config);
            $segment = $this->uri->segment(3);
            $segment = intval($segment);
//        pre($segment);
            $input = array();
            $input['limit'] = array($config['per_page'], $segment);
        }
        //


        $thietbi = $this->thietbi_model->get_list($dieukien);
        
        foreach ($thietbi as $row) {
            $img = json_decode($row->HinhAnh);
            $row->HinhDaiDien = $img[0];
        }
        //khoi tao cac cau hinh phan trang
        $this->data['thietbi'] = $thietbi;
//        pre($thietbi);
        $this->data['temp'] = 'site/thietbi/index';
        $this->load->view('site/layout', $this->data);
    }

    function view() {
        $maTB = $this->input->get('matb');

        $input['phong-thietbi.MaTB'] = $maTB;
        $phongtb = $this->phong_thietbi_model->get_Join_TB($input);
        
  
        $tb = $this->thietbi_model->get_info($maTB);

        foreach ($phongtb as $rows) {
            $s = "";
            $dstinhtrang = json_decode($rows->TTTB);
            foreach ($dstinhtrang as $key => $value) {

                $tttb = $this->tinhtrangthietbi_model->get_info($key);
                $s .= $tttb->TenTT . ':' . $value . ' | ';
                
            }
            $s = substr_replace($s, "", -2);
            $rows->TTTB = $s;
        }
        
        
        //pre($phongtb);
        $this->data['tb'] = $tb;
        $this->data['phongtb'] = $phongtb;
        $this->data['temp'] = 'site/thietbi/view';
        $this->load->view('site/thietbi/view', $this->data);
    }

}
