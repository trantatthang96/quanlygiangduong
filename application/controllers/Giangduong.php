<?php

Class Giangduong extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('giangduong_model');
        $this->load->model('phong_model');
//        $this->load->model('phonghocthietbi_model');
//        $this->load->model('dangky_model');
    }

    function index() {
        //$MaGD = $this->input->get('view');
        //pre($MaGD);
        $input['order'] = array('MaGD', 'asc');
        $list_gd = $this->giangduong_model->get_list($input);
        $this->data['list_gd'] = $list_gd;
        //pre($list_gd);
        //$list = $this->giangduong_model->get_Join_where('phong', 'MaGD', 'MaGD');
        //pre($list);
        $giangduong = $this->giangduong_model->get_list($input);
        foreach ($giangduong as $row) {
            $input['where'] = array('MaGD' => $row->MaGD);
            $subs = $this->phong_model->get_list($input);
            $row->subs = $subs;
        }
        $this->data['giangduong'] = $giangduong;
        //pre($giangduong);
        //pre($info);
        $this->data['temp'] = 'site/giangduong/index';
        $this->load->view('site/layout', $this->data);
    }

    function view() {
        $MaGD = $this->input->get('gd');
        $info = $this->giangduong_model->get_info($MaGD);
        $input['order'] = array('MaPhong', 'ASC');
        $input['where']['MaGD'] = $MaGD;
        $list = $this->phong_model->get_list($input);
        //pre($list);
        $this->data['info'] = $info;
        $this->data['list'] = $list;
        $this->data['temp'] = 'site/giangduong/view';
        $this->load->view('site/giangduong/view', $this->data);
    }

    function search() {
        $maphong = $this->input->post('maphong');

        $tenphong = $this->input->post('tenphong');

        $giangduong = $this->input->post('giangduong');

        $succhua = $this->input->post('succhua');
        
        $tinhtrang = $this->input->post('trangthai');
        //echo $MaPhong;
        //$DieuKien['order'] = array('MaPhong','asc');
        $DieuKien = array();
        if ($maphong) {
            $DieuKien['where']['MaPhong'] = $maphong;
        }

        if ($tenphong) {
            $DieuKien['like'] = array('TenPhong', $tenphong);
        }

        if ($giangduong) {
            $DieuKien['where']['MaGD'] = $giangduong;
        }
        
        if ($succhua){
            $DieuKien['where']['SucChua'.'>='] = $succhua;
        }
        
        if ($tinhtrang){
            $DieuKien['where']['TinhTrang'] = $tinhtrang;
        }

        $list = $this->phong_model->get_list($DieuKien);
        //pre($list);
        // $input['like'] = array('name' => 'abc');
        $result = json_encode($list);
        //$this->data['list_ph'] = $list;
        echo $result;
        //$this->data['temp'] = 'site/giangduong/search';
        //$this->load->view('site/giangduong/search', $this->data);
    }

}
