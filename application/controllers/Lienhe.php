<?php

class Lienhe extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('nguoidung_model');
        $this->load->model('giangduong_model');
    }

    function index() {
        $DieuKien['MaLoaiND'] = 2;
        $nhanvien = $this->giangduong_model->get_Join_where('nguoidung', 'MaND', 'MaNVQL', $DieuKien);
        //pre($nhanvien);
        $this->data['nhanvien'] = $nhanvien;
        $this->data['temp'] = 'site/lienhe/index';
        $this->load->view('site/layout', $this->data);
    }

}
