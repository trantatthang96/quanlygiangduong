<?php 
class Chitietnhatkyphanhoi_model extends My_Model{
    
	var $table = 'chitietnhatkyphanhoi';
    var $key = 'MaNKPH';

    function get_list_mulTable($input = array()){
    	$this->db->select("chitietnhatkyphanhoi.*, phong.TenPhong, thietbi.TenTB, phong.MaPhong, thietbi.MaTB, tinhtrangthietbi.TenTT");
    	$this->db->from($this->table);
    	$this->db->join('nhatkyphanhoi','nhatkyphanhoi.MaPH = chitietnhatkyphanhoi.MaPH');
        $this->db->join('phong-thietbi','phong-thietbi.MaPTB = chitietnhatkyphanhoi.MaPTB');
    	$this->db->join('phong','phong.MaPhong = phong-thietbi.MaPhong');
    	$this->db->join('thietbi','thietbi.MaTB = phong-thietbi.MaTB');
        $this->db->join('tinhtrangthietbi','tinhtrangthietbi.MaTTTB = chitietnhatkyphanhoi.TinhTrang');
    	$this->get_list_set_input($input);
    	$query = $this->db->get();
    	return $query->result();
    }

    /*
        params: 
            $id: id của chi tiết phản hồi duyệt
    */
    function approval($id, $isAdmin, $user_id = '', $idPH = ''){

        $data = array(
            'TrangThai' => 1,
            'ThoiGianDuyet' => now()
        );

        $result = $this->db->update($this->table, $data, array('MaNKPH' => $id));

        if($isAdmin){
            $changeNVDuyetByAdmin = array('MaNVDuyet' => $user_id);

            $adminApprovalNKPH = $this->db->update('nhatkyphanhoi', $changeNVDuyetByAdmin, array('MaPH' => $idPH));
        }
        

        return $result;
    }

    /*
        params: 
            $id: id của chi tiết phản hồi duyệt
    */
    function repaired($id){
        $data = array(
            'TrangThai' => 2
        );

        $result = $this->db->update($this->table, $data, array('MaNKPH' => $id));

        return $result;
    }
}