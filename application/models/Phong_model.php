<?php

Class Phong_model extends MY_Model {

    var $table = 'phong';
    var $key = 'MaPhong';

    function get_Join_P_GD($input = array()) {
        $this->db->select("phong.*, giangduong.TenGD");
        $this->db->from($this->table);
        $this->db->join('giangduong', "giangduong.MaGD = phong.MaGD");
        $this->get_list_set_input($input);
        $query = $this->db->get();
        return $query->result();
    }

    function get_Join_muchTable($where = array()) {
        $this->db->select("*");
//        $this->db->select("$this->table.MaPH, TenTB, TenTT, chitietnhatkyphanhoi.SoLuong, HoTenND, ThoiGianDuyet, TrangThai");
        $this->db->from($this->table);
        $this->db->join('giangduong', "giangduong.MaGD = $this->table.MaGD");
        //$this->db->join('phong-thietbi', "phong-thietbi.MaPhong = phong.MaPhong");
        //$this->db->join('thietbi', "thietbi.MaTB = phong-thietbi.MaTB");
        //$this->db->join('tinhtrangthietbi', "tinhtrangthietbi.MaTTTB = chitietnhatkyphanhoi.TinhTrang");
        $this->db->where($where);
//        if ($user == 'guest') {
//            $this->db->where("(TrangThai=1 OR TrangThai=2)");
//        }
//        //$this->db->where("(TrangThai=0 OR TrangThai=1)");
//        $this->db->order_by("MaNKPH", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    function getRoom($input = array()) 
    {
        $field = array(
            'phong.*', 
            // 'thietbi.TenTB', 
            // 'thietbi.HinhAnh',
            // 'thietbi.MaTB',
            // 'phong-thietbi.MaTB'
        );

        $this->db->distinct();

        $this->db->select($field);

        $this->db->from('phong');

        $this->db->join('phong-thietbi', 'phong-thietbi.MaPhong = phong.MaPhong', 'left');

        $this->db->join('thietbi', 'phong-thietbi.MaTB = thietbi.MaTB', 'left');

        $this->get_list_set_input($input);

        $query = $this->db->get();

        return $query->result();
    }
}
