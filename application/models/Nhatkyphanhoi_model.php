<?php

Class Nhatkyphanhoi_model extends MY_Model {

    var $table = 'nhatkyphanhoi';
    var $key = 'MaPH';

    //Lấy lấy các dòng có người dùng có khóa ngoại chỉ định
    function get_ND_NKPH($MaND) {
        $query = "SELECT *
					FROM   nhatkyphanhoi
					WHERE  MaGVPH = '" . $MaND . "' or MaNVDuyet = '" . $MaND . "'";

        $result = $this->db->query($query);
        return $result->num_rows();
    }

    //hàm lấy danh sách Nhật ký phản hồi, bởi vì có 2 kết nối đến bảng người dùng
    //nên không thể dùng cách thông thường được
    function get_List_Join($input = array()) {
        $this->load->model("nhatkyphanhoi_model");

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join('nguoidung', "nguoidung.MaND = $this->table.MaGVPH");
        $this->get_list_set_input($input);
        $this->db->order_by("$this->table.ThoiGianGoi", "DESC");
        $query = $this->db->get();

        $list = $query->result();

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join('nguoidung', "nguoidung.MaND = $this->table.MaNVDuyet");
        $this->get_list_set_input($input);
        $this->db->order_by("$this->table.ThoiGianGoi", "DESC");
        $query_1 = $this->db->get();

        $list_1 = $query_1->result();

        //nếu có MaPH trùng nhau, ta thêm vào $list 1 thành phần NVDuyet chính là tên của nhân viên duyệt bên bảng người dùng
        foreach ($list as $k => $v) {
            foreach ($list_1 as $k1 => $v1) {
                if ($v->MaPH == $v1->MaPH) {
                    $v->NVDuyet = $v1->HoTenND;
                    $v->HA_NVDuyet = $v1->HinhAnh;
                }
            }
        }

        return $list;
    }

    function getNVQL() {
        $this->load->model('nguoidung_model');
        $this->load->model('giangduong_model');

        $this->db->select('nguoidung.*');
        $this->db->distinct();

        $this->db->from('nguoidung');
        $this->db->join('giangduong', 'nguoidung.MaND = giangduong.MaNVQL');

        $query = $this->db->get();
        return $query->result();
    }

    function get_Join_muchTable($where = array(), $user = 'guest') {
        //      $this->db->select("*");
        $this->db->select("$this->table.MaPH, nguoidung.MaND,phong-thietbi.MaPTB, TenTB, TenTT, chitietnhatkyphanhoi.SoLuong, HoTenND, ThoiGianDuyet, TrangThai");
        $this->db->from($this->table);
        $this->db->join('chitietnhatkyphanhoi', "chitietnhatkyphanhoi.MaPH = $this->table.MaPH");
        $this->db->join('phong-thietbi', "phong-thietbi.MaPTB = chitietnhatkyphanhoi.MaPTB");
        $this->db->join('thietbi', "thietbi.MaTB = phong-thietbi.MaTB");
        $this->db->join('tinhtrangthietbi', "tinhtrangthietbi.MaTTTB = chitietnhatkyphanhoi.TinhTrang");
        $this->db->join('nguoidung', "nguoidung.MaND = $this->table.MaGVPH");
        $this->db->where($where);
        if ($user == 'guest') {
            $this->db->where("(TrangThai=1 OR TrangThai=2)");
        }
        //$this->db->where("(TrangThai=0 OR TrangThai=1)");
        $this->db->order_by("MaNKPH", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    function get_Last_MaPH() {
        $this->db->select("MAX(`MaPH`) AS `MaPH`");
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

}
