<?php 
class Loainguoidung_model extends My_Model{

	var $table = 'loainguoidung';
    var $key = 'MaLoaiND';

    function get_Join_LND_Q($where = array()) {
        $this->db->select("loainguoidung.MaLoaiND, quyen.TenQuyen, loainguoidung.Mota, loainguoidung.TenLoai, loainguoidung.MaQuyen");
        $this->db->from('loainguoidung');
        $this->db->join('quyen', 'loainguoidung.MaQuyen = quyen.MaQuyen');
        $this->db->where($where);
        //$this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }
}