<?php

Class Phong_thietbi_model extends MY_Model {

    var $table = 'phong-thietbi';
    var $key = 'MaPTB';

    //Lấy ra danh sách các field cần thiết cho danh sách các chi tiết nhật ký phản hồi
    function getListIndex() {

        $field = array('phong-thietbi.*', 'phong.TenPhong', 'thietbi.TenTB', 'thietbi.HinhAnh');

        $this->db->select($field);

        $this->db->from('phong-thietbi');

        $this->db->join('thietbi', 'phong-thietbi.MaTB = thietbi.MaTB');
        $this->db->join('phong', 'phong-thietbi.MaPhong = phong.MaPhong');

        $query = $this->db->get();

        return $query->result();
    }

    function getList_Where($input = array()) {

        $field = array('phong-thietbi.*', 'phong.TenPhong', 'thietbi.TenTB', 'thietbi.HinhAnh');
        $new_input = $this->changeInput($input);
        $this->db->select($field);

        $this->db->from('phong-thietbi');

        $this->db->join('thietbi', 'phong-thietbi.MaTB = thietbi.MaTB');
        $this->db->join('phong', 'phong-thietbi.MaPhong = phong.MaPhong');
        $this->db->join('loaithietbi', 'loaithietbi.MaLoaiTB = thietbi.MaLoaiTB');

        // $this->db->where($where);
        $this->get_list_set_input($new_input);

        $query = $this->db->get();

        return $query->result();
    }

    function changeInput($input) 
    {
        if(isset($input['where'])) 
        {
            $where = $input['where'];
            if(isset($where['MaLoaiTB'])) 
            {
                $where['loaithietbi.MaLoaiTB'] = $where['MaLoaiTB'];
                unset($where['MaLoaiTB']);
            }
        }

        return $input;
    }

    function getListWhere($where = array()) {

        $field = array('phong-thietbi.*', 'phong.TenPhong', 'thietbi.TenTB', 'thietbi.HinhAnh');

        $this->db->select($field);

        $this->db->from('phong-thietbi');

        $this->db->join('thietbi', 'phong-thietbi.MaTB = thietbi.MaTB');
        $this->db->join('phong', 'phong-thietbi.MaPhong = phong.MaPhong');

        $this->db->where($where);

        $query = $this->db->get();

        return $query->row();
    }

    function delete_p_tb($id) {
        $where = array('MaPTB' => $id);
        $this->db->delete('phong-thietbi', $where);
        return true;
    }

    function getListPhong() {
        $this->db->select("DISTINCT phong.MaPhong, phong.TenPhong", FALSE);

        $this->db->from('phong-thietbi');

        $this->db->join('phong', 'phong-thietbi.MaPhong = phong.MaPhong');

        $query = $this->db->get();

        return $query->result();
    }

    function getListTB() {
        $this->db->select("DISTINCT thietbi.MaTB, thietbi.TenTB", FALSE);

        $this->db->from('phong-thietbi');

        $this->db->join('thietbi', 'phong-thietbi.MaTB = thietbi.MaTB');

        $query = $this->db->get();

        return $query->result();
    }

    /*

      params:
      $value: đoạn value được tính toán sẵn, chỉ cần thêm vào phòng, thiết bị
      $maTB, $maPhong: khóa chính của row cập nhật
     */

    function approval($value, $id) {

        $data = array(
            'TTTB' => $value
        );

        $result = $this->db->update($this->table, $data, array('MaPTB' => $id));

        return $result;
    }

    /*

      params:
      $value: đoạn value được tính toán sẵn, chỉ cần thêm vào phòng, thiết bị
      $maTB, $maPhong: khóa chính của row cập nhật
     */

    function repaired($value, $id) {

        $data = array(
            'TTTB' => $value
        );

        $result = $this->db->update($this->table, $data, array('MaPTB' => $id));

        return $result;
    }

    function get_Join_TB($where = array()) {
        $this->db->select("*");
        //$this->db->select("$this->table.MaPH, TenTB, TenTT, chitietnhatkyphanhoi.SoLuong, HoTenND, ThoiGianDuyet, TrangThai");
        $this->db->from($this->table);
        $this->db->join('thietbi', "thietbi.MaTB = $this->table.MaTB");
        $this->db->join('phong', "phong.MaPhong = $this->table.MaPhong");
        $this->db->where($where);
        $this->db->order_by("$this->table.MaPhong", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

}
