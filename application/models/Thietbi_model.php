<?php

class Thietbi_model extends MY_Model {

    var $table = 'thietbi';
    var $key = 'MaTB';

    function get_Join_TB_LTB($where = array()) {
        $this->db->select("thietbi.*,loaithietbi.TenLoai");
        $this->db->from($this->table);
        $this->db->join('loaithietbi', "thietbi.MaLoaiTB = loaithietbi.MaLoaiTB");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
}
