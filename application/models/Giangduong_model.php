<?php

Class Giangduong_model extends MY_Model {

    var $table = 'giangduong';
    var $key = 'MaGD';

    function getGiangDuong() {
        $this->db->select("*");
        $this->db->from('giangduong');
        $this->db->join('phonghoc', 'phonghoc.MaGD = giangduong.MaGD');
        $query = $this->db->get();
        return $query->result();
    }

    function get_Join_GD_ND($where = array()) {
        $this->db->select("giangduong.MaGD, giangduong.TenGD, giangduong.SoTang, giangduong.TongSoPhong, giangduong.HinhAnh, nguoidung.HoTenND, giangduong.MaNVQL");
        $this->db->from('giangduong');
        $this->db->join('nguoidung', 'giangduong.MaNVQL = nguoidung.MaND');
        $this->db->where($where);
        //$this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_Join_where_GD($joinTable, $joinKey, $refKey, $where = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($joinTable, "$joinTable.$joinKey = $this->table.$refKey");
        $this->db->where($where);
        $this->db->order_by('TenGD', 'ASC');
        //$this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    function demMaGD($id) {
        return count($this->db->get_where('giangduong', array("MaGD =" => $id)));
    }

    function countRoom($MaGD) {
        $input['where'] = array();
        $input['where']['MaGD'] = $MaGD;

        //Xuat du lieu ra
        $this->load->model('phonghoc_model');
        $list = $this->phonghoc_model->get_list($input);
        //pre($list);
        //$this->data['list'] = $list;
        return count($list);
    }

    function demPhongTH($MaGD) {
        //$input['where'] = array();
        $input['where'] = array('MaGD' => $MaGD, 'MaLP' => 'TH');

        //Xuat du lieu rar
        $this->load->model('phonghoc_model');
        $list = $this->phonghoc_model->get_list($input);
        //pre($list);
        //$this->data['list'] = $list;
        return count($list);
    }

    function demPhongHoc($MaGD) {
        //$input['where'] = array();
        $input['where'] = array('MaGD' => $MaGD, 'MaLP' => 'PH');
        //Xuat du lieu rar
        $this->load->model('phonghoc_model');
        $list = $this->phonghoc_model->get_list($input);
        return count($list);
    }

}
