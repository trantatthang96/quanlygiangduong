var base_url = "http://quanlygiangduong.lokatech.net/"
$(document).ready(function () {
    //Tim kiem tren giang duong
    $('#btnSearchGD').click(function (event) {
        var mp = $('#inputMaPhong').val();
        var tp = $('#inputTenPhong').val();
        var gd = $('#selGiangDuong').val();
        var tt = $('#selTinhTrang').val();
        var sc = $('#inputSucChua').val();
        var link = base_url + "qlgd/giangduong/search";
        $.ajax({
            url: 'giangduong/search',
            type: 'post',
            dataType: 'text',
            data: {
                maphong: mp,
                tenphong: tp,
                giangduong: gd,
                succhua: sc,
                trangthai: tt
            },
            success: function (result) {
                var obj = jQuery.parseJSON(result);
                var html = '<div class="row">';
                html += '<div class="col-md-12">';
                html += '<div class="panel panel-default">';
                html += '<div class="panel-heading text-center main-color-bg">Danh sách phòng học</div>';
                html += '<table class="table table-bordered text-center">';
                html += '<thead>';
                html += '<tr>';
                html += '<th class="text-center">STT</th>';
                html += '<th class="text-center">Mã phòng</th>';
                html += '<th class="text-center">Tên phòng</th>';
                html += '<th class="text-center">Sức chứa</th>';
                html += '<th class="text-center">Tình trạng</th>';
                html += '<th class="text-center">Thiết bị</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                for (var i = 0; i < obj.length; i++) {
                    console.log(obj[i].TenPhong);
                    html += '<tr>';
                    html += '<th class="text-center" scope="row">';
                    html += i + 1;
                    html += '</th>';
                    html += '<td>';
                    html += obj[i].MaPhong;
                    html += '</td>';
                    html += '<td>';
                    html += obj[i].TenPhong;
                    html += '</td>';
                    html += '<td>';
                    html += obj[i].SucChua;
                    html += '</td>';
                    html += '<td>';
                    var showTinhTrang = obj[i].TinhTrang === '1' ? '<span class="label label-success">Sử dụng được</span>': ' <span class="label label-danger">Không sử dụng được</span>' 
                    html += showTinhTrang;
                    html += '</td>';
                    html += '<td>';
                    var linkXem = base_url + 'qlgd/phong?mp='+ obj[i].MaPhong;
                    html += '<a href="'+ linkXem +'">Xem thiết bị';
                    html += '</td>';
                    html += '</tr>';
                }
                $('#loadView').html(html);
            }
        });
    });
    $('#list').click(function (event) {
        event.preventDefault();
        $('#dsthietbi .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#dsthietbi .item').removeClass('list-group-item');
        $('#products .item').addClass('grid-group-item');
    });

    $('.treeview').each(function () {
        var tree = $(this);
        tree.treeview();
    })

    $('.filterable .btn-filter').click(function () {
        var $panel = $(this).parents('.filterable'),
                $filters = $panel.find('.filters input'),
                $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9')
            return;
        var $input = $(this),
                inputContent = $input.val().toLowerCase(),
                $panel = $input.parents('.filterable'),
                column = $panel.find('.filters th').index($input.parents('th')),
                $table = $panel.find('.table'),
                $rows = $table.find('tbody tr');
        var $filteredRows = $rows.filter(function () {
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        $table.find('tbody .no-result').remove();
        $rows.show();
        $filteredRows.hide();
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
        }
    });
});

function xemChiTietND(obj) {
    var mnd = obj.value;
    var loc = location.pathname.substring(1);
    var url_get = '';
    if (loc == 'qlgd/user')
        url_get = 'user/xemchitiet';
    else
        url_get = 'xemchitiet';
    $.ajax({
        url: url_get,
        type: 'post',
        dataType: 'text',
        data: {
            manguoidung: mnd,
        },
        success: function (result) {
            var obj = jQuery.parseJSON(result);
            var html = '<div class="col-md-3 col-lg-3 " align="center">';
            html += '<img alt="User Pic" src="';
            html += base_url + 'qlgd/upload/nguoidung/' + obj.HinhAnh + '" class="img-rounded img-responsive">';
            html += '</div>';
            html += '<div class=" col-md-9 col-lg-9 ">';
            html += '<table class="table table-user-information">';
            html += '<tbody>';
            html += '<tr>';
            html += '<td>Tên người dùng:</td>';
            html += '<td>';
            html += obj.HoTenND;
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Ngày sinh:</td>';
            html += '<td>';
            var dateString = obj.NgaySinh; // Oct 23
            var dateParts = dateString.split("-");
            var dateFormat = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
            html += dateFormat;
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Giới tính</td>';
            html += '<td>';
            html += obj.GioiTinh == '0' ? 'Nữ' : 'Nam';
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Chức vụ</td>';
            html += '<td>';
            html += obj.ChucVu
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Email</td>';
            html += '<td>';
            html += '<a href="mailto:' + obj.Email + '">' + obj.Email + '</a>';
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Số điện thoại</td>';
            html += '<td>';
            html += obj.DienThoai;
            html += '</td>';
            html += '</tr>';
            html += '</tbody>';
            html += '</table>';
            html += '</div>';


            $('#infoNguoiDung').html(html);

        }
    });
}

function btnBaoHong(ma) {
    var maPTB = ma.value;
//    var link = base_url + "qlgd/giangduong/view?gd=1";
    var link = base_url + "qlgd/phong/baohong?maPTB=" + maPTB;
    $("#loadFormBaoHong").load(link + " #sub", function () {
        $('#btnSendBaoHong').click(function () {
            var ghichu = $('#baohongGhiChu').val();
            var soluong = $('#baohongSoLuong').val();
            var tinhtrang = $('#baohongTinhTrang').val();
            var MoTaVT = $('#baohongMoTaVT').val();
            var MoTa = $('#baohongMoTa').val();
            var maxSoLuong = $('#baohongSoLuong').attr('max');
            if(soluong < 1 || soluong > parseInt(maxSoLuong)){
                soluong < 1 ? alert("Số lượng phải lớn hơn 1") : alert("Số lượng thiết bị báo hỏng lớn hơn số lượng thiết bị hiện có");
                return;
            }
            $.ajax({
                url: 'phong/post',
                type: 'post',
                dataType: 'text',
                data: {
                    maPTB: maPTB,
                    ghichu: ghichu,
                    soluong: soluong,
                    tinhtrang: tinhtrang,
                    MoTaViTri: MoTaVT,
                    MoTa: MoTa
                },
                success: function (result) {
                    $('#btnBaoHongSended').click();
                    if (result == 'success'){
                        alert("Báo hỏng thành công");
                        location.reload();
                    }
                    else
                        alert("Đã xảy ra lỗi");
                }
            });
        })
    });
    $("#loadFormBaoHong").show(500);
}

function btnEditBaoHong(ma) {
    var stringGet = ma.getAttribute('value');
    var res = stringGet.split("|");
    var maPTB = res[0];
    var maPH = res[1];
//    console.log(res);
//    var link = base_url + "qlgd/giangduong/view?gd=1";
    var link = base_url + "qlgd/phong/suabaohong?maPTB=" + maPTB + '&maPH=' + maPH;
    $("#loadFormBaoHong").load(link + " #sub", function () {
        $('#btnSendBaoHong').click(function () {
            var ghichu = $('#baohongGhiChu').val();
            var soluong = $('#baohongSoLuong').val();
            var tinhtrang = $('#baohongTinhTrang').val();
            var MoTaVT = $('#baohongMoTaVT').val();
            var MoTa = $('#baohongMoTa').val();
            var maxSoLuong = $('#baohongSoLuong').attr('max');
            if (soluong < 1 || soluong > parseInt(maxSoLuong)) {
                soluong < 1 ? alert("Số lượng phải lớn hơn 1") : alert("Số lượng thiết bị báo hỏng lớn hơn số lượng thiết bị hiện có");
                return;
            }
            $.ajax({
                url: 'phong/editbaohong',
                type: 'post',
                dataType: 'text',
                data: {
                    maPTB: maPTB,
                    maPH: maPH,
                    ghichu: ghichu,
                    soluong: soluong,
                    tinhtrang: tinhtrang,
                    MoTaViTri: MoTaVT,
                    MoTa: MoTa
                },
                success: function (result) {
                    $('#btnBaoHongSended').click();
                    if (result == 'success') {
                        alert("Cập nhật thành công");
                        location.reload();
                    } else
                        alert("Đã xảy ra lỗi");
                }
            });
        })
    });
    $("#loadFormBaoHong").show(500);
}

function btnDeleteBaoHong(ma) {
    var maPH = ma.getAttribute('value');
    var link = base_url + "qlgd/phong/deleteBaoHong?maPH=" + maPH;
    if (confirm("Bạn có chắc muốn xoá")) {
        $.ajax({
            url: 'phong/deleteBaoHong',
            type: 'get',
            dataType: 'text',
            data: {
                maPH: maPH,
            },
            success: function (result) {
                console.log(result);
                if (result == 'success') {
                    alert("Xoá thành công");
                    location.reload();
                } else
                    alert("Đã xảy ra lỗi");
            }
        });
    } else {
        alert("Đã huỷ yêu cầu");
    }
    $("#loadFormBaoHong").show(500);
}

function btnXemCTTB(obj){
    var maTB = obj.value;
    console.log(maTB);
    var link = base_url + "qlgd/thietbi/view?matb=" + maTB;
    $("#loadXemCTTB").load(link + " #sub");
    
}


function loadTB(obj) {
    $.ajax({
        url: 'method/Ajax',
        type: 'post',
        dataType: 'json',
        data: {
            maphong: obj.value
        },
        success: function (result) {
            //Load Marker
            console.log(result);
            //alert(result);
            var html = '';
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<th scope="row" class="text-center">';
                html += key + 1;
                html += '</th>';
                html += '<td>';
                html += item['MaTB'];
                html += '</td>';
                html += '<td>';
                html += item['MaTB'];
                html += '</td>';
                html += '<td>';
                html += item['SoLuong'];
                html += '</td>';
                html += '<td>';
                html += item['SoLuong'];
                html += '</td>';
                html += '</tr>';
            });
            $('#loadTablePTB').html(html);
        }
    });
}

$.fn.extend({
    treeview: function () {
        return this.each(function () {
            // Initialize the top levels;
            var tree = $(this);

            tree.addClass('treeview-tree');
            tree.find('li').each(function () {
                var stick = $(this);
            });
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='tree-indicator glyphicon glyphicon-chevron-right'></i>");
                branch.addClass('tree-branch');
                branch.on('click', function (e) {

                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass("glyphicon-chevron-down glyphicon-chevron-right");
                        $(this).children().children().toggle();
                        //Load View
                        var getVal = $(this).find('a').attr('href');
                        var gd = getVal.split('=');
                        //alert(gd[1]);
                        //var thu = $(this).find('a').attr('href');
                        //alert(thu);
                        var link = base_url + "qlgd/giangduong/view?gd=" + gd[1];
                        $("#loadView").load(link + " #sub");
                        $("#loadView").show(500);

                    }
                })
                branch.children().children().toggle();
                branch.children('.tree-indicator, button, a').click(function (e) {
                    branch.click();
                    e.preventDefault();
                });
            });
        });
    }
});




